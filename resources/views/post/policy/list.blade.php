@extends('frontend.layouts.master')
@section('title_head', isset($title) ? $title : '')
@section('content')
        <div class="content list-page">
            <!-- Breadcrumb -->
        @include('frontend.includes.breadcrumb')
        <!-- End Breadcrumb -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-8 order-md-2 col-sm-12 col-xs-12">
                        <div class="list-product__content">
                            @if(!empty($posts) && count($posts) > 0)
                                @foreach ($posts as $post)
                                    <?php
                                    $imageProduct = 'images/product/news.jpg';
                                    if (!empty($post->image)) {
                                        $imageProduct = $post->image->origin;
                                    }
                                    ?>
                                    <div class="post-item">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-4 col-sm-5 col-12">
                                                    <a href="{{ route('fr_post_detail', ['page'=> $page->slug, 'post_slug' => $post->slug]) }}" class="post-item__image">
                                                        <img src="{{ asset($imageProduct) }}" alt="{{ $post->name }}">
                                                    </a>
                                                </div>
                                                <div class="col-lg-9 col-md-8 col-sm-7 col-12">
                                                    <h3 class="post-item__title">
                                                        <a href="{{ route('fr_post_detail', ['page'=> $page->slug, 'post_slug' => $post->slug]) }}">{{ $post->name }}</a>
                                                    </h3>
                                                    <p class="post-item__description">
                                                        {!! $post->description !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @include('post.pagination.pagination_list', ['pagination' => $posts])
                            @else
                                <div class="post-detail__content">
                                    <h4 class="text-center">Hiện tại chưa có bài viết nào!</h4>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 order-md-1 col-sm-12 col-xs-12">
                        @include('post.include.categories')
                    </div>
                </div>
            </div>
        </div>
    <!-- /diy -->
@endsection