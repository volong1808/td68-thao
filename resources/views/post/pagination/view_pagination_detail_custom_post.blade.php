<div class="other-post">
    <h3 class="other-post__title">Tin liên quan</h3>
    <ul class="other-post__content">
        @foreach($data as $post)
            <li>
                <a href="{{ route('fr_post_detail', ['page_slug' => $pageSlug, 'post_slug' => $post->slug]) }}" title="{{ $post->name }}">
                    {{ $post->name }}
                </a>
                <span class="time">({{ $post->updated_at->format("d.m.Y") }})</span>
            </li>
        @endforeach
    </ul>
    @include('post.pagination.pagination_detail_custom_post', ['pagination' => $data, 'postId' => $postId,  'postType' => $postType])
</div>