@if ($pagination->lastPage() > 1)
    <?php
    $currentRoute = \Illuminate\Support\Facades\Route::currentRouteName();
    $maxShowPage = config('constants.MAX_PAGE_SHOW');
    $pageStart = $pagination->currentPage();
    if ($pageStart > 1) {
        $pageStart = $pageStart - 1;
    }
    $maxShowPage = $maxShowPage + $pageStart;
    if ($maxShowPage > $pagination->lastPage()) {
        $maxShowPage = $pagination->lastPage();
    }
    $prevPage = $pagination->currentPage() - 1;
    if ($prevPage < 1) {
        $prevPage = 1;
    }
    $nextPage = $pagination->currentPage() + 1;
    if ($nextPage > $pagination->lastPage()) {
        $nextPage = $pagination->lastPage();
    }
    ?>
    <div class="list-pagination">
        @if($pagination->currentPage() == 1)
            <a disabled="disabled" href="javascript:void (0)" class="prev">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </a>
        @else
            <a class="prev" data-post-type="{{ $postType }}" data-title="{{ $title }}"
               href="{{ route($currentRoute, ['page' => $prevPage]) }}">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </a>
        @endif
        @if($nextPage > $pagination->lastPage())
            <a disabled="disabled" href="javascript:void (0)" class="next">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
        @else
            <a class="next" data-post-type="{{ $postType }}" data-title="{{ $title }}"
               href="{{ route($currentRoute, ['page' => $nextPage]) }}">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
        @endif
    </div>
@endif

