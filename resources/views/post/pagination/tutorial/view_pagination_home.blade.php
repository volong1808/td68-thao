<div class="title-list__wrap">
    <div class="title-list__left">
        <h2 class="title-list">Cẩm Nang & Hướng Dẫn</h2>
    </div>
    <div class="title-list__right">
        @include('post.pagination.pagination_home', ['pagination' => $data, 'postType' => 'tutorial', 'title' => 'Cẩm Nang & Hướng Dẫn'])
    </div>
</div>
<div class="list-item__content">
    <div class="container">
        <div class="row">
            @foreach($data as $item)
                <div class="product-item col-md-3 col-sm-6 col-12">
                    <?php
                    $imageProduct = 'images/product/news.jpg';
                    if (!empty($item->image)) {
                        $imageProduct = $item->image->origin;
                    }
                    ?>
                    <div class="image">
                        <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.tutorial.pageSlug'), 'post_slug' => $item->slug]) }}">
                            <img class="hvr-grow" src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                        </a>
                    </div>
                    <h3 class="title">
                        <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.tutorial.pageSlug'), 'post_slug' => $item->slug]) }}">
                            {{ $item->name }}</a>
                    </h3>
                </div>
            @endforeach
        </div>
    </div>
</div>
<div class="list-item__more"><a class="view-all" href="{{ url(config('custom_post.tutorial.pageSlug')) }}">Xem Tất Cả&nbsp;<i class="fa fa-caret-right"></i></a></div>