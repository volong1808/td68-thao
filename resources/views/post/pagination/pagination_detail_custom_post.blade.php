@if ($pagination->lastPage() > 1)
    <div class="paginate">
        <ul class="pages">
            <?php
            $currentUrl = url()->current();
            $maxShowPage = config('constants.MAX_PAGE_SHOW');
            $firstPage = 1;
            $pageStart = $pagination->currentPage();
            if ($pageStart > 1) {
                $pageStart = $pageStart - 1;
            }
            $maxShowPage = $maxShowPage + $pageStart;
            if ($maxShowPage > $pagination->lastPage()) {
                $maxShowPage = $pagination->lastPage();
            }
            $prevPage = $pagination->currentPage() - 1;
            if ($prevPage < 1) {
                $prevPage = 1;
            }
            $nextPage = $pagination->currentPage() + 1;
            if ($nextPage > $pagination->lastPage()) {
                $nextPage = $pagination->lastPage();
            }
            ?>

            @if($pagination->currentPage() > 1)
                <li><a data-post-type="{{ $postType }}" data-post-id="{{ $postId }}"  href="{{ $currentUrl . '?page=' .$prevPage }}" class="prev">Trang Trước</a>
            @endif
            <li>
                <a data-post-type="{{ $postType }}" data-post-id="{{ $postId }}" @if(1 == $pagination->currentPage()) class="active"
                   @endif href="{{ $currentUrl . '?page=1'  }}">{{ 1 }}</a>
            </li>
            @for ($i = $firstPage + 1; $i <= $maxShowPage; $i++)
                <li>
                    <a data-post-type="{{ $postType }}" data-post-id="{{ $postId }}" @if($i == $pagination->currentPage()) class="active"
                       @endif href="{{ $currentUrl . '?page=' .$i }}">{{ $i }}</a>
                </li>
            @endfor
            @if ($maxShowPage < $pagination->lastPage())
                <li>...</li>
                <li>
                    <a data-post-type="{{ $postType }}" data-post-id="{{ $postId }}"  href="{{ $currentUrl . '?page=' .$pagination->lastPage() }}">{{ $pagination->lastPage() }}</a>
                </li>
            @endif
            @if($pagination->currentPage() < $pagination->lastPage())
                <li><a data-post-type="{{ $postType }}" data-post-id="{{ $postId }}"  href="{{ $currentUrl . '?page=' .$nextPage }}" class="next">Trang Sau</a></li>
            @endif
        </ul>
    </div>
@endif
