<div class="container">
    <div class="row">
        @foreach($data as $item)
            <div class="product-item col-md-3 col-sm-6 col-12">
                <?php
                $imageProduct = 'images/product/product-1.jpg';
                if (!empty($item->image)) {
                    $imageProduct = $item->image->origin;
                }
                ?>
                <div class="image">
                    <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.'. $item->post_type .'.pageSlug'), 'post_slug' => $item->slug]) }}">
                        <img src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                    </a>
                </div>
                <h3 class="title">
                    <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.'. $item->post_type .'.pageSlug'), 'post_slug' => $item->slug]) }}">
                        {{ $item->name }} - {{ $item->{$item->post_type}->code }}</a>
                </h3>
                <div class="bottom">
                    <p class="price">{{ number_format($item->{$item->post_type}->price_default, 0, '.' , '.') }}
                        VNĐ</p>
                    <?php $options = getContentPostOptionPost($item->id, $item->post_type, 'size'); ?>
                    @if(!empty($options) && count($options) > 0)
                        <div class="btn-buy">
                            <button>Mua <i class="fa fa-sort-desc" aria-hidden="true"></i></button>
                            <div class="size">
                                <p class="size__title">Chọn size</p>
                                @foreach($options as $option)
                                    <?php
                                    $optionContent = json_decode($option->content, true);
                                    ?>
                                    <a href="{{ route('fr_order', ['id' => $item->id, 'size' => $option->id]) }}"
                                       data-price="{{ !empty($optionContent['price']) ? formatMoneyVI($optionContent['price']) : '0' }}"
                                       data-price-int="{{ !empty($optionContent['price']) ? $optionContent['price'] : '0' }}"
                                       data-option-id="{{ $option->id }}"
                                       class="size__item">
                                        {{ $option->name }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <a class="btn-buy" href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.'. $item->post_type .'.pageSlug'), 'post_slug' => $item->slug]) }}">
                            <button>Mua <i class="fa fa-sort-desc" aria-hidden="true"></i></button>
                        </a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    @include('post.pagination.pagination_detail', ['pagination' => $data, 'postId' => $postId, 'postType' => $postType])
</div>