@if ($pagination->lastPage() > 1)
    <div class="paginate">
        <ul class="pages">
            <?php
            $currentUrl = url()->current();
            $maxShowPage = config('constants.MAX_PAGE_SHOW');
            $pageStart = $pagination->currentPage();
            if ($pageStart > 1) {
                $pageStart = $pageStart - 1;
            }

            if ($maxShowPage > $pagination->lastPage()) {
                $maxShowPage = $pagination->lastPage();
            }
            $prevPage = $pagination->currentPage() - 1;
            if ($prevPage < 1) {
                $prevPage = 1;
            }
            $nextPage = $pagination->currentPage() + 1;
            if ($nextPage > $pagination->lastPage()) {
                $nextPage = $pagination->lastPage();
            }
            ?>

            @if($pagination->currentPage() > 1)
                <li><a href="{{ $currentUrl . (!empty($keyword) ? ('?key=' . $keyword . '&page=' ) : '?page=') .$prevPage }}" class="prev">Trang Trước</a>
            @endif
            <li>
                <a @if(1 == $pagination->currentPage()) class="active" @endif href="{{ $currentUrl . (!empty($keyword) ? ('?key=' . $keyword . '&page=1' ) : '?page=1')  }}">{{ 1 }}</a>
            </li>
            @for ($i = $pageStart + 1; $i <= $maxShowPage; $i++)
                <li>
                    <a @if($i == $pagination->currentPage()) class="active" @endif href="{{ $currentUrl . (!empty($keyword) ? ('?key=' . $keyword . '&page=' ) : '?page=') .$i }}">{{ $i }}</a>
                </li>
            @endfor
            @if ($maxShowPage < $pagination->lastPage())
                <li>...</li>
                <li>
                    <a href="{{ $currentUrl . (!empty($keyword) ? ('?key=' . $keyword . '&page=' ) : '?page=') .$pagination->lastPage() }}">{{ $pagination->lastPage() }}</a>
                </li>
            @endif
            @if($pagination->currentPage() < $pagination->lastPage())
                    <li><a href="{{ $currentUrl . (!empty($keyword) ? ('?key=' . $keyword . '&page=' ) : '?page=') .$nextPage }}" class="next">Trang Sau</a></li>
            @endif
        </ul>
    </div>
@endif
