<?php
use App\Category;
$products = Category::whereNull('parent_id')->where('post_type', 'product')->get();
$currentRoute = Route::current()->getName();
$currentSlug = \Illuminate\Support\Facades\Request::segment(3);
?>

<div class="categories">
    <div class="categories__content">
        <h3 class="categories__title">Danh mục sản phẩm</h3>
        @if(!empty($products) && count($products) > 0)
            @foreach ($products as $productItem)
                <?php
                /**
                 * @var $productItem
                 */
                $children = $productItem->children()->get();
                ?>

                @if (count($children) === 0)
                    <div class="categories__parent {{ ($currentSlug == $productItem->slug) ? 'active' : '' }}">
                        <a class="hvr-sweep-to-right" href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => $productItem->slug]) }}">
                            <i class="fa-angle-right fa"></i>{{ $productItem->name }}</a>
                    </div>
                @else
                    <div class="categories__parent {{ ($currentSlug == $productItem->slug) ? 'active' : '' }}">
                        <a class="hvr-sweep-to-right" href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => $productItem->slug]) }}">
                            <i class="fa-angle-right fa"></i>{{ $productItem->name }}</a>
                    </div>
                    @foreach ($children as $child)
                        <div class="categories__children {{ ($currentSlug == $child->slug) ? 'active' : '' }}">
                            <a href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => $child->slug]) }}">
                                • {{ $child->name }}</a>
                        </div>
                    @endforeach
                @endif
            @endforeach
        @endif
    </div>
</div>