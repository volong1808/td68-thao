@extends('frontend.layouts.master')
@section('content')
    <div class="content list-page">
    @include('frontend.includes.breadcrumb')
    <!-- diy -->
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 order-md-2 col-sm-12 col-xs-12">
                    <div class="post-detail__content">
                        {!! $postDetail->content !!}
                    </div>

                    <div class="other-post-wrap">
                        @if( (!empty($otherCustomPost)) && count($otherCustomPost) > 0)
                            <div class="other-post">
                                <h3 class="other-post__title">Tin liên quan</h3>
                                <ul class="other-post__content">
                                    @foreach ($otherCustomPost as $post)
                                        <li>
                                            <a href="{{ route('fr_post_detail', ['page_slug' => $page->slug, 'post_slug' => $post->slug]) }}" title="{{ $post->name }}">
                                                {{ $post->name }}
                                            </a>
                                            <span class="time">({{ $post->updated_at->format("d.m.Y") }})</span>
                                        </li>
                                    @endforeach
                                </ul>

                                @include('post.pagination.pagination_detail_custom_post', ['pagination' => $otherCustomPost, 'postId' => $postDetail->id, 'postType' => $postDetail->post_type])
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 order-md-1 col-sm-12 col-xs-12">
                    @include('post.include.categories')
                </div>
            </div>
        </div>
    </div>
    <!-- /diy -->
@endsection
@section('script_page')
    <script type="text/javascript">
        $(document).on('click', '.paginate a', function (event) {
            event.preventDefault();
            var listWrap = $(this).closest('.other-post-wrap');
            var page = $(this).attr('href').split('page=')[1];
            var postId = $(this).data('post-id');
            var postType = $(this).data('post-type');
            var pageSlug = "{{ $page->slug }}";

            fetch_data_detail(page, postId, postType,pageSlug, listWrap);
        });

        function fetch_data_detail(page, postId, postType,pageSlug, listWrap) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: "{{url('/')}}" + "/paginate-detail-custom-post",
                data: {
                    page: page,
                    postId: postId,
                    postType: postType,
                    pageSlug: pageSlug
                },
                success: function (data) {
                    console.log(data);
                    listWrap.html(data);
                }
            });
        }
    </script>
@endsection