@extends('frontend.layouts.master')
@section('title_head', isset($title) ? $title : '')
@section('content')
    @isset($posts)
        <div class="content list-page">
            <div class="banner-default">
                <div class="container">
                    <h3>TÌM KIẾM</h3>
                    <ul class="breadcumb">
                        <li><a href="{{ route('home') }}">Trang chủ</a></li>
                        <li><span class="sep"><i class="fa fa-angle-right"></i></span></li>
                        <li>Tìm Kiếm</li>
                    </ul>
                </div><!-- /.container -->
            </div>
        <!-- End Breadcrumb -->
            <div class="container">
                <div class="title-list__wrap">
                    <div class="title-list__left">
                        <h2 class="title-list">Tìm kiếm cho: {{ $keyword }}</h2>
                    </div>
                </div>
            </div>
            <div class="list-product__content">
                <div class="container">
                    @if(count($posts)  > 0)
                        <div class="row">
                            @foreach ($posts as $item)
                                <div class="product-item hvr-bob col-md-3 col-sm-6 col-xs-12">
                                    <?php
                                    $imageProduct = 'images/product.jpg';
                                    if (!empty($item->image)) {
                                        $imageProduct = $item->image->thumb;
                                    }
                                    ?>
                                    <div class="product-item__image">
                                        <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                            <img src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                                        </a>
                                    </div>
                                    <div class="product-item__title">
                                        <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                            {{ $item->name }}
                                        </a>
                                    </div>
                                    <div class="product-item__status">Tình trạng: {{ getStatusProduct(optional($item->product)->status) }}</div>
                                    <div class="product-item__price">
                                        <p class="price__default {{ !empty(optional($item->product)->price_sale) ? 'sale' : '' }}">{{ formatMoneyVI(optional($item->product)->price_default) }}</p>
                                        @if(!empty(optional($item->product)->price_sale))
                                            <p class="price__sale">{{ formatMoneyVI(optional($item->product)->price_sale) }}</p>
                                        @endif
                                    </div>
                                    <div class="product-item__description zoomIn animated">
                                        <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                            <div class="description__content">
                                                <strong>{{ $item->name }}</strong>
                                                <p>{!! nl2br($item->description) !!}</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @include('post.pagination.pagination_list', ['pagination' => $posts])
                    @else
                        <p class="mt-3">Không có kết quả nào phù hợp!</p>
                    @endif
                </div>
            </div>
        </div>
    @endisset
    <!-- /diy -->
@endsection