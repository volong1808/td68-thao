@extends('frontend.layouts.master')
@section('title_head', isset($title) ? $title : '')
@section('content')

    <!-- product -->
    <div class="content list-page">
        @include('frontend.includes.breadcrumb')
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 order-md-2 col-sm-12 col-xs-12">
                    <div class="product-detail">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-12">
                                    <div class="detail__image">
                                        <?php
                                        $image = '';
                                        if (!empty($postDetail->image)) {
                                            $image = $postDetail->image->origin;
                                        }
                                        ?>

                                        <div class="slick-initialized slick-slider">
                                            <div aria-live="polite" class="slick-list draggable">
                                                <div class="slick-track" role="listbox">
                                                    <a id="post-image-zoom" class="MagicZoom slick-slide slick-current slick-active mz-thumb-selected mz-thumb"
                                                       href="{{ asset($image) }}">
                                                        <img src="{{ asset($image) }}" alt="{{ $postDetail->name }}">
                                                    </a>
                                                </div>
                                                @if(!empty($gallery))
                                                    <div class="product__selectors">
                                                        @foreach($gallery as $item)
                                                            <a data-zoom-id="post-image-zoom" data-image="{{ asset($item->origin) }}"
                                                               href="{{ asset($item->origin) }}">
                                                                <img src="{{ asset($item->thumb) }}" alt="{{ $postDetail->name }}">
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-12">
                                    <div class="product__info">
                                        <ul>
                                            <li class="info__title">{{ $title }}</li>
                                            <li class="info__code">Mã sản phẩm: {{ $postDetail->code }}</li>
                                            @if(!empty($postDetail->storage))
                                                <li class="info__storage">Dung Lượng: {{ $postDetail->storage }}</li>
                                            @endif
                                            <li class="info__price">Giá: <span id="show-price">{{ !empty($postDetail->price_sale) ? formatMoneyVI($postDetail->price_sale) : formatMoneyVI($postDetail->price_default)}}</span>
                                            </li>
                                            @if(!empty($postDetail->description))
                                            <li class="info__description">{{ $postDetail->description }}</li>
                                            @endif
                                            <li class="info__storage">Tình trạng: {{ getStatusProduct($postDetail->status) }}</li>
                                            <li class="info__buy">
                                                @if($postDetail->status == 2)
                                                    <a class="info__btn-buy" id="order_cart" href="javascript:void(0)">
                                                        Vui Lòng Quay Lại Sau
                                                    </a>
                                                @elseif($postDetail->status == 1)
                                                    <a class="info__btn-buy" id="order_cart" href="javascript:void(0)">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Đặt Hàng
                                                    </a>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-detail__content">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#content-detail">Thông tin sản phẩm</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#info-detail">Thông số kỹ thuật</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="content-detail">
                                    <div class="tab">
                                        {!! $postDetail->content  !!}
                                    </div>
                                </div>
                                <div class="tab-pane" id="info-detail">
                                    <div class="tab">
                                        {!! nl2br(optional($postDetail->product)->info)  !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(!empty($posts) && count($posts) > 0)
                        <div class="title-list__wrap">
                            <div class="title-list__left">
                                <h2 class="title-list">Sản Phẩm Cùng loại</h2>
                            </div>
                        </div>
                        <div class="list-product__content">
                            <div class="container-fluid">
                                <div class="row">
                                    @foreach ($posts as $item)
                                        <div class="product-item hvr-bob col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                            <?php
                                            $imageProduct = 'images/product.jpg';
                                            if (!empty($item->image)) {
                                                $imageProduct = $item->image->thumb;
                                            }
                                            ?>
                                            <div class="product-item__image">
                                                <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                    <img src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                                                </a>
                                            </div>
                                            <div class="product-item__title">
                                                <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                    {{ $item->name }}
                                                </a>
                                            </div>
                                            <div class="product-item__status">Tình trạng: {{ getStatusProduct(optional($item->product)->status) }}</div>
                                                <div class="product-item__price">
                                                    <p class="price__default {{ !empty(optional($item->product)->price_sale) ? 'sale' : '' }}">{{ formatMoneyVI(optional($item->product)->price_default) }}</p>
                                                    @if(!empty(optional($item->product)->price_sale))
                                                        <p class="price__sale">{{ formatMoneyVI(optional($item->product)->price_sale) }}</p>
                                                    @endif
                                                </div>
                                            <div class="product-item__description zoomIn animated">
                                                <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                    <div class="description__content">
                                                        <strong>{{ $item->name }}</strong>
                                                        <p>{!! nl2br($item->description) !!}</p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-3 col-md-4 order-md-1 col-sm-12 col-xs-12">
                    @include('post.include.categories')
                </div>
            </div>
        </div>
    </div>
    <!-- /product -->
@endsection
@section('css_page')
    <link href="{{ asset('backend/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <style>
        .page-content {
            display: block !important;
        }
    </style>
@endsection
@section('script_page')
    <script type="text/javascript">
        $(document).on('click', '.paginate a', function (event) {
            event.preventDefault();
            var listWrap = $(this).closest('.list-product__content');
            var page = $(this).attr('href').split('page=')[1];
            var postId = $(this).data('post-id');
            var postType = $(this).data('post-type');
            var pageSlug = "{{ $page->slug }}";

            fetch_data_detail(page, postId, postType, pageSlug, listWrap);
        });

        function fetch_data_detail(page, postId, postType, pageSlug, listWrap) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: "{{url('/')}}" + "/paginate-detail",
                data: {
                    page: page,
                    postId: postId,
                    postType: postType,
                    pageSlug: pageSlug
                },
                success: function (data) {
                    listWrap.html(data);
                }
            });
        }
    </script>
@endsection