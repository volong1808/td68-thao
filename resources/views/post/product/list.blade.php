@extends('frontend.layouts.master')
@section('title_head', isset($title) ? $title : '')
@section('content')
    @isset($posts)
        <div class="content list-page">
            <!-- Breadcrumb -->
        @include('frontend.includes.breadcrumb')
        <!-- End Breadcrumb -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-8 order-md-2 col-sm-12 col-xs-12">
                        <div class="title-list__wrap">
                            <div class="title-list__left">
                                <h2 class="title-list">{{ $title }}</h2>
                            </div>
                            <div class="title-list__right">
                                <div class="filter-price">
                                    <select class="form-control">
                                        <option disabled selected>Lọc giá</option>
                                        <option value="1">Giá tăng dần</option>
                                        <option value="2">Giá giảm dần</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="list-product__content">
                            <div class="container-fluid">
                                <div class="row">
                                    @foreach ($posts as $item)
                                        <div class="product-item hvr-bob col-lg-3  col-md-4 col-sm-6 col-xs-12">
                                            <?php
                                            /**
                                             * @var $item
                                             */
                                            $imageProduct = 'images/product.jpg';
                                            if (!empty($item->image)) {
                                                $imageProduct = $item->image->thumb;
                                            }
                                            ?>
                                            <div class="product-item__image">
                                                <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                    <img src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                                                </a>
                                            </div>
                                            <div class="product-item__title">
                                                <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                    {{ $item->name }}
                                                </a>
                                            </div>
                                            <div class="product-item__status">Tình trạng: {{ getStatusProduct(optional($item->product)->status) }}</div>
                                                <div class="product-item__price">
                                                    <p class="price__default {{ !empty(optional($item->product)->price_sale) ? 'sale' : '' }}">{{ formatMoneyVI(optional($item->product)->price_default) }}</p>
                                                    @if(!empty(optional($item->product)->price_sale))
                                                        <p class="price__sale">{{ formatMoneyVI(optional($item->product)->price_sale) }}</p>
                                                    @endif
                                                </div>
                                            <div class="product-item__description zoomIn animated">
                                                <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                    <div class="description__content">
                                                        <strong>{{ $item->name }}</strong>
                                                        <p>{!! nl2br($item->description) !!}</p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                @include('post.pagination.pagination_list', ['pagination' => $posts])
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 order-md-1 col-sm-12 col-xs-12">
                        @include('post.include.categories')
                    </div>
                </div>
            </div>
        </div>
    @endisset
    <!-- /product -->
@endsection