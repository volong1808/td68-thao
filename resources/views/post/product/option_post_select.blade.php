<div class="check_price">
    <label for="">{{ $optionName }}</label>
    <div>
        <select name="{{ $inputName }}" class="calc-price-product">
            @if (!empty($optionsItem))
                @foreach($optionsItem as $option)
                    <option value="{{ $option->id }}">{{ $option->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>