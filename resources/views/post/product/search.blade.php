@extends('frontend.layouts.master')
@section('title_head', isset($title) ? $title : '')
@section('content')
    @isset($posts)
        <div class="content list-page">
            <!-- Breadcrumb -->
            <div class="breadcrumb-wrap">
                <ul class="breadcrumb_content">
                    <li><a href="{{ route('home') }}">Trang chủ</a></li>
                    <li>Tìm kiếm</li>
                </ul>
            </div>
        <!-- End Breadcrumb -->
            <div class="title-list__wrap">
                <div class="title-list__left">
                    <h2 class="title-list">Tìm kiếm cho: {{ $keyword }}</h2>
                </div>
                <div class="title-list__right">
                    <div class="filter-price">
                        <select class="form-control">
                            <option disabled selected>Lọc giá</option>
                            <option value="1">Giá tăng dần</option>
                            <option value="2">Giá giảm dần</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="list-product__content">
                <div class="container-fluid">
                    @if(count($posts)  > 0)
                        <div class="row">
                            @foreach ($posts as $post)
                                <div class="product-item col-md-3 col-sm-6 col-12">
                                    <?php
                                    $imageProduct = 'images/product/product-1.jpg';
                                    if (!empty($post->image)) {
                                        $imageProduct = $post->image->origin;
                                    }
                                    ?>
                                    <div class="image">
                                        <a href="{{ route('fr_post_detail', ['page_slug' => 'san-pham', 'post_slug' => $post->slug]) }}">
                                            <img src="{{ asset($imageProduct) }}" alt="{{ $post->name }}">
                                        </a>
                                    </div>
                                    <h3 class="title">
                                        <a href="{{ route('fr_post_detail', ['page_slug' => 'san-pham', 'post_slug' => $post->slug]) }}">
                                            {{ $post->name }} - {{ $post->product->code }}</a>
                                    </h3>
                                    <div class="bottom">
                                        <p class="price">{{ number_format($post->product->price_default, 0, '.' , '.') }}
                                            VNĐ</p>
                                        <div class="btn-buy">
                                            <button>Mua <i class="fa fa-sort-desc" aria-hidden="true"></i>
                                            </button>
                                            <div class="material">
                                                <p class="material__title">Chọn chất liệu</p>
                                                <ul>
                                                    @if(!empty($post->option) && count($post->option) > 0)
                                                        <?php $options = getContentPostOptionPost($post->id, 'diy', 'materials'); ?>
                                                        @if(!empty($options))
                                                            @foreach($options as $item)
                                                                <?php
                                                                $sizeIds = json_decode($item->post_option_post_content, true);
                                                                ?>
                                                                @if(!empty($sizeIds['size']))
                                                                    <li class="material__item">
                                                                        <a  href="javascript:void(0)">
                                                                            {{ $item->name }}
                                                                        </a>
                                                                        <div class="size">
                                                                            <p class="size__title">Chọn size</p>
                                                                            <?php
                                                                            $sizes = getMappingByKeyAndToIds($sizeIds['size']);
                                                                            $priceOfSize = getMappingByKeyInContent($item->id, $sizeIds['size'], 'price');
                                                                            ?>
                                                                            @foreach ($sizes as $size)
                                                                                <a class="size__item" href="{{ route('fr_order', ['id' => $post->id, 'size' => $size->id, 'materials' => $item->id]) }}"
                                                                                   data-price-default="{{ !empty($post->product->price_default) ? formatMoneyVI($post->product->price_default) : ''  }}"
                                                                                   data-price="{{ !empty($priceOfSize[$size->id]) ? formatMoneyVI($priceOfSize[$size->id]) : ''  }}">
                                                                                    {{ $size->name }}
                                                                                </a>
                                                                            @endforeach
                                                                        </div>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @include('post.pagination.pagination_list', ['pagination' => $posts])
                    @else
                        <p class="mt-3">Không có kết quả nào phù hợp!</p>
                    @endif
                </div>
            </div>
        </div>
    @endisset
    <!-- /diy -->
@endsection