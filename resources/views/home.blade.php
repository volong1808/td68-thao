@extends('frontend.layouts.master')
@section('css_page')
    <link rel="stylesheet" href="{{ asset('assets/bxslider/css/bxslider.min.css') }}">
@endsection
@section('content')
    <!-- Content -->
    <div class="content home">
        <div class="container">
            <!-- Slide -->
            <div class="row">
                <div class="top-content__left col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="slider-home owl-carousel owl-theme w-100 h-100" id="slider-home">
                        @if (count($slides) > 0)
                            @foreach($slides as $item)
                                @php
                                $image = 'images/slider/slider-1.jpg';
                                /**
                                 * @var $item
                                 */
                                if (!empty($item->image)) {
                                    $image = $item->image->origin;
                                }
                                @endphp

                                <div class="slider-item" style="background-image: url('{{asset($image)}}')"></div>
                            @endforeach
                        @else
                            <div class="slider-item"
                                 style="background-image: url('{{ asset('images/slider/slider-1.jpg') }}') "></div>
                        @endif
                    </div>
                </div>
                <div class="top-content__right col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="content-news">
                        <div class="title"><span>Tin Nổi Bật</span></div>
                        <ul>
                            @foreach($news as $item)
                                <li>
                                    <?php
                                    $imageNews = 'images/news.jpg';
                                    if (!empty($item->image)) {
                                        $imageNews = $item->image->thumb;
                                    }
                                    ?>
                                    <div class="content-img">
                                        <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.tutorial.pageSlug'), 'post_slug' => $item->slug]) }}"><img src="{{ asset($imageNews) }}" alt="{{ $item->name }}"></a>
                                    </div>
                                    <div class="content-info">
                                        <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.tutorial.pageSlug'), 'post_slug' => $item->slug]) }}">{{ $item->description }}</a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Slide -->

            <!-- Product -->
            @if(!empty($products) && count($products) > 0)
                <div class="list-item">
                    <div class="title-list__wrap">
                        <div class="title-list__left">
                            <h2 class="title-list">SẢN PHẨM NỔI BẬT</h2>
                        </div>
                    </div>
                    <div class="list-item__content">
                        <div class="container">
                            <div class="row">
                                @foreach($products as $item)
                                    <div class="product-item hvr-bob col-xl-2 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <?php
                                        $imageProduct = 'images/product1.jpg';
                                        if (!empty($item->image)) {
                                            $imageProduct = $item->image->thumb;
                                        }
                                        ?>
                                        <div class="product-item__image">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                <img src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                                            </a>
                                        </div>
                                        <div class="product-item__title">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                {{ $item->name }}
                                            </a>
                                        </div>
                                        <div class="product-item__status">Tình trạng: {{ getStatusProduct(optional($item->product)->status) }}</div>
                                        <div class="product-item__price">
                                            <p class="price__default {{ !empty(optional($item->product)->price_sale) ? 'sale' : '' }}">{{ formatMoneyVI(optional($item->product)->price_default) }}</p>
                                            @if(!empty(optional($item->product)->price_sale))
                                                <p class="price__sale">{{ formatMoneyVI(optional($item->product)->price_sale) }}</p>
                                            @endif
                                        </div>
                                        <div class="product-item__description zoomIn animated">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                <div class="description__content">
                                                    <strong>{{ $item->name }}</strong>
                                                    <p>{!! nl2br($item->description) !!}</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="list-item__more"><a class="view-all" href="{{ url(config('custom_post.product.pageSlug')) }}">Xem Thêm Sản Phẩm&nbsp;<i class="fa fa-caret-right"></i></a></div>
                </div>
            @endif
            <!-- /Product -->

            <!-- accessories -->
            @if(!empty($accessories) && count($accessories) > 0)
                <div class="list-item">
                    <div class="title-list__wrap">
                        <div class="title-list__left">
                            <h2 class="title-list">Phụ Kiện Chính Hãng</h2>
                        </div>
                    </div>
                    <div class="list-item__content">
                        <div class="container">
                            <div class="row">
                                @foreach($accessories as $item)
                                    <div class="product-item hvr-bob col-xl-2 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <?php
                                        $imageProduct = 'images/product1.jpg';
                                        if (!empty($item->image)) {
                                            $imageProduct = $item->image->thumb;
                                        }
                                        ?>
                                        <div class="product-item__image">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                <img src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                                            </a>
                                        </div>
                                        <div class="product-item__title">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                {{ $item->name }}
                                            </a>
                                        </div>
                                        <div class="product-item__status">Tình trạng: {{ getStatusProduct(optional($item->product)->status) }}</div>
                                        <div class="product-item__price">
                                            <p class="price__default {{ !empty(optional($item->product)->price_sale) ? 'sale' : '' }}">{{ formatMoneyVI(optional($item->product)->price_default) }}</p>
                                            @if(!empty(optional($item->product)->price_sale))
                                                <p class="price__sale">{{ formatMoneyVI(optional($item->product)->price_sale) }}</p>
                                            @endif
                                        </div>
                                        <div class="product-item__description zoomIn animated">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                <div class="description__content">
                                                    <strong>{{ $item->name }}</strong>
                                                    <p>{!! nl2br($item->description) !!}</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="list-item__more"><a class="view-all" href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => 'may-cu']) }}">Xem Thêm Sản Phẩm&nbsp;<i class="fa fa-caret-right"></i></a></div>
                </div>
            @endif
            <!-- /accessories -->

            <!-- OldProduct -->
            @if(!empty($oldProducts) && count($oldProducts) > 0)
                <div class="list-item">
                    <div class="title-list__wrap">
                        <div class="title-list__left">
                            <h2 class="title-list">Máy Cũ</h2>
                        </div>
                    </div>
                    <div class="list-item__content">
                        <div class="container">
                            <div class="row">
                                @foreach($oldProducts as $item)
                                    <div class="product-item hvr-bob col-xl-2 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <?php
                                        $imageProduct = 'images/product1.jpg';
                                        if (!empty($item->image)) {
                                            $imageProduct = $item->image->thumb;
                                        }
                                        ?>
                                        <div class="product-item__image">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                <img src="{{ asset($imageProduct) }}" alt="{{ $item->name }}">
                                            </a>
                                        </div>
                                        <div class="product-item__title">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                {{ $item->name }}
                                            </a>
                                        </div>
                                        <div class="product-item__status">Tình trạng: {{ getStatusProduct(optional($item->product)->status) }}</div>
                                        <div class="product-item__price">
                                            <p class="price__default {{ !empty(optional($item->product)->price_sale) ? 'sale' : '' }}">{{ formatMoneyVI(optional($item->product)->price_default) }}</p>
                                            @if(!empty(optional($item->product)->price_sale))
                                                <p class="price__sale">{{ formatMoneyVI(optional($item->product)->price_sale) }}</p>
                                            @endif
                                        </div>
                                        <div class="product-item__description zoomIn animated">
                                            <a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.product.pageSlug'), 'post_slug' => $item->slug]) }}">
                                                <div class="description__content">
                                                    <strong>{{ $item->name }}</strong>
                                                    <p>{!! nl2br($item->description) !!}</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="list-item__more"><a class="view-all" href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => 'may-cu']) }}">Xem Thêm Sản Phẩm&nbsp;<i class="fa fa-caret-right"></i></a></div>
                </div>
        @endif
            <!-- /OldProduct -->

        </div>
    </div>
    <!-- End Content -->
@endsection
@section('script_page')
    <!-- Bxslider JS -->
    <script src="{{ asset('assets/bxslider/js/bxslider.min.js') }}"></script>
    <script>
        $(document).on('click', '.list-pagination a', function (event) {
            event.preventDefault();
            var listWrap = $(this).closest('.list-item');
            var page = $(this).attr('href').split('page=')[1];
            var postType = $(this).data('post-type');
            var title = $(this).data('title');

            fetch_data(page, postType, title, listWrap);
        });

        function fetch_data(page, postType, title, listWrap) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: "{{url('/')}}" + "/paginate-home",
                data: {
                    page: page,
                    postType: postType,
                    title: title
                },
                success: function (data) {
                    console.log(data);
                    listWrap.html(data);
                }
            });
        }

    </script>
@endsection