@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Trang Quản Lý Trang Tĩnh</h1>
            </div>

            <div class="heading-elements">
                <a href="{{ route('page_add') }}" class="btn btn-labeled bg-blue heading-btn">
                    <b><i class="icon-plus2"></i></b> Thêm Mới
                </a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li class="active">Danh Sách Trang Tĩnh</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
                @endif
                <!-- table form -->
                <form action="{{ route('page_bulk_action') }}" method="post">
                    @csrf
                    <div class="row table-control">
                        <div class="col-md-6">
                            <select name="action" class="form-control form-item-inline">
                                <option value="{{ config('constants.BULK_ACTION.none') }}">Tác Vụ</option>
                                <option value="{{ config('constants.BULK_ACTION.delete') }}">Xóa Đã Chọn</option>
                            </select>
                            <button type="submit" class="btn btn-default">Áp Dụng</button>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <!-- table -->
                    <div class="table-responsive">
                        <table class="table table-framed table-hover">
                            <thead>
                            <tr>
                                <th width="50px">
                                    <label for="all-checkbox" class="hidden">Check All</label>
                                    <input type="checkbox" id="all-checkbox" class="styled" value="1">
                                </th>
                                <th>Tên Trang</th>
                                <th>Slug</th>
                                <th>Trạng Thái</th>
                                <th>Thời Gian</th>
                                <th width="120px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (empty($pages))
                                <tr>
                                    <td colspan="6"><strong>Hiện tại chưa có sản phẩm nào</strong></td>
                                </tr>
                            @else
                                <?php
                                /* @var $pages */
                                $currentOffset = ($pages->currentpage() - 1) * $pages->perpage() + 1;
                                $toOffset = ($pages->currentpage() - 1) * $pages->perpage() + $pages->count();
                                ?>
                                @foreach($pages as $page)
                                    <tr>
                                        <td>
                                            <input name="checkedPost[]" type="checkbox" class="styled table-checkbox"
                                                   value="{{ $page->id }}">
                                        </td>
                                        <td>
                                            <a href="{{ route('page_edit', ['id' => $page->id]) }}">
                                                {{ $page->name }}
                                            </a>
                                        </td>
                                        <td>{{ $page->slug }}</td>
                                        <td>
                                            @if ($page->state == config('constants.POST_STATE.PUBLISHED'))
                                                <span class="label label-info">Hiển Thị</span>
                                            @else
                                                <span class="label label-danger">Không Hiển Thị</span>
                                            @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($page->updated_at)->format('d/m/Y') }}</td>
                                        <td>
                                            <a href="{{ route('page_edit', ['id' => $page->id]) }}" class="text-warning" data-popup="tooltip" title="Chỉnh Sửa">
                                                <i class="icon-pencil7"></i>
                                            </a>
                                            <a href="#" class="text-black" data-toggle="modal" title="Xoá"  data-popup="tooltip"
                                                    data-target="#modal-delete" data-id="{{ $page->id }}">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /table -->
                    <!-- table footer -->
                    <div class="row table-footer">
                        <div class="col-md-6">
                            <p class="summary-table-result">
                                @if($pages->count() > 0)
                                    Hiển thị từ <strong>{{ $currentOffset }}</strong>
                                    tới <strong>{{ $toOffset }}</strong>
                                    của <strong>{{ $pages->total() }}</strong> kết quả.
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            {{ $pages->links() }}
                        </div>
                    </div>
                </form>
                <!-- /table footer -->
            </div>
        </div>
        <!-- /simple panel -->
    </div>
    <!-- /content area -->
    @include('admin.layout.include.modal_delete', ['url' => route('page_delete')])
@endsection
