<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Tên Tùy Chọn',
                    'inputType' => 'text',
                    'inputName' => 'name',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                @if (!empty($optionConfig['content']))
                    @foreach ($optionConfig['content'] as $option)
                        @include ('admin.layout.include.form_input', $option)
                    @endforeach
                @endif

                @if(!empty($mappingOption))
                    @foreach ($mappingOption as $key => $mapping)
                        <?php
                        $configOption = array_get($postConfig, 'option.' . $key);
                        $items = [];
                        if (!empty($mapping['items'])) {
                            $items = $mapping['items'];
                            if ($mapping['inputType'] != 'checkboxInput') {
                                $items = getOptionSelectFromTable($mapping['items'], 'id', 'name');
                            }
                        }
                        $inputData = [
                            'label' => $configOption['name'],
                            'inputType' => $mapping['inputType'],
                            'inputName' => "mapping[{$key}][]",
                            'permalink' => false,
                            'item' => $items,
                            'selected' => !empty($mappingSelect[$key]) ?  getValuesByKey($mappingSelect[$key], 'option_post_to_id') : null,
                            'formChildContent' => !empty($mappingSelect[$key]) ?  getOptionSelectFromTable($mappingSelect[$key], 'option_post_to_id', 'content', null) : null,
                            'formChildConfig' => !empty($mapping['content']) ? $mapping['content'] : null
                        ];
                        ?>
                        @include ('admin.layout.include.form_input', $inputData)
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                                {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                                {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    @if (isset($category))
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    @else
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    @endif
                </div>
            </div>
        </div><!-- /panel -->
    </div>
</div>
