<!-- Left icons -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <label class="control-label">Tùy Chọn {{ $postType }}</label>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @foreach ($configFormOptions as $inputType => $optionForm)
                    <?php $viewInput = 'admin.option_post.' . $inputType; ?>
                    @if (view()->exists($viewInput))
                        @include('admin.option_post.' . $inputType, ['optionConfig' => $optionForm])
                    @else
                        @foreach ($optionForm as $nameOption => $config)
                            <?php
                            $option = [];
                            $selected = null;
                            $selectItem = [];
                            if (!empty($options[$nameOption])) {
                                $option = $options[$nameOption];
                            }
                            if (!empty($optionsSelect[$nameOption])) {
                                $selectItem = $optionsSelect[$nameOption];
                            }
                            $item = getOptionSelectFromTable($option, 'id', 'name', '');
                            $childForm = null;
                            $inputType = $config['typeInput'];
                            if (!empty($config['mappingOption'])) {
                                $childForm = $config['mappingOption'];
                                $inputType = 'checkboxInput';
                                $item = $option;
                            }

                            $inputData = [
                                'label' => $config['name'],
                                'inputType' => $inputType,
                                'inputName' => 'option_post[]',
                                'permalink' => false,
                                'item' => $item,
                                'selected' => getValuesByKey($selectItem, 'option_post_id'),
                                'formChildConfig' => $childForm,
                                'formChildContent' => getOptionSelectFromTable($selectItem, 'option_post_id', 'post_option_post_content', null)
                            ];
                            ?>
                            @if (!empty($config['mappingOption']))
                                    @include ('admin.layout.include.inputs.checkboxFormChild', $inputData)
                            @else
                                @include ('admin.layout.include.form_input', $inputData)
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>