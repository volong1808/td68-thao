<div class="tabbable">
    <ul class="nav nav-tabs nav-tabs-highlight">
        <?php $tabActive = 'size' ?>
        @foreach($optionConfig as $tab => $config)
            <li @if ($tab == $tabActive) class="active" @endif>
                <a href="#{{ $postType }}-option-{{ $tab }}" data-toggle="tab">
                    <i class="{{ $config['icon'] }} position-left"></i>{{ $config['name'] }}
                </a>
            </li>
        @endforeach
    </ul>

    <div class="tab-content">
        @foreach($optionConfig as $tab => $config)
            <div class="tab-pane @if ($tab == $tabActive) active @endif" id="{{ $postType }}-option-{{ $tab }}">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                data-target="#option-modal-{{ $tab }}"
                                data-content="#{{ $postType }}-option-{{ $tab }}"><i
                                    class="icon-plus3"></i> Thêm Tùy Chọn
                        </button>
                        <button type="button" class="btn btn-danger btn-sm js-hide-option" data-content="#{{ $postType }}-option-{{ $tab }}"><i class="icon-minus-circle2"></i> Ẩn Tùy Chọn</button>
                    </div>
                </div>
                @include('admin.option_post.option_post_list', ['config' => $config, 'optionList' => $options[$tab], 'type' => $tab])
            </div>
        @endforeach
    </div>
</div>
<!-- /left icons -->
@section('js')
    @include('admin.layout.include.modal_delete_ajax')
    @include('admin.option_post.modal_edit_option')
    @foreach($optionConfig as $tab => $config)
        @include('admin.option_post.modal_option')
    @endforeach
    <script type="text/javascript">
        const RESPONSIVE_SUCCESS = 200;
        $(document).ready(function () {
            let clearOptionButton = $('.js-hide-option');
            clearOptionButton.click(function () {
                let tabContent = $(this).attr('data-content');
                $(tabContent).find('.js-check').each(function () {
                    if (!$(this).is(':checked')) {
                        $(this).closest('tr').remove();
                    }
                })
            });

            // submit form
            let submitFormOptionPostButton = $('.js-submit-form');
            submitFormOptionPostButton.click(function (e) {
                e.preventDefault();
                let formContent =  $(this).attr('data-content');
                let contentData =  $(this).attr('data-content-row');
                let modal =  $(this).attr('data-modal');
                let form = $(formContent);
                var url = form.attr('action');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(responsive)
                    {
                        if (responsive.status = RESPONSIVE_SUCCESS) {
                            $(contentData).prepend(responsive.data.row);
                            $('.styled').uniform();
                            $(modal).modal('hide');
                        }
                    }
                });

            });

            // get detail edit option
            let editOptionButon = '.js-edit-option';
            let modal =  $('#option-modal-edit');
            let fromBodyContent = $('#body-form-option-content');
            let editForm = $('#option-modal-edit-form');
            $(document).on('click', editOptionButon, function () {
                let url = $(this).attr('data-url');
                let urlSubmit = $(this).attr('data-url-submit');
                editForm.attr('action', urlSubmit);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(responsive)
                    {
                        if (responsive.status = RESPONSIVE_SUCCESS) {
                            fromBodyContent.html(responsive.data.form);
                            $('.styled').uniform();
                            $(modal).modal('show');
                        }
                    }
                });
            });

            // do update option
            let editFormButtonSubmit = $('.js-submit-edit-form');
            editFormButtonSubmit.click(function (e) {
                var url = editForm.attr('action');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: editForm.serialize(), // serializes the form's elements.
                    success: function(responsive)
                    {
                        if (responsive.status = RESPONSIVE_SUCCESS) {
                            let idItem = responsive.data.id;
                            let rowItem = $('#row-table-' + idItem);
                            rowItem.html(responsive.data.row);
                            modal.modal('hide');
                            $('.styled').uniform();
                        }
                    }
                });
            });



            // show modal confirm
            let deleteAjaxForm = $('#delete-form-ajax');
            let idItemInput = $('#id-item-delete');
            let showModalConfirmDelete = '.js-show-modal-confirm-delete';
            $(document).on('click', showModalConfirmDelete, function (e) {
                let url = $(this).attr('data-url');
                let id = $(this).attr('data-id');
                idItemInput.val(id);
                deleteAjaxForm.attr('action', url);
            });

            // delete option
            let deleteConfirmButton = $('#js-delete-item-ajax');
            let deleteModalAjax = $('#modal-delete-ajax');
            deleteConfirmButton.click(function () {
                let idItem = idItemInput.val();
                let rowItem = $('#row-table-' + idItem);
                let url = deleteAjaxForm.attr('action');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: deleteAjaxForm.serialize(), // serializes the form's elements.
                    success: function(responsive)
                    {
                        if (responsive.status = RESPONSIVE_SUCCESS) {
                            $('.styled').uniform();
                            rowItem.remove();
                            deleteModalAjax.modal('hide');
                        }
                    }
                });
            })
        })
    </script>
@endsection