<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-body">
                <input type="hidden" name="type" value="{{ $tab }}">
                <?php
                $inputData = [
                    'label' => 'Tên Tùy Chọn',
                    'inputType' => 'text',
                    'inputName' => 'name',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                @if(!empty($mappingOption))
                    @foreach ($mappingOption as $key => $mapping)
                        <?php
                        $configOption = array_get($postConfig, 'option.' . $key);
                        $items = [];
                        if (!empty($mapping['items'])) {
                            $items = $mapping['items'];
                            if ($mapping['inputType'] != 'checkboxInput') {
                                $items = getOptionSelectFromTable($mapping['items'], 'id', 'name');
                            }
                        }
                        $inputData = [
                            'label' => $configOption['name'],
                            'inputType' => $mapping['inputType'],
                            'inputName' => "mapping[{$key}]",
                            'permalink' => false,
                            'items' => $items,
                            'selected' => !empty($mappingSelect[$key]->option_post_to_id) ? $mappingSelect[$key]->option_post_to_id : null,
                        ]
                        ?>
                        @include ('admin.layout.include.form_input', $inputData)
                    @endforeach
                @endif

                @if (!empty($postConfig['option'][$tab]['content']))
                @foreach ($postConfig['option'][$tab]['content'] as $option)
                @include ('admin.layout.include.form_input', $option)
                @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                                   {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio-inline">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                                   {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
            </div>
        </div><!-- /panel -->
    </div>
</div>