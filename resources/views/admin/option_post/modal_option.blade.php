<!-- Modal add option -->
<div id="option-modal-{{ $tab }}" class="modal fade" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route($postType . '_option_add_ajax', ['postTyp' => $postType, 'type' => $tab]) }}"
                  method="post"
                  id="{{ $postType }}-option-form-{{ $tab }}">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Thêm Option</h5>
                </div>

                <div class="modal-body">
                    <?php
                        $mappingOptionInput = null;
                        if (!empty($mappingOption[$tab])) {
                            $mappingOptionInput = $mappingOption[$tab];
                        }
                    ?>
                    @include('admin.option_post.option_form_body', ['mappingOption' => $mappingOptionInput] )
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary js-submit-form"
                            data-content-row="#data-content-row-{{ $tab }}"
                            data-modal="#option-modal-{{ $tab }}"
                            data-content="#{{ $postType }}-option-form-{{ $tab }}">Thêm Mới
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Modal add option -->