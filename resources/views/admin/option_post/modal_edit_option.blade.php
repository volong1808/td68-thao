<!-- Modal add option -->
<div id="option-modal-edit" class="modal fade" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action=""
                  method="post"
                  id="option-modal-edit-form">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Cập Nhật Option</h5>
                </div>

                <div class="modal-body" id="body-form-option-content">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary js-submit-edit-form"
                            data-content-row="#data-content-row-{{ $tab }}"
                            data-modal="#option-modal-{{ $tab }}"
                            data-content="#{{ $postType }}-option-form-{{ $tab }}">Cập Nhật
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Modal add option -->