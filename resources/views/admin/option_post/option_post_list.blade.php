<?php
    $configOptionContent = [];
    if (!empty($config['content'])) {
        $configOptionContent = $config['content'];
    }
?>
<table class="table datatable-{{ $type }} table-striped">
    <thead>
    <tr>
        <th></th>
        <th>Tên {{ $config['name'] }}</th>
        @if (!empty($configOptionContent))
            @foreach($configOptionContent as $item)
                @if (!empty($item['isShowList']))
                    <th>{{ $item['label'] }}</th>
                @endif
            @endforeach
        @endif
        <th>Trạng Thái</th>
        <th>Vị Trí</th>
        <th>Thao Tác</th>
    </tr>
    </thead>
    <tbody id="data-content-row-{{ $type }}">
    @if (!empty($optionList))
        @foreach($optionList as $option)
            @include('admin.option_post.option_row')
        @endforeach
    @endif
    </tbody>
</table>
