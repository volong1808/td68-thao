<td><input type="checkbox" class="styled js-check" name="option_post[]" @if (!empty($post->id) && $option->post_id == $post->id) checked @endif value="{{ $option->id }}"></td>
<td><a href="#">{{ $option->name }}</a></td>
<?php $content = json_decode($option->content, true) ?>
@if (!empty($configOptionContent && !empty($content)))
    @foreach($configOptionContent as $item)
        @if (!empty($content[$item['inputName']]))
            <td>{{ $content[$item['inputName']] }}</td>
        @else
            <td>-</td>
        @endif
    @endforeach
@endif
<td>
    @if ($option->state == config('constants.POST_STATE.PUBLISHED'))
        <span class="label label-info">Hiển Thị</span>
    @else
        <span class="label label-danger">Không Hiển Thị</span>
    @endif
</td>
<td>
    <input type="number" name="order" data-id="{{$option->id }}" value="{{ $option->option_post_order }}" class="form-control input-sm">
</td>
<td>
    <button type="button" class="btn btn-sm btn-warning js-edit-option"
            data-modal="#option-modal-edit-{{ $tab }}" data-id="{{ $option->id }}" data-popup="tooltip"
            data-url="{{ route($postType . '_option_edit_ajax', ['postTyp' => $postType, 'type' => $tab, 'id' => $option->id]) }}"
            data-url-submit="{{ route($postType . '_option_edit_post_ajax', ['postTyp' => $postType, 'type' => $tab, 'id' => $option->id]) }}"
            title="Chỉnh Sửa">
        <i class="icon-pencil7"></i>
    </button>
    <button type="button" href="#" class="btn btn-sm btn-danger js-show-modal-confirm-delete" data-toggle="modal" title="Xoá"
            data-popup="tooltip"
            data-row="row-table-{{ $option->id }}"
            data-url="{{ route($postType . '_option_delete_ajax', ['postTyp' => $postType, 'type' => $tab]) }}"
            data-target="#modal-delete-ajax" data-id="{{ $option->id }}">
        <i class="icon-trash"></i>
    </button>
</td>