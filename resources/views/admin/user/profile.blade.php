@extends('admin.layout.admin')
<?php
/**
 * var $currentUser
 */
$currentUser = Auth::user();
?>
@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>{{ $title }}</h1>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li class="active">My Profile</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        @if (session('success'))
            <div class="alert alert-success alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session('error') }}
            </div>
        @endif
        @if (!empty($errors->all()))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                <p>{{ config('constants.FORM_ERROR_MESSAGE') }}</p>
            </div>
        @endif
        <form action="{{ route('update_profile') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
            <input type="hidden" name="username" value="{{ $initValue['username'] }}">
        <!-- Simple panel -->
            <div class="panel panel-flat">
                <div class="panel-body">
                <!-- table form -->
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group mb-0">
                                <?php
                                $imgsrc = empty($currentUser->avatar) ?  asset('backend/images/image.png') : asset($currentUser->image->url);
                                ?>
                                <label class="control-label">Hình Đại Diện</label>
                                <img src="{{ $imgsrc }}" class="img-responsive input-file__image" id="file-image" style="width: 100%">
                                <input type="file" name="avatar" id="file" class="input-file" data-cover="#file-image" />
                                <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
                                <p class="help-block">
                                    Kích thước tốt nhất {{ config('constants.product_image.WIDTH') }}
                                    x{{ config('constants.product_image.HEIGHT') }} (px)</p>
                            </div>
                        </div>
                        <div class="col-sm-8 col-xs-12">
                            <legend class="text-semibold">Thông Tin Cá Nhân</legend>
                            <?php
                            $inputData = [
                                'label' => 'Họ Và Tên',
                                'inputType' => 'text',
                                'inputName' => 'name',
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)

                            <?php
                            $inputData = [
                                'label' => 'Email',
                                'inputType' => 'text',
                                'inputName' => 'email',
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)

                            <?php
                            $inputData = [
                                'label' => 'Số Điện Thoại',
                                'inputType' => 'text',
                                'inputName' => 'phone',
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)

                            <legend class="text-semibold">Thông Tin Tài Khoản</legend>
                            <?php
                            $inputData = [
                                'label' => 'Mật Khẩu',
                                'inputType' => 'password',
                                'inputName' => 'password',
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)

                            <?php
                            $inputData = [
                                'label' => 'Mật Khẩu Mới',
                                'inputType' => 'password',
                                'inputName' => 'new_password',
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)

                            <?php
                            $inputData = [
                                'label' => 'Xác Nhận Mật Khẩu Mới',
                                'inputType' => 'password',
                                'inputName' => 'new_password_confirm',
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="heading-elements">
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
