<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <input type="hidden" name="id" value="{{ $initValue['id'] }}" />
                <?php
                $inputData = [
                    'label' => 'Name',
                    'inputType' => 'text',
                    'inputName' => 'name',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                @if (!empty($type) && $type)
                    <?php
                    $inputData = [
                        'label' => 'Username',
                        'inputType' => 'text',
                        'inputName' => 'username',
                        'permalink' => false,
                    ]
                    ?>
                        @include ('admin.layout.include.form_input', $inputData)
                @endif
                <?php
                $inputData = [
                    'label' => 'Email',
                    'inputType' => 'email',
                    'inputName' => 'email',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <?php
                $inputData = [
                    'label' => 'Phone',
                    'inputType' => 'text',
                    'inputName' => 'phone',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <div class="form-group">
                    <label class="text-semibold control-label" for="status-select">User role:</label>
                    <div class="form-control-static">
                        <div class="status-input">
                            <select class="select form-control" id="status-select" name="role">
                                <?php foreach (config('constants.role') as $key => $value): ?>
                                <option value="{{ $value }}" {{ $initValue['role'] == $value ? 'selected' : '' }}>{{ $key }}</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                @if (!empty($typePassword) && $typePassword)
                    <?php
                    $inputData = [
                        'label' => 'Mật Khẩu',
                        'inputType' => 'password',
                        'inputName' => 'user_password',
                        'permalink' => false,
                    ]
                    ?>
                    @include ('admin.layout.include.form_input', $inputData)
                    <?php
                    $inputData = [
                        'label' => 'Nhập Lại Mật Khẩu',
                        'inputType' => 'password',
                        'inputName' => 'user_password_confirmation',
                        'permalink' => false,
                    ]
                    ?>
                    @include ('admin.layout.include.form_input', $inputData)
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <?php
                    $imgsrc = empty($initValue['avatar']) ? asset(config('constants.post_image.PLACE_HOLDER')) : asset($initValue['avatar']->url)
                    ?>
                    <label class="control-label">Hình Đại Diện</label>
                    <img src="{{ $imgsrc }}" class="img-responsive input-file__image" id="file-image">
                    <input type="file" name="avatar" id="file" class="input-file" data-cover="#file-image" />
                    <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
                    <p class="help-block">
                        Kích thước tốt nhất {{ config('constants.post_image.WIDTH') }}
                        x{{ config('constants.post_image.HEIGHT') }} (px)</p>
                </div>
            </div>
        </div><!-- /panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="status" class="styled" value="1"
                                    {{ ($initValue['status'] == config('constants.USER_STATUS.ACTIVE')) ? 'checked' : '' }}>
                            Active
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="status" class="styled" value="0"
                                    {{ ($initValue['status'] == config('constants.USER_STATUS.INACTIVE')) ? 'checked' : '' }}>
                            InActive
                        </label>
                    </div>
                </div>
            </div>
            @if (!empty($type) && $type)
                <div class="panel-footer">
                    <input type="hidden" name="old-mail" value="{{ $initValue['email'] }}">
                    <div class="heading-elements">
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    </div>
                </div>
            @else
                <div class="panel-footer">
                    <input type="hidden" name="old-mail" value="{{ $initValue['email'] }}">
                    <div class="heading-elements">
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    </div>
                </div>
            @endif
        </div><!-- /panel -->
    </div>
</div>
