@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Upload File Excel</h1>
            </div>

            <div class="heading-elements"></div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li class="active">Upload</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('error') }}
                    </div>
                @endif
                @if (!empty($errors->all()))
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        <p>{{ config('constants.FORM_ERROR_MESSAGE') }}</p>
                    </div>
                @endif
                <div class="row">
                    <form class="" action="{{ route('admin_import_post', ['post_type' =>  $postType]) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-8 col-lg-offset-2">
                            <?php
                            $inputData = [
                                'label' => 'File Excel',
                                'inputType' => 'file',
                                'inputName' => 'file',
                                'permalink' => false,
                                'note' => 'Định dạng File: xls, xlsx. Max file size 5Mb'
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)
                            <?php
                            $inputData = [
                                'label' => 'File Hình Ảnh',
                                'inputType' => 'file',
                                'inputName' => 'file_image',
                                'permalink' => false,
                                'note' => 'Định dạng File: zip. Max file size 50M'
                            ]
                            ?>
                            @include ('admin.layout.include.form_input', $inputData)
                            <button type="submit" class="btn btn-primary btn-block">Import Data <i
                                        class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->
@endsection
