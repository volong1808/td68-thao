<form id="change-stage-form" action="{{ $url }}" method="post" >
    @csrf
    <?php
        $stageOrder = config('constants.ORDER_STATUS_LABEL');
    ?>
    <div id="modal-change-stage" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Thay Đổi Trạng Thái Đơn Hàng</h5>
                </div>

                <div class="modal-body">
                    <p>
                        Bạn có chắc chắn muốn thực hiện thay đổi trạng thái của [<span id="name-item"></span>]?
                    </p>
                    <div class="form-group">
                        <select class="form-control" name="status">
                            <option value="">Chọn trạng thái</option>
                            @foreach ($stageOrder as $key => $stage)
                                <option value="{{ $key }}">{{ $stage }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-link" data-dismiss="modal">Không</button>
                        <input type="hidden" name="code" value="">
                        <button type="submit" class="btn btn-warning">Có</button>
                </div>
            </div>
        </div>
    </div>
</form>
