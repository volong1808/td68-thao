<div class="panel panel-flat">
    <div class="panel-heading">
        <h4 class="panel-title">Thông Tin Về {{ $postTitle }}</h4>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            @foreach ($configForm as $inputData)
                @include ('admin.layout.include.form_input', $inputData)
            @endforeach
        </div>
    </div>
</div>
