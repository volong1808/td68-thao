<?php
/**
* var $currentUser
 */
$currentUser = Auth::user();
?>
<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('admin_index') }}">
            <img src="{{ asset('images/logo-admin.png') }}" alt="Logo">
        </a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            @if (! empty($currentUser))
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ empty($currentUser->avatar) ? asset('backend/images/image.png') : asset($currentUser->avatar) }}" alt="">
                    <span>{{ $currentUser->username }}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ route('my_profile') }}"><i class="icon-user-plus"></i> Profile</a></li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0)" id="logout" data-toggle="modal" data-target="#logoutModal">
                            <i class="icon-switch2"></i> Đăng Xuất
                        </a>
                    </li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>
            </li>
            @endif
        </ul>
    </div>
</div>
<!-- Logout Modal -->
<div class="bootbox modal fade bootbox-confirm in" id="logoutModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×
                </button>
                <h5 class="modal-title text-capitalize">Xác nhận</h5>
            </div>
            <div class="modal-body">
                <p>Bạn có muốn đăng xuất không?</p>
            </div>
            <form action="{{ route('logout') }}" method="POST">
                @csrf
                <div class="modal-footer">
                    <a data-bb-handler="cancel" type="button" data-dismiss="modal" class="btn btn-link">Không</a>
                    <button data-bb-handler="confirm" type="submit" class="btn btn-danger">Đăng Xuất</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Logout Modal -->
<!-- /main navbar -->
