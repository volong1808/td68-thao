<!-- panel -->
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-group mb-0">
            <label class="control-label">{{ $label }}</label>
            <img src="{{ $imgsrc }}" class="img-responsive input-file__image" id="file-{{ $name }}">
            <input type="file" name="{{ $name }}" id="file" class="input-file" data-cover="#file-{{ $name }}" />
            <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
            <p class="help-block">
                Kích thước tốt nhất {{ $size['width'] }}
                x{{ $size['height'] }} (px)</p>
        </div>
    </div>
</div><!-- /panel -->