<input type="email" class="form-control {{ $cssClass }}" name="{{ $inputName }}"
       value="{{ old($inputName, $initValue[$inputName]) }}"
       placeholder="{{ "Nhập {$label}" }}" id="input-{{ $inputName }}" {{ $eAttributes }}>