<div class="form-group row">
    <div class="col-lg-12">
        <div class="input-group">
            <label class="control-label">{{ $label }}</label>
            @foreach ($item as $value)
            <div class="checkbox">
                <label>
                    <div><span><input type="checkbox" id="{{ $value->type }}-{{ $value->id }}" class="styled" value="{{ $value->id }}" name="{{ $inputName }}" @if (is_array($selected) && in_array($value->id, $selected)) checked="checked" @endif></span></div>
                    {{ $value->name }}
                </label>
                <div class="checkbox">
                    <?php

                        $childItem = getMappingByFromId($value->id);
                        $toIds = getValuesByKey($childItem, 'option_post_to_id');
                        $childItem = getMappingByKeyAndToIds($toIds);
                        $contentItem = [];

                        if (!empty($formChildContent[$value->id]) && !empty($post->id)) {
                            $contentItem = json_decode($formChildContent[$value->id], true);
                        }
                    ?>
                    @if (!empty($childItem))
                        @foreach ($childItem as $child)
                            <?php
                                $checkedList = [];
                                if (!empty($contentItem[$child->type])) {
                                    $checkedList = $contentItem[$child->type];
                                }
                            ?>
                            <div class="ml-1">
                                <div class="checkbox">
                                    <label>
                                        <div><span><input type="checkbox" id="child-item-{{ $child->id }}"
                                                          class="styled" value="{{ $child->id }}"
                                                          @if (in_array($child->id, $checkedList)) checked @endif
                                                          name="post_option_post_content[{{ $value->id }}][{{ $child->type }}][]"></span></div>
                                        {{ $child->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
