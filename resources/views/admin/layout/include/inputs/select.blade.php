<select name="{{ $inputData['inputName'] }}" class="form-control">
    @foreach ($item as $id => $value)
        <?php
        $isSelect = false;
        if ((!is_array($selected) && $selected == $id) || (is_array($selected) && in_array($id, $selected))) {
                $isSelect = true;
        }
        ?>
        <option value="{{ $id }}" @if ($isSelect) selected @endif >{{ $value }}</option>
    @endforeach
</select>