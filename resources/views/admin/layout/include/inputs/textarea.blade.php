<textarea name="{{ $inputName }}" rows="3" cols="3" class="form-control {{ $cssClass }}" {{ $eAttributes }}
placeholder="{{ "Nhập {$label}" }}">{{ old($inputName, $initValue[$inputName]) }}</textarea>