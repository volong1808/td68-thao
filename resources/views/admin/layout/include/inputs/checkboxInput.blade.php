@foreach ($item as $value)
    <div class="form-group row">
        <div class="col-lg-12">
            <div class="input-group">
                <span class="input-group-addon">
                    <input type="checkbox" class="styled" value="{{ $value->id }}" name="{{ $inputName }}" @if (is_array($selected) && in_array($value->id, $selected)) checked="checked" @endif>
                </span>
                @if (!empty($formChildConfig))
                    <?php
                    $valueContent = [];
                    if (!empty($formChildContent[$value->id])) {
                        $valueContent = json_decode($formChildContent[$value->id], true);
                    }
                    ?>
                    <div class="content-input-group">
                        @foreach ($formChildConfig as $inputChild)
                            <div class="">
                                <input type="text"
                                       name="mapping_content[{{ $value->id }}][{{ $inputChild['inputName'] }}]"
                                       class="form-control"
                                       value="{{ (!empty($valueContent) && !empty($valueContent[$inputChild['inputName']])) ? $valueContent[$inputChild['inputName']] : '' }}"
                                       placeholder=" Nhập {{ $inputChild['label'] }} cho {{ $label }} ({{ $value->name }})">
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endforeach