@foreach($item as $key => $value)
    <div class="radio-inline">
        <label>
            <input type="radio" name="{{ $inputName }}" class="styled"
                   value="{{ $key }}"
                    {{ $initValue[$inputName] == $key ? 'checked'  : '' }}>
            {{ $value }}
        </label>
    </div>
@endforeach