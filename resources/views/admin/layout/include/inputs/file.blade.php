<div class="uploader">
    <input type="file" name="{{ $inputName }}" class="file-styled {{ $cssClass }}"  {{ $eAttributes }} id="input-{{ $inputName }}">
    <span class="help-block">Accepted formats: {{ $note }}</span>
</div>