<input type="password" class="form-control" name="{{ $inputName }}"
       value="{{ old($inputName, $initValue[$inputName]) }}"
       id="input-{{ $inputName }}">