<?php
    $image = null;
    if (!empty($initValue[$inputName])) {
        $image = getImageById($initValue[$inputName]);
    }
?>
<div class="form-group mb-0">
    <img src="{{ !empty($image->origin) ? asset($image->origin) : '' }}" class="img-responsive input-file__image" id="file-{{ $inputName }}">
    <input type="file" name="{{ $inputName }}" id="file" class="input-file" data-cover="#file-{{ $inputName }}" />
    <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
    <p class="help-block">
        Kích thước tốt nhất {{ $size['origin']['width'] }}
        x{{ $size['origin']['height'] }} (px)</p>
</div>