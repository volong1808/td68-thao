@foreach($item as $key => $value)
    <?php
        $isSelect = false;
        if ((!is_array($selected) && $selected == $key) || (is_array($selected) && in_array($key, $selected))) {
            $isSelect = true;
        }
    ?>
    <div class="checkbox-inline">
        <label>
            <input type="checkbox" name="{{ $inputName }}" class="styled"
                   value="{{ $key }}"
                    {{ $isSelect ? 'checked'  : '' }}>
            {{ $value }}
        </label>
    </div>
@endforeach