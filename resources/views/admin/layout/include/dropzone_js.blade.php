<script>
    Dropzone.autoDiscover = false;
    var galleryDropzone = new Dropzone("div#galleries-dropzone", {
        url: "{{ route('add_gallery_ajax') }}",
        paramName: 'image',
        method: 'post',
        dictDefaultMessage: "Kéo thả file hoặc click để tải hình ảnh lên",
        maxFilesize: 2,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        dictFileTooBig: "Kích thước File quá lớn (tối đa 2MiB).",
        dictRemoveFile: "Xoá",
        init: function () {
            this.on("sending", function(file, xhr, formData) {
                var postId = $("div#{{ $dropzoneData['dropzoneId'] }}").attr('data-post-id');
                var postType = $("div#{{ $dropzoneData['dropzoneId'] }}").attr('data-post-type');
                formData.append("postId", postId);
                formData.append("postType", postType);
            });
            @if (isset($initDzData))
                var initImages = {!! json_encode($gallery, JSON_HEX_TAG) !!};
                var myDropZone = this;
                $.each(initImages, function (i, image) {
                    var mockFile = {
                        name: image.file_name,
                        size: image.size,
                    };
                    myDropZone.options.addedfile.call(myDropZone, mockFile);
                    myDropZone.options.thumbnail.call(myDropZone, mockFile, '/' + image.thumb);
                    $(myDropZone.element.lastChild.lastChild).attr('data-dz-remove', image.id);
                });
            @endif
        },
        success: function (a) {
            response = $.parseJSON(a.xhr.response);
            $('.dz-remove').each(function (i, el) {
                if ($(el).attr('data-dz-remove') == '') {
                    $(el).attr('data-dz-remove', response.image_id);
                    @if (isset($hasMockItem) && $hasMockItem)
                    $(el).parent().append('<input type="hidden" name=imageList[] value="' + response.image_id +'">');
                    @endif
                }
            });
        },
        removedfile: function (a) {
            var removeBtn = $($(a.previewElement)[0].lastChild);
            var imageId = removeBtn.attr('data-dz-remove');
            var postId = $("div#{{ $dropzoneData['dropzoneId'] }}").attr('data-post-id');
            $.ajax({
                url: "{{ $removeRoute }}",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    'postId': postId,
                    'imageId': imageId,
                },
                success: function (response) {
                    var b;
                    return a.previewElement && null != (b = a.previewElement) && b.parentNode.removeChild(a.previewElement);
                },
            });
        }
    });
    $(document).ready(function () {
        submitBtn.on('mouseleave', function () {
            setBeforUnload();
            $(window).on('unload', function () {
                $imageIds = [];
                $('.dz-remove').each(function (i, el) {
                    $imageIds.push($(el).attr('data-dz-remove'));
                });
                $.ajax({
                    url: '{{ route('clear_gallery_ajax') }}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        'imageIds': $imageIds,
                    },
                    async: false,
                    success: function (response) {}
                });
            });
        });
    });

    $(window).on('unload', function () {
        $imageIds = [];
        $('.dz-remove').each(function (i, el) {
            $imageIds.push($(el).attr('data-dz-remove'));
        });
        $.ajax({
            url: '{{ route('clear_gallery_ajax') }}',
            type: 'post',
            dataType: 'json',
            data: {
                'imageIds': $imageIds,
            },
            async: false,
            success: function (response) {}
        });
    });
</script>
