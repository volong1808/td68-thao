<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <?php
                    $currentRoute = Route::current()->getName();
                    $currentUrl = url()->current();
                    $customPost = config('custom_post');
                    ?>
                    <li @if($currentRoute == 'admin_index') class="active" @endif>
                        <a href="{{ route('admin_index') }}">
                            <i class="icon-home4"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    @foreach($customPost as $post)
                        <li @if(in_array($currentRoute, ['product_edit']))class="active"@endif>
                            <a href="#"><i class="{{ $post['icon'] }}"></i> <span>{{ $post['title'] }}</span></a>
                            <ul>
                                <li @if($currentRoute == $post['post_type'] . '_list') class="active" @endif><a href="{{ route($post['post_type'] . '_list', $post['post_type']) }}">Tất Cả {{ $post['title'] }}</a></li>
                                <li @if($currentRoute == $post['post_type'] . '_add') class="active" @endif><a href="{{ route($post['post_type'] . '_add', $post['post_type']) }}">Thêm Mới {{ $post['title'] }}</a></li>
                                @if (!empty($post['category']))
                                    <li @if(strpos($currentRoute, $post['post_type'] . '_category') !== false) class="active" @endif><a href="{{ route($post['post_type'] . '_category_list', $post['post_type']) }}">Danh Mục</a></li>
                                @endif
                                @if (!empty($post['option']))
                                    @foreach ($post['option'] as $keyOption => $option)
                                        <li @if(strpos($currentRoute, $post['post_type'] . '_option') !== false) class="active" @endif><a href="{{ route($post['post_type'] . '_option_list', [$post['post_type'], $keyOption]) }}">Tùy Chọn {{ $option['name'] }}</a></li>
                                    @endforeach
                                @endif
                                @if (!empty($post['plugin']))
                                    @foreach ($post['plugin'] as $keyOption => $plugin)
                                        <li @if(strpos($currentRoute, $post['post_type'] . '_option') !== false) class="active" @endif><a href="{{ route($plugin['route'], $plugin['option']) }}">{{ $plugin['name'] }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                    @endforeach
                    <li @if(in_array($currentRoute, ['admin_order']))class="active"@endif>
                        <a href="{{ route('admin_order') }}"><i class="icon-cash4"></i> <span>Đơn Hàng</span></a>
                    </li>
                    <li @if($currentRoute == 'admin_contacts') class="active" @endif>
                        <a href="{{ route('admin_contacts') }}">
                            <i class="icon-envelop2"></i> <span>Liên Hệ</span>
                        </a>
                    </li>
                    <!-- /main -->
                    <?php $routeConfig =  route('config_website', config('constants.company_config.key')); ?>
                    <li @if($currentUrl == $routeConfig) class="active" @endif>
                        <a href="{{ $routeConfig }}">
                            <i class="icon-info22"></i> <span>Thông Tin Công Ty</span>
                        </a>
                    </li>
                    <?php $routeConfig =  route('config_website', config('constants.banking_config.key')); ?>
                    <li @if($currentUrl == $routeConfig) class="active" @endif>
                        <a href="{{ $routeConfig }}">
                            <i class="icon-credit-card"></i> <span>Thanh Toán Ngân Hàng</span>
                        </a>
                    </li>
                    <?php $routeConfig =  route('config_website', config('constants.plugin_config.key')); ?>
                    <li @if($currentUrl == $routeConfig) class="active" @endif>
                        <a href="{{ $routeConfig }}">
                            <i class="icon-bell-plus"></i> <span>Script Plugins</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
<!-- /main sidebar -->
