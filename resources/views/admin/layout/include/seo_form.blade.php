<div class="panel panel-flat">
    <div class="panel-heading">
        <h4 class="panel-title">Thông Tin Về SEO</h4>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <?php
                $inputData = [
                    'label' => 'Tiêu Đề SEO',
                    'inputType' => 'text',
                    'inputName' => 'seo_name',
                    'classes' => 'google-seo-preview',
                    'attributes' => ['data-target-preview' => '#seo-preview__title']
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <?php
                $inputData = [
                    'label' => 'Mô tả ngắn SEO',
                    'inputType' => 'textarea',
                    'inputName' => 'seo_description',
                    'classes' => 'google-seo-preview',
                    'attributes' => ['data-target-preview' => '#seo-preview__desc']
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
            </div>
            <div class="col-md-5 col-sm-8">
                <div class="seo-preview">
                    <h3 class="seo-preview__title" id="seo-preview__title">
                        @if (empty($initValue['seo_name']))
                            Bạn chưa có tiêu đề hiển thị trên Google Search. Vui lòng nhập để SEO tốt hơn.
                        @else
                            {{ old('seo_name', $initValue['seo_name']) }}
                        @endif
                    </h3>
                    <p class="seo-preview__desc" id="seo-preview__desc">
                        @if (empty($initValue['seo_description']))
                            Bạn chưa có mô tả ngắn hiển thị trên Google Search. Vui lòng nhập để SEO tốt hơn.
                        @else
                            {{ old('seo_description', $initValue['seo_description']) }}
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
