<script>
    $(document).ready(function(){
        var changeDisplayInput = $('.google-seo-preview');
        changeDisplayInput.on('input', function () {
            var contentDisplayId = $(this).attr('data-target-preview');
            var inputValue = $(this).val();
            $(contentDisplayId).html(inputValue);
        });
    });
</script>
