<form id="delete-form-ajax" action="" method="post" >
    @csrf
    <div id="modal-delete-ajax" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Xác Nhận Xóa</h5>
                </div>

                <div class="modal-body">
                    <p>
                        Bạn có chắc chắn muốn thực hiện xóa? Tất cả dữ liệu sẽ bị xóa và không thể
                        khôi phục lại được.
                    </p>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-link" data-dismiss="modal">Không</button>
                        <input id="id-item-delete" type="hidden" name="itemId" value="">
                        <button type="button" id="js-delete-item-ajax" class="btn btn-danger">Tôi muốn xóa</button>
                </div>
            </div>
        </div>
    </div>
</form>
