<?php
use App\Category;

$product = Category::whereNull('parent_id')
    ->where('post_type', 'product')->get();

$tutorials = Category::whereNull('parent_id')
    ->where('post_type', 'tutorial')->get();

$currentRoute = Route::current()->getName();
$currentSlug = \Illuminate\Support\Facades\Request::segment(1);

?>
<!-- Header -->
<header>
    <div class="container">
        <div class="header-content">
            <div class="header-content__left">
                <a class="header__logo" href="{{ url('/') }}">
                    <img src="{{ asset('images/logo.png') }}" height="40" alt="logo">
                </a>
                <a id="hamburger" href="#menu"><span></span></a>
            </div>
            <div class="header-content__right">
                <div class="header-content__search">
                    <form class="form-search" action="{{ route('search_post') }}" method="GET">
                        <input name="key" class="form-control" type="search" placeholder="Nhập từ khóa tìm kiếm..."
                               aria-label="Search" required>
                        <button class="btn" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </form>
                </div>

                <div class="header-content__contact">
                    <a href="tel:0123456789">0123456789</a>
                    <p>hotline 24/7</p>
                </div>
                <div class="header-content__tutorial">
                    <a href="{{ url(config('custom_post.tutorial.pageSlug')) }}">Tin Tức & Thủ Thuật</a>
                </div>
            </div>
        </div>
    </div>
    <!-- .menu -->
    <section class="menu-container">
        <div class="container">
            <div class="menu-content">
                <nav class="navigation" role="navigation">
                    <ul>
                        <li class="@if($currentRoute == 'home') active @endif"><a href="{{ url('/') }}"> Trang chủ</a>
                        </li>
                        <li class="@if($currentSlug == config('custom_post.product.pageSlug')) active @endif">
                            <a href="{{ url( config('custom_post.product.pageSlug')) }}">Sản Phẩm</a>
                            @if(!empty($product) && count($product) > 0)
                                <ul>
                                    @foreach ($product as $productItem)
                                        <?php

                                        $children = $productItem->children()->get();

                                        ?>
                                        @if (count($children) === 0)
                                            <li>
                                                <a href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => $productItem->slug]) }}">{{ $productItem->name }}</a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => $productItem->slug]) }}">{{ $productItem->name }}</a>
                                                <ul>
                                                    @foreach ($children as $child)
                                                        <li>
                                                            <a href="{{ route('category_slug', ['page-slug' => config('custom_post.product.pageSlug'), 'cate_slug' => $child->slug]) }}">{{ $child->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                        <li class="@if($currentSlug == config('custom_post.tutorial.pageSlug')) active @endif">
                            <a href="{{ url( config('custom_post.tutorial.pageSlug')) }}">Tin Tức - Thủ Thuật</a>
                            @if(!empty($tutorials) && count($tutorials) > 0)
                                <ul>
                                    @foreach ($tutorials as $tutorial)
                                        <?php

                                        $children = $tutorial->children()->get();

                                        ?>
                                        @if (count($children) === 0)
                                            <li>
                                                <a href="{{ route('category_slug', ['page-slug' => config('custom_post.tutorial.pageSlug'), 'cate_slug' => $tutorial->slug]) }}">{{ $tutorial->name }}</a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="{{ route('category_slug', ['page-slug' => config('custom_post.tutorial.pageSlug'), 'cate_slug' => $tutorial->slug]) }}">{{ $tutorial->name }}</a>
                                                <ul>
                                                    @foreach ($children as $child)
                                                        <li>
                                                            <a href="{{ route('category_slug', ['page-slug' => config('custom_post.tutorial.pageSlug'), 'cate_slug' => $child->slug]) }}">{{ $child->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                        <li>
                            <a href="{{ url('gioi-thieu') }}">GIỚI THIỆU</a>
                        </li>
                        <li>
                            <a href="{{ url('lien-he') }}">LIÊN HỆ</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
</header>