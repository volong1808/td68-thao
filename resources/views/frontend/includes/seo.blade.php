@isset($seoData)
<meta name="title" content="{{ $seoData->name }}">
<meta name="description" content="{{ $seoData->description }}">
<meta property="og:title" content="{{ $seoData->name }}">
<meta property="og:description" content="{{ $seoData->description }}">
@isset ($seoData->image)
<meta property="og:image" content="{{ asset($seoData->image->thumb) }}">
@endisset
@endisset
<link rel="canonical" href="{{ \Illuminate\Support\Facades\URL::current() }}"/>
@isset($ogType)
<meta property="og:type" content="{{ $ogType }}">
@endisset
<meta property="og:url" content="{{ \Illuminate\Support\Facades\Request::url() }}">
