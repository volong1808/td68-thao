<ul class="dm_c2" id="sub-cate-{{ $cate->id }}">
    @foreach($subCateItem->subCate as $item)
    <li><a class="" href="{{ route('category_slug', ['page' => $page->slug, 'category_slug' => $item->slug]) }}">{{ $item->name }}</a></li>
    @endforeach
</ul>