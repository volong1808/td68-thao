@if ($pagination->lastPage() > 1)
<div class="paging-block">
    <ul>
        <?php
            $currentRoute = \Illuminate\Support\Facades\Route::currentRouteName();
            $maxShowPage = config('constants.MAX_PAGE_SHOW');
            $pageStart = $pagination->currentPage();
            if ($pageStart > 1) {
                $pageStart = $pageStart - 1;
            }
            $maxShowPage = $maxShowPage + $pageStart;
            if ($maxShowPage > $pagination->lastPage()) {
                $maxShowPage = $pagination->lastPage();
            }
            $prevPage = $pagination->currentPage() - 1;
            if ($prevPage < 1) {
                $prevPage = 1;
            }
            $nextPage = $pagination->currentPage() + 1;
            if ($nextPage > $pagination->lastPage()) {
                $nextPage = $pagination->lastPage();
            }
        ?>
        <li><a href="{{ route($currentRoute, ['page' => $prevPage]) }}" class="prev"><span class="icon-left-arrow"></span></a></li>
        @for ($i = $pageStart; $i <= $maxShowPage; $i++)
            <li @if($i == $pagination->currentPage()) class="active" @endif>
                <a href="{{ route($currentRoute, ['page' => $i]) }}">{{ $i }}</a>
            </li>
        @endfor
        @if ($maxShowPage < $pagination->lastPage())
            <li><a href="#" class="dots"><span class="icon-paging-dots"></span></a></li>
            <li><a href="{{ route($currentRoute, ['page' => $pagination->lastPage()]) }}">{{ $pagination->lastPage() }}</a></li>
        @endif
        <li><a href="{{ route($currentRoute, ['page' => $nextPage]) }}" class="next"><span class="icon-right-arrow"></span></a></li>
    </ul>
</div>
@endif
