<?php
$support = getCustomPost('support', 3);
$policy = getCustomPost('policy', 3);
?>
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="footer-item footer-info col-md-3 col-sm-6 col-12">
                <div class="footer-item__title">Thông tin</div>
                <div class="footer-item__content">
                    <p class="mb-2"><strong>TD68 Store</strong></p>
                    <p class="mb-0"><span>Tel/Zalo: {{ $companyInfo['phone'] ?? '0123456789' }}</span></p>
                    <p class="mb-0"><span>Email: {{ $companyInfo['email'] ?? 'example@mail.com' }}</span></p>
                </div>
            </div>
            <div class="footer-item col-md-3 col-sm-6 col-12">
                <div class="footer-item__title">Chính Sách Mua Hàng</div>
                <div class="footer-item__content">
                    @if(count($policy) > 0)
                        <ul>
                            @foreach($policy as $item)
                                <li><a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.policy.pageSlug'), 'post_slug' => $item->slug]) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
            <div class="footer-item col-md-3 col-sm-6 col-12">
                <div class="footer-item__title">Hỗ Trợ</div>
                <div class="footer-item__content">
                    <ul>
                        @foreach($support as $item)
                            <li><a href="{{ route('fr_post_detail', ['page_slug' => config('custom_post.support.pageSlug'), 'post_slug' => $item->slug]) }}">{{ $item->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="footer-item footer-social col-md-3 col-sm-6 col-12">
                <div class="footer-item__title">Social</div>
                <div class="footer-item__content">
                    <a class="facebook" href="{{ $companyInfo['facebook'] ?? 'https://www.facebook.com' }}" target="_blank"><img src="{{ asset('images/facebook.png') }}" alt=""></a>
                    <a class="instagram" href="{{ $companyInfo['instagram'] ?? 'https://www.instagram.com' }}" target="_blank"><img src="{{ asset('images/itram.jpg') }}" alt=""></a>
                    <a class="youtube" href="{{ $companyInfo['youtube'] ?? 'https://www.youtube.com' }}" target="_blank"><img src="{{ asset('images/youtube.jpeg') }}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
    <div id="copy-right">
        <p>© Bản quyền thuộc về <a href="{{ url('/') }}"><strong>Sam Media</strong></a></p>
    </div>
</footer>
<!-- End Footer -->