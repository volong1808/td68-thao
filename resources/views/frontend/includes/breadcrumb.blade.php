<div class="banner-default">
    <div class="container">
        <h3>{{ !empty($breadcrumb['active']) ?  $breadcrumb['active'] : $page->name}}</h3>
        <ul class="breadcumb">
            <li><a href="{{ route('home') }}">Trang chủ</a></li>
            <li><span class="sep"><i class="fa fa-angle-right"></i></span></li>
            <li><a href="{{ route('page', $page->slug) }}">{{ $page->name }}</a></li>
            @if (!empty($breadcrumb['categoryOfPost']))
                @foreach ($breadcrumb['categoryOfPost'] as $breadcrumbItem)
                    <li><span class="sep"><i class="fa fa-angle-right"></i></span></li>
                    <li><a href="{{ route('category_slug', ['page_slug' => $page->slug, 'cate_slug' => $breadcrumbItem->slug]) }}">{{ $breadcrumbItem->name }}</a></li>
                @endforeach
            @endif
            @if (!empty($breadcrumb['active']))
                <li><span class="sep"><i class="fa fa-angle-right"></i></span></li>
                <li>{{ $breadcrumb['active'] }}</li>
            @endif
        </ul>
    </div><!-- /.container -->
</div>