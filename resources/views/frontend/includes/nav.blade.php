<!-- menu header -->
<div id="menu_top">

    <nav id="menu_mobi" style="height:0; overflow:hidden;"></nav>
    <div class="header">
        <a href="#menu_mobi" class="hien_menu">
            <i class="icofont-listine-dots"></i>
        </a>
        <a class="logo_mobi"  href="{{ route('home') }}"> <img src="{{ $company['logo'] }}" alt=""></a>
    </div>

    <div class="menu_pc container" >
        <nav id="menu">
            <ul>
                <li class="menu_c1"><a class="active" href="{{ route('home') }}">Home <i class="icofont-home"></i></a></li>
                @foreach ($pageMenu as $page)
                    <li class="menu_c1"><a class="" href="{{ route('page', $page->slug) }}">{{ $page->name }} <i class="{{ $page->icon_menu }}"></i> </a>
                        @if (!empty($page->category))
                            <ul class="dm_c1">
                                @foreach($page->category as $cate)
                                    <li>
                                        <a class="" href="{{ route('category_slug', ['page_slug' => $page->slug, 'cate_slug' => $cate->slug]) }}">{{ $cate->name }} @if (!empty($cate->subCate))<i class="icofont-rounded-right"></i>@endif</a>
                                        @if ($cate->subCate)
                                            @include('frontend.includes.sub_cate_post_list', ['subCateItem' => $cate])
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
            <div class="clear"></div>
        </nav>
    </div>

</div>
<!-- end menu header -->