<section class="inner-banner contact-banner"
         @if (!empty($bannerImage)) style="background: url('{{ asset($bannerImage) }}')" @endif>
    <div class="container">
        <div class="contents">
            <h1>{{ !empty($title) ? $title : '' }}</h1>
            @if (!empty($description))
            <p>{{ $description }}</p>
            @endif
        </div>
    </div>
</section>
