<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title_head', 'Trang Chủ')</title>

    <!-- favicon -->
    @include('include.favicon')
    @include('frontend.includes.seo')

    <link rel="stylesheet" type="text/css" href="{{ asset('icofont/icofont.css') }}" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('assets/select2/css/select2.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Iconmoon -->
    <link href="{{ asset('assets/iconmoon/css/iconmoon.css') }}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.min.css') }}" rel="stylesheet">
    <!-- Video Popup -->
    <link href="{{ asset('assets/magnific-popup/css/magnific-popup.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/magiczoomplus.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
    <link rel="stylesheet" href=" {{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hover.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    @yield('css_page')
    <link href="{{ asset('css/main_gonnhe.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    @if (!empty($plugin['google_analytics']))
        {!! $plugin['google_analytics'] !!}
    @endif
</head>
<body>
<div id="wrapper">
    <!-- ==============================================
    **Header**
    =================================================== -->
    @include('frontend.includes.header')
    <!-- ==============================================
    **Content opt1**
    =================================================== -->
    <main id="content">
        @yield('content')
    </main>
    <!-- ==============================================
    **Footer opt1**
    =================================================== -->
    @include('frontend.includes.footer')

    @include('frontend.includes.header_sp')
</div>
@if (!empty($plugin['chat_box']))
    {!! $plugin['chat_box'] !!}
@endif

<a id="scrollTop" href="javascript:void(0)"></a>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Popper JS -->
<script src="{{ asset('js/popper.min.js') }}"></script>
<!-- Bootsrap JS -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Select2 JS -->
<script src="{{ asset('assets/select2/js/select2.min.js') }}"></script>
<!-- Owl Carousal JS -->
<script src="{{ asset('js/vendor/owl.carousel.min.js') }}"></script>
<!-- Menu JS -->
<script src="{{ asset('js/jquery.mmenu.min.all.js') }}"></script>

<script src="{{ asset('js/vendor/magiczoomplus.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('js/main_gonnhe.js') }}"></script>
@yield('script_page')
</body>
</html>
