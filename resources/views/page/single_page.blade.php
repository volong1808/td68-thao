@extends('frontend.layouts.master')
@section('title_head', $page->name)
@section('content')
    <div class="content list-page">
    @include('frontend.includes.breadcrumb')
    <!-- diy -->
        <div class="container">
            <div class="post-detail__content">
                {!! $page->content !!}
            </div>
            <div class="inc-info">
                <p>Mọi chi tiết xin liên hệ với chúng tôi, thông qua:</p>
                @if(!empty($companyInfo['address']))
                <p>Địa chỉ: {{ $companyInfo['address'] }}</p>
                @endif
                @if(!empty($companyInfo['facebook']))
                <p>Fanpage: <a href="{{ $companyInfo['facebook'] }}" target="_blank">{{ $companyInfo['facebook'] }}</a>
                </p>
                @endif
                <p>Website: <a href="{{ url('/') }}" target="_blank">{{ url('/') }}</a>
                </p>
            </div>
        </div>
    </div>
    <!-- /diy -->
@endsection