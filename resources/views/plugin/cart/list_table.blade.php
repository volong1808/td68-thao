<table id="cart-table" class="table table-bordered table-responsive d-md-table">
    <tbody>
    <tr>
        <td>Xóa</td>
        <td>Hình ảnh </td>
        <td>Tên</td>
        <td>Tùy Chọn</td>
        <td>Đơn giá</td>
        <td>Số lượng</td>
        <td>Thành tiền</td>
    </tr>
    <?php $total = 0; ?>
    @foreach($cart as $item)
        <?php
        $product =  $item['item'];
        $image = 'images/product/product-1.jpg';
        if (!empty($product->image)) {
            $image = $product->image->thumb;
        }

        $price = $product->{$product->post_type}->price_default;

        $option = $item['option'];
        foreach ($option as $OptionItem) {
            $priceContent = json_decode($OptionItem->content, true);
            if (!empty($priceContent)) {
                $price = $priceContent['price'];
            }
        }
        $totalItem = $item['quantity']*$price;
        $total = $total + $totalItem;
        ?>
        <tr>
            <td><a href="{{ route('fr_delete_order', $product->id) }}">Xóa</a></td>
            <td><img width="50" src="{{ asset($image) }}" alt="{{ $product->name }}"></td>
            <td>{{ $product->name }}</td>
            <td>
                @foreach($option as $optionName => $OptionItem)
                    <p>{{ $item['option_config'][$optionName]['name'] }}: {{$OptionItem->name}}</p>
                @endforeach
            </td>
            <td>
                <p>{{ $price }}</p>
            </td>
            <td><input class="js-update-quantity" data-action="{{ route('fr_change_quantity_order_ajax', $product->id) }}" type="number" name="" value="{{ $item['quantity'] }}" maxlength="5" min="1" size="2"> </td>
            <td>
                {{ formatMoneyVI($totalItem) }}
            </td>
        </tr>
    @endforeach
    <tr>
        <td colspan="6">Tổng tiền</td>
        <td>{{ formatMoneyVI($total) }}</td>
    </tr>
    </tbody>
</table>