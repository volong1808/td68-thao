@extends('frontend.layouts.master')
@section('title_head', isset($title) ? $title : '')
@section('content')
<div class="content list-page">
    <div class="container">
        <div class="title-list__wrap">
            <div class="title-list__left">
                <h2 class="title-list">Giỏ hàng</h2>
            </div>
        </div>
        <div class="cart__content">
            <div class="row">
                @if (!empty($cart))
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3>Thông tin giỏ hàng</h3>
                    <div id="table-content">
                        @include('plugin.cart.list_table')
                    </div>

                </div>
                <div class="col-lg-8 offset-lg-2 col-md-12 col-sm-12 col-12">
                    <h3>Thông tin khách hàng</h3>
                    <?php
                        $paymentMethod = config('option.banking');
                        $userLogin = Auth::user();
                    ?>
                    <form method="post" name="order_form" id="order_form" action="{{ route('checkout_cart') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-item">
                            <select name="payment_type">
                                @foreach($paymentMethod as $key => $item)
                                    <option value="{{ $key }}">{{ $item['label'] }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('payment_type'))
                                <label class="help__text">{{ $errors->first('payment_type') }}</label>
                            @endif
                        </div>
                        <div class="form-item">
                            <input placeholder="Họ tên" name="name" type="text" id="name" value="">
                            @if ($errors->has('name'))
                                <label class="help__text">{{ $errors->first('name') }}</label>
                            @endif
                        </div>
                        <div class="form-item">
                            <input placeholder="Điện thoại" name="phone" id="phone" value="" type="text">
                            @if ($errors->has('phone'))
                                <label class="help__text">{{ $errors->first('phone') }}</label>
                            @endif
                        </div>
                        <div class="form-item">
                            <input placeholder="Địa chỉ giao hàng" name="address" type="text" id="address" value="">
                            @if ($errors->has('address'))
                                <label class="help__text">{{ $errors->first('address') }}</label>
                            @endif
                        </div>
                        <div class="form-item">
                            <input placeholder="Email" name="email" type="email" id="email" value="">
                            @if ($errors->has('email'))
                                <label class="help__text">{{ $errors->first('email') }}</label>
                            @endif
                        </div>
                        <div class="form-item">
                            <textarea placeholder="Ghi chú" name="message" cols="50" rows="4"></textarea>
                            @if ($errors->has('message'))
                                <label class="help__text">{{ $errors->first('message') }}</label>
                            @endif
                        </div>
                        <div class="form-item text-center">
                            <input type="button" value="Mua thêm" onclick="window.location.href='/'">
                            <input type="submit" value="Đặt hàng">
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('script_page')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('change', '.js-update-quantity', function () {
                let quantity = $(this).val();
                let action = $(this).attr('data-action');
                $.ajax({
                    type: "POST",
                    url: action,
                    data: {quantity: quantity}, // serializes the form's elements.
                    success: function(responsive)
                    {
                        console.log(responsive);
                        if (responsive.status = true) {
                            $('#table-content').html(responsive.table);
                        }
                    }
                });
            })
        })
    </script>
@endsection