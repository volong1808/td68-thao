@extends('frontend.layouts.master')
@section('css_page')
    @include('include.reglog_style')
@endsection
@section('content')
    <div class="container wrap-reglog" >
        <div class="row">
            <div id="login-form" class="reglog login-area text-center active">

                <h2>Đăng nhập</h2>
                <h3>Đăng nhập bằng mạng xã hội</h3>
                <ul class="list-inline social-icons text-center">
                    <li class="d-inline-block"><a href="{{ url('redirect/facebook') }}" class="fb-icon"><i class="fa fa-facebook-f"></i> Facebook</a></li>
                    <li class="d-inline-block"><a href="{{ url('redirect/google') }}" class="gmail-icon"><i class="fa fa-google"></i> Gmail</a></li>
                </ul>
                <p>hoặc</p>
                <h3>Đăng nhập bằng tài khoản</h3>
                <form class="form form-dang-nhap" action="{{ route('login_user_post') }}" method="post">
                    @if(session('errorLogin'))
                        <div class="alert alert-danger">
                            <strong>Lỗi!</strong> {{ session('errorLogin') }}
                        </div>
                    @endif
                    @if(session('resetSuccess'))
                        <div class="alert alert-success">
                            <strong>Thành công!</strong> {{ session('resetSuccess') }}
                        </div>
                    @endif
                    @if(session('successRegister'))
                        <div class="alert alert-success">
                            <strong>Thành công!</strong> {{ session('successRegister') }}
                        </div>
                    @endif
                    @csrf
                    <input type="text"
                           name="username" value="{{ $errors->any() ? old('username') : '' }}"
                           placeholder="Tên đăng nhập hoặc email" required>
                    @if ($errors->has('username'))
                        <label class="message__form--errors">{{ $errors->first('username') }}</label>
                    @endif
                    <input type="password" name="password" placeholder="Mật khẩu" required>
                    @if ($errors->has('password'))
                        <label class="message__form--errors">{{ $errors->first('password') }}</label>
                    @endif
                    <label><input type="checkbox" name="remember"> Ghi nhớ đăng nhập</label>
                    <input type="submit" value="Đăng nhập">
                </form>
                <div class="container-fluid">
                    <ul class="list-inline row">
                        <li class="col-sm-6 text-left"><a class="bt" href="{{ route('register') }}">Đăng ký tài khoản</a>
                        </li>
                        <li class="col-sm-6 text-right"><a class="bt" href="{{ route('reset_password_user') }}">Quên mật
                                khẩu</a></li>
                    </ul>
                </div>
            </div><!--end login area-->
        </div>
    </div>

@endsection