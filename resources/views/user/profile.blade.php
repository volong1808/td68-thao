@extends('frontend.layouts.master')
@section('css_page')
    @include('include.reglog_style')
@endsection
@section('content')
    <div class="container wrap-reglog">
        <div class="row">
            <div id="reg-form" class="reglog register-area text-center active">
                <h2>Thông Tin Tài Khoản</h2>
                <form class="form form-dang-nhap" method="post" action="{{ route('profile_user_post') }}" enctype="multipart/form-data">
                    @csrf
                    @if(session('updateErrors'))
                        <div class="alert alert-danger">
                            <strong>Lỗi!</strong> {{ session('updateErrors') }}
                        </div>
                    @endif
                    @if(session('updateSuccess'))
                        <div class="alert alert-success">
                            <strong>Thành công!</strong> {{ session('updateSuccess') }}
                        </div>
                    @endif
{{--                    <div style="margin-bottom: 10px; text-align: center">--}}
{{--                        <img id="file-image-avatar" src="{{ isset($user->avatar) ? asset($user->image->url) : asset('images/html/no-avatar.jpg') }}"--}}
{{--                             alt="logo" width="150" height="150">--}}
{{--                        <div class="upload-btn-wrapper">--}}
{{--                            <label for="imageUploadAvatar" class="labelUpload btn" style="text-align: center; background-color: #c9c9c9">--}}
{{--                                Upload Avatar--}}
{{--                            </label>--}}
{{--                            <input name="avatar" type="file" id="imageUploadAvatar" data-cover="#file-image-avatar"--}}
{{--                            style="display: none">--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <input type="text" name="name"
                           placeholder="Tên" value="{{ $errors->any() ? old('name') : $user->name }}" required>
                    @if ($errors->has('name'))
                        <label class="message__form--errors">{{ $errors->first('name') }}</label>
                    @endif
                    <input type="text"
                           ame="username" placeholder="Tên đăng nhập"
                           value="{{ $errors->any() ? old('username') : $user->username }}" readonly>
                    @if ($errors->has('username'))
                        <label class="message__form--errors">{{ $errors->first('username') }}</label>
                    @endif
                    <input type="text"
                           name="email" placeholder="Email" value="{{ $errors->any() ? old('email') : $user->email }}"
                           required>
                    @if ($errors->has('email'))
                        <label class="message__form--errors">{{ $errors->first('email') }}</label>
                    @endif
                    <input type="text"
                           name="phone" placeholder="Số điện thoại"
                           value="{{ $errors->any() ? old('phone') : $user->phone }}" >
                    @if ($errors->has('phone'))
                        <label class="message__form--errors">{{ $errors->first('phone') }}</label>
                    @endif
                    <input type="submit" value="Cập Nhật">
                </form>
                <a class="bt switch-bt" href="{{ route('change_password_user') }}">Đổi Mật Khẩu</a>
            </div><!--end register area-->
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            var fileInputAvatar = $('#imageUploadAvatar');
            fileInputAvatar.change(function (e) {
                console.log('change');
                var label = $(this).next();
                var labelVal = label.html();
                var fileName = '';
                if (this.files && this.files.length > 1) {
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                } else {
                    fileName = e.target.value.split('\\').pop();
                }
                console.log(fileName);
                var cover = $(this).data('cover');
                readURL(this, cover);
            });
        });

        function readURL(input, imageSelector) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(imageSelector).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection