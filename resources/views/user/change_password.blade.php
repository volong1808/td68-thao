@extends('frontend.layouts.master')
@section('css_page')
    @include('include.reglog_style')
@endsection
@section('content')
<div class="container wrap-reglog">
    <div class="row">
        <div id="reg-form" class="reglog register-area text-center active">

            <h2>Đổi Mật Khẩu</h2>
            <form class="form form-dang-nhap" method="post" action="{{ route('change_password_user_post') }}">
                @csrf
                @if(session('changePasswordErrors'))
                    <div class="alert alert-danger">
                        <strong>Lỗi!</strong>  {{ session('changePasswordErrors') }}
                    </div>
                @endif
                @if(session('changePasswordSuccess'))
                    <div class="alert alert-success">
                        <strong>Thành công!</strong>  {{ session('changePasswordSuccess') }}
                    </div>
                @endif
                <input type="password" name="password" placeholder="Mật khẩu" required>
                @if ($errors->has('password'))
                    <label class="message__form--errors">{{ $errors->first('password') }}</label>
                @endif
                <input type="password" name="password_new" placeholder="Mật khẩu mới" required>
                @if ($errors->has('password_new'))
                    <label class="message__form--errors">{{ $errors->first('password_new') }}</label>
                @endif
                <input type="password" name="password_confirmation" placeholder="Nhập lại mật khẩu mới" required>
                @if ($errors->has('password_confirmation'))
                    <label class="message__form--errors">{{ $errors->first('password_confirmation') }}</label>
                @endif
                <input type="submit" value="Đổi Mật Khẩu">
            </form>
            <a class="bt switch-bt" href="{{ route('profile_user') }}">Thông Tin Tài Khoản</a>
        </div><!--end register area-->
    </div><!--end row-->
</div>

@endsection
@section('script_page')
<script>
    jQuery(document).ready(function($){
      $('[data-toggle="tooltip"]').tooltip(); 
  });
</script>
@endsection