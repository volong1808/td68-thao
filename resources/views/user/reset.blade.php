@extends('frontend.layouts.master')
@section('css_page')
    @include('include.reglog_style')
@endsection
@section('content')
<div class="container wrap-reglog">
    <div class="row">
        <div id="lostpass-form" class="reglog lostpass-area text-center active">

            <h2>Quên mật khẩu</h2>
            <h3>Vui lòng nhập email</h3>
            <form class="form form-lostpass" action="{{ route('reset_password_user_post') }}" method="post">
                @csrf
                <input type="email" name="email" value="{{ $errors->any() ? old('email') : '' }}" placeholder="Email" required>
                @if ($errors->has('email'))
                    <label class="message__form--errors">{{ $errors->first('email') }}</label>
                @endif
                @if(session('errorEmail'))
                    <div class="alert alert-danger">
                        <strong>Lỗi!</strong> {{ session('errorEmail') }}
                    </div>
                @endif
                <input type="submit" value="Gửi yêu cầu">
            </form>
            <div class="container-fluid">
                <ul class="list-inline row">
                    <li class="col-sm-6 text-left"><a class="bt" href="{{ route('login_user') }}">Đăng nhập</a></li>
                    <li class="col-sm-6 text-right"><a class="bt switch-bt" href="{{ route('register') }}">Đăng ký tài khoản</a></li>
                </ul>
            </div>
        </div><!--end register area-->
    </div><!--end row-->
</div>

@endsection
@section('script_page')
<script>
    jQuery(document).ready(function($){
      $('[data-toggle="tooltip"]').tooltip(); 
  });
</script>
@endsection