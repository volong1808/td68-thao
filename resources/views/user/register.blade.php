@extends('frontend.layouts.master')
@section('css_page')
    @include('include.reglog_style')
@endsection
@section('content')
<div class="container wrap-reglog">
    <div class="row">
        <div id="reg-form" class="reglog register-area text-center active">

            <h2>Đăng ký</h2>
            <form class="form form-dang-nhap" method="post" action="{{ route('register_post') }}">
                @csrf
                <input type="text" name="name"
                       placeholder="Tên" value="{{ $errors->any() ? old('name') : '' }}" required>
                @if ($errors->has('name'))
                    <label class="message__form--errors">{{ $errors->first('name') }}</label>
                @endif
                <input type="text"
                       name="username" placeholder="Tên đăng nhập" value="{{ $errors->any() ? old('username') : '' }}"  required>
                @if ($errors->has('username'))
                    <label class="message__form--errors">{{ $errors->first('username') }}</label>
                @endif
                <input type="text"
                       name="email" placeholder="Email" value="{{ $errors->any() ? old('email') : '' }}"  required>
                @if ($errors->has('email'))
                    <label class="message__form--errors">{{ $errors->first('email') }}</label>
                @endif
                <input type="text"
                       name="phone" placeholder="Số điện thoại" value="{{ $errors->any() ? old('phone') : '' }}" >
                @if ($errors->has('phone'))
                    <label class="message__form--errors">{{ $errors->first('phone') }}</label>
                @endif
                <input type="password" name="password" placeholder="Mật khẩu" required>
                @if ($errors->has('password'))
                    <label class="message__form--errors">{{ $errors->first('password') }}</label>
                @endif
                <input type="password" name="password_confirmation" placeholder="Nhập lại mật khẩu" required>
                @if ($errors->has('password_confirmation'))
                    <label class="message__form--errors">{{ $errors->first('password_confirmation') }}</label>
                @endif
                <input type="submit" value="Đăng ký">
            </form>
            <a class="bt switch-bt" href="{{ route('login_user') }}">Đăng nhập</a>
        </div><!--end register area-->
    </div>
</div>

@endsection
@section('script_page')
<script>
    jQuery(document).ready(function($){
      $('[data-toggle="tooltip"]').tooltip(); 
  });
</script>
@endsection