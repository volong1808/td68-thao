<style type="text/css">

    .message__form--errors {
        width: 100% !important;
        font-size: 12px;
        padding-left: 20px;
        color: #ff0000;
    }
    .wrap-reglog {
        padding: 30px 0;
        position: relative;
        z-index: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-content: center;
        background: rgba(237, 181, 210, 0.4);
    }

    .footer-wrapper {
        position: relative;
        z-index: 2;
        background: #fff;
    }
    .reglog {
        max-width: 400px;
        margin: 0 auto;
        background: white;
        padding: 50px 20px;
        border-radius: 5px;
        border: 1px solid #efefef;
    }
    .reglog h3{font-size:17px;text-align:center;font-weight:normal;margin-bottom:15px;}
    .reglog h2{
        margin-bottom:25px;
        background: #4dc6fd;
        padding: 10px 0;
        color: #fff;
    }

    .reglog p {
        margin: 2rem 0;
    }

    .reglog .social-icons li a {
        padding: 8px 30px;
        color: white;
        text-decoration: none;
        border-radius: 5px;
    }

    .reglog a.fb-icon {
        background: #3a559f;
    }

    .reglog a.gmail-icon {
        background: #f14336;
    }

    .form input{width:100%;margin-bottom:10px;border-radius:25px;border:1px solid #dcdcdc;padding:10px 20px;max-width:420px; outline: none;}
    .form input[type="submit"]{width:140px;background:#4dc6fd;color:white;border:none;font-size:17px;margin-top: 15px}
    .form label{width:50%;text-align:left;max-width:420px;line-height: 25px;}
    .form label input{float:left;width:20px;height:25px;margin-right:10px;}
    .reglog input[type="checkbox"] {
        width: 20px;
        height: 20px;
        line-height: 20px;
        display: inline-block;
        padding: 0;
        margin: 2px 0 0;
        cursor: pointer;
        vertical-align: top;
    }
    .reglog input[type="checkbox"]:checked + span:before {
        position: absolute;
        top: -1px;
        left: 4px;
        display: block;
        content: "";
        content: "\f00c";
        font-family: 'FontAwesome';
        font-size: 12px;
        color: #69c2e4;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
</style>