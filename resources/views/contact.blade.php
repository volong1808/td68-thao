@extends('frontend.layouts.master')
@section('title_head', 'Liên Hệ')
@section('content')
    <div class="content list-page">
        <div class="banner-default">
            <div class="container">
                <h3>LIÊN HỆ</h3>
                <ul class="breadcumb">
                    <li><a href="{{ route('home') }}">Trang chủ</a></li>
                    <li><span class="sep"><i class="fa fa-angle-right"></i></span></li>
                    <li>Liên Hệ</li>
                </ul>
            </div><!-- /.container -->
        </div>
    <!-- diy -->
        <div class="contact-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="contact-form__content">
                            @if (session('senContactSuccess'))
                                <div class="alert alert-success alert-styled-left alert-dismissable">
                                    <button type="button" class="close close-alert" data-dismiss="alert">
                                        <span>×</span>
                                    </button>
                                    {{ session('senContactSuccess') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-styled-left alert-dismissable">
                                    <button type="button" class="close close-alert" data-dismiss="alert">
                                        <span>×</span>
                                    </button>
                                    Có lỗi trong thông tin vừa nhập. Vui lòng kiểm tra lại
                                </div>
                            @endif
                            <form method="post" name="contact_form" class="contact_form" action="{{ route('contact_post') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-item">
                                    <p>Họ và tên:<span>*</span></p>
                                    <input name="name" type="text" id="name">
                                    @error('name')
                                    <span role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-item">
                                    <p>Địa chỉ:<span>*</span></p>
                                    <input name="address" type="text" id="address">
                                    @error('address')
                                    <span role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-item">
                                    <p>Điện thoại:<span>*</span></p>
                                    <input name="phone_number" type="text" id="phone_number">
                                    @error('phone_number')
                                    <span role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-item">
                                    <p>Email:<span>*</span></p>
                                    <input name="email" type="email" id="email">
                                    @error('email')
                                    <span role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-item">
                                    <p>Chủ đề:<span>*</span></p>
                                    <input name="subject" type="text" id="subject">
                                    @error('subject')
                                    <span role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-item">
                                    <p>Nội dung:<span>*</span></p>
                                    <textarea name="message" id="message" rows="5"></textarea>
                                    @error('message')
                                    <span role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-item submit">
                                    <input type="submit" value="Gửi">
                                    <input type="button" value="Nhập lại" onclick="document.contact_form.reset();">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="contact-info">
                            <h3 class="text-center">TD68 STORE</h3>

                            @if(!empty($companyInfo['address']))
                                <p><i class="fa fa-map-marker"></i> {{ $companyInfo['address'] }}</p>
                            @endif

                            @if(!empty($companyInfo['email']))
                                <p><span><i class="fa fa-envelope"></i> <a href="mailto:{{ $companyInfo['email'] }}" target="_blank" >{{ $companyInfo['email'] }}</a></span></p>
                            @endif

                            @if(!empty($companyInfo['phone']))
                                <p><span><i class="fa fa-phone"></i> <a href="tel:{{ $companyInfo['phone'] }}" target="_blank"> {{ $companyInfo['phone'] }}</a></span></p>
                            @endif

                            <div class="contact-info__social">
                                <ul>
                                    <li><a href="{{ $companyInfo['facebook'] ?? 'https://www.facebook.com' }}"><i class="fa fa-facebook-square"></i></a></li>
                                    <li><a href="{{ $companyInfo['instagram'] ?? 'https://www.instagram.com' }}"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="{{ $companyInfo['youtube'] ?? 'https://www.youtube.com' }}"><i class="fa fa-youtube-square"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /diy -->
@endsection
@section('script_page')
@endsection