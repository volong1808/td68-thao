<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain default error messages used by
    | validator class. Some of these rules have multiple versions such
    | as size rules. Feel free to tweak each of these messages here.
    |
    */

    'required' => ':attribute là bắt buộc',
    'in' => ':attribute giá trị lựa chọn không đúng',
    'url' => ':attribute phải là một link! Ví dụ (https://example.com)',
    'email' => ':attribute phải là một email! Ví dụ (example@'. url('/') .')',
    'max' => [
        'numeric' => ':attribute không được lớn hơn :max.',
        'file' => 'Dung lương cuar :attribute không được lớn hơn :max kilobytes.',
        'string' => ':attribute không được lớn hơn :max ký tự.',
        'array' => ':attribute không được lớn hơn :max phần tử.',
    ],
    'same' => ':attribute và mật khẩu mới phải trùng khớp.',
    'unique' => ':attribute đã tồn tại.',
    'mimes' => ':attribute là định dạng (:values)',
    'custom' => [
        'name' => [
            'required' => 'Tên là bắt buộc',
        ],
    ],
];
