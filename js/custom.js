$(function() {
    $m = $('nav#menu').html();
    $('nav#menu_mobi').append($m);
    $('.hien_menu').click(function(){
        $('nav#menu_mobi').css({height: "auto"});
    });
    $('.user .fa-user-plus').toggle(function(){
        $('.user ul').slideDown(300);
    },function(){
        $('.user ul').slideUp(300);
    });

    $('nav#menu_mobi').mmenu({
        extensions	: [ 'effect-slide-menu', 'pageshadow' ],
        searchfield	: true,
        counters	: true,
        navbar 		: {
            title		: 'Menu'
        },
        navbars		: [
            {
                position	: 'top',
                content		: [ 'searchfield' ]
            }, {
                position	: 'top',
                content		: [
                    'prev',
                    'title',
                    'close'
                ]
            }
        ]
    });
});