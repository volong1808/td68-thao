$(document).ready(function () {
    /* Scroll Top */
    $(window).scroll(function() {
        if ( $(this).scrollTop() > 300 ) {
            $('#scrollTop').show();
        } else {
            $('#scrollTop').hide();
        }
    });

    $('#scrollTop').on('click', function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    /* Slider */
    $('#slider-home').owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000,
        nav:true,
        navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        dots: false
    });

    /* Slider Collection */
    $('#collection-slide').owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 4000,
        nav:false,
        dots: false
    });

});
$(function () {
    $('nav#menu').mmenu({
        extensions: ['effect-slide-menu', 'pageshadow'],
        searchfield: true,
        counters: false,
        navbar: {
            title: 'TD68 Store',
            position: 'top',
            content: [
                'prev',
                'close'
            ]
        },
        offCanvas: {
            position: "left"
        },
        navbars: [
            {
                position: 'top',
                content: ['searchfield']
            }
        ]
    });

    /* Product Item */
    $(document).on("mouseover", '.size__item', function () {
        var price = $(this).data("price");
        var elmPrice = $(this).parent().parent().parent().find(".price");

        elmPrice[0].innerHTML = price;
    });

    /* Product Info */
    $(document).on('click', '.info__size .size', function () {
        var infoPrice = $(this).data('price');
        let url = $(this).attr('data-url');
        $('.size.active').removeClass('active');
        $(this).addClass('active');
        $('#show-price').html(infoPrice);
        $('#order_cart').attr('href', url);
    });
});