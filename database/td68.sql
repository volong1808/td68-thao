-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th6 30, 2020 lúc 12:20 AM
-- Phiên bản máy phục vụ: 5.7.24
-- Phiên bản PHP: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `td68`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `description`, `content`, `image_id`, `slug`, `post_type`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Apple Iphone', NULL, 'Apple Iphone', '', NULL, 'apple-iphone', 'product', '1', '2020-06-29 07:42:47', '2020-06-29 07:42:47'),
(2, 'Iphone 6', 1, 'Iphone 6', '', NULL, 'iphone-6', 'product', '1', '2020-06-29 07:43:01', '2020-06-29 07:43:01'),
(3, 'Iphone 5', 1, 'Iphone 5', '', NULL, 'iphone-5', 'product', '1', '2020-06-29 07:43:18', '2020-06-29 07:43:18'),
(4, 'Phụ Kiện', NULL, 'Phụ Kiện', '', NULL, 'phu-kien', 'product', '1', '2020-06-29 07:43:33', '2020-06-29 07:43:33'),
(5, 'Ốp Lưng', 4, 'Ốp Lưng', '', NULL, 'p-lung', 'product', '1', '2020-06-29 07:44:16', '2020-06-29 07:44:16'),
(6, 'Tin Tuc', NULL, 'Tin Tuc', '', NULL, 'tin-tuc', 'tutorial', '1', '2020-06-29 07:52:27', '2020-06-29 07:52:27'),
(7, 'Thủ Thuật', NULL, 'Thủ Thuật', '', NULL, 'thu-thuat', 'tutorial', '1', '2020-06-29 07:52:47', '2020-06-29 07:52:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category_post`
--

CREATE TABLE `category_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category_post`
--

INSERT INTO `category_post` (`id`, `category_id`, `post_id`) VALUES
(4, 6, 2),
(5, 1, 1),
(6, 2, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL COMMENT '1: Mới, 2: Đã feedback, 3: Đã hủy',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gallery`
--

CREATE TABLE `gallery` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `gallery`
--

INSERT INTO `gallery` (`id`, `post_id`, `image_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-06-29 07:55:10', '2020-06-29 07:55:28'),
(2, 0, 2, '2020-06-29 07:55:10', '2020-06-29 07:55:10');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `alt` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `origin`, `url`, `thumb`, `file_name`, `file_path`, `width`, `height`, `size`, `name`, `alt`, `created_at`, `updated_at`) VALUES
(1, 'upload/2020/06/29/origin_about-3-1-20200629145510.png', 'upload/2020/06/29/origin_about-3-1-20200629145510.png', 'upload/2020/06/29/thumb_about-3-1-20200629145510.png', 'origin_about-3-1-20200629145510.png', './upload/2020/06/29/origin_about-3-1-20200629145510.png', 600, 600, 311082, NULL, ' image', '2020-06-29 07:55:10', '2020-06-29 07:55:10'),
(2, 'upload/2020/06/29/origin_about-us-20200629145510.png', 'upload/2020/06/29/origin_about-us-20200629145510.png', 'upload/2020/06/29/thumb_about-us-20200629145510.png', 'origin_about-us-20200629145510.png', './upload/2020/06/29/origin_about-us-20200629145510.png', 600, 600, 355084, NULL, ' image', '2020-06-29 07:55:10', '2020-06-29 07:55:10'),
(3, 'upload/2020/06/29/origin_263730-20200629145527.jpg', 'upload/2020/06/29/origin_263730-20200629145528.jpg', 'upload/2020/06/29/thumb_263730-20200629145528.jpg', 'origin_263730-20200629145528.jpg', './upload/2020/06/29/origin_263730-20200629145528.jpg', 600, 600, 208441, 'Iphone 6 16G', 'Iphone 6 16G image', '2020-06-29 07:55:28', '2020-06-29 07:55:28'),
(4, 'upload/2020/06/29/origin_263730-20200629145652.jpg', 'upload/2020/06/29/origin_263730-20200629145652.jpg', 'upload/2020/06/29/thumb_263730-20200629145652.jpg', 'origin_263730-20200629145652.jpg', './upload/2020/06/29/origin_263730-20200629145652.jpg', 600, 600, 208441, 'tin tuc 1', 'tin tuc 1 image', '2020-06-29 07:56:53', '2020-06-29 07:56:53'),
(5, 'upload/2020/06/29/origin_banner-20200629145900.png', 'upload/2020/06/29/origin_banner-20200629145900.png', 'upload/2020/06/29/thumb_banner-20200629145900.png', 'origin_banner-20200629145900.png', './upload/2020/06/29/origin_banner-20200629145900.png', 1141, 443, 489848, 'aa', 'aa image', '2020-06-29 07:59:00', '2020-06-29 07:59:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `metadata_page`
--

CREATE TABLE `metadata_page` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `link_post_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'static',
  `show_menu` int(11) NOT NULL DEFAULT '1',
  `icon_menu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'static',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `metadata_page`
--

INSERT INTO `metadata_page` (`id`, `post_id`, `link_post_type`, `show_menu`, `icon_menu`, `created_at`, `updated_at`) VALUES
(1, 4, 'product', 1, 'icofont-ui-settings', '2020-06-29 08:32:54', '2020-06-29 08:32:54'),
(2, 5, 'tutorial', 1, 'icofont-ui-settings', '2020-06-29 08:33:17', '2020-06-29 08:33:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `metadata_product`
--

CREATE TABLE `metadata_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'static',
  `storage` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_default` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `metadata_product`
--

INSERT INTO `metadata_product` (`id`, `post_id`, `info`, `code`, `type`, `storage`, `price_default`, `price_sale`, `created_at`, `updated_at`) VALUES
(1, 1, '- Màn Hình: 320x440\r\n- Ok ok : 0303', 'IPhone 6G', 'active', '16G', 1200000, 300, '2020-06-29 08:46:31', '2020-06-29 08:46:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_27_072007_create_posts_table', 1),
(4, '2019_09_27_074626_create_categories_table', 1),
(5, '2019_09_27_074953_create_category_post_table', 1),
(6, '2019_09_27_075435_create_images_table', 1),
(7, '2019_09_27_080305_create_pages_table', 1),
(8, '2019_09_27_080436_create_seo_data_table', 1),
(9, '2019_09_27_092350_add_avatar_to_user', 1),
(10, '2019_09_29_152238_create_gallery_table', 1),
(11, '2019_09_30_143751_add_name_to_user', 1),
(12, '2019_09_30_152546_add_phone_to_user', 1),
(13, '2019_10_08_031254_create_user_plan_table', 1),
(14, '2019_10_08_074925_create_option_table', 1),
(15, '2019_10_08_145802_create_contacts_table', 1),
(16, '2019_10_10_075825_create_orders_table', 1),
(17, '2019_10_10_081633_add_code_to_orders_table', 1),
(18, '2019_10_13_085830_add_total_to_order_table', 1),
(19, '2019_10_15_084740_add_username_to_orders_table', 1),
(20, '2019_10_17_035355_add_transaction_no_to_orders_table', 1),
(21, '2020_03_23_123021_create_option_post', 1),
(22, '2020_03_25_225244_create_post_option_post_table', 1),
(23, '2020_05_08_123855_drop_transaction_no_to_orders_table', 1),
(24, '2020_05_08_124348_careate_order_items_table', 1),
(25, '2020_05_14_140943_create_social_accounts_table', 1),
(26, '2020_05_15_074253_create_option_post_option_table', 1),
(27, '2020_05_19_090939_add_type_to_option_post_option_table', 1),
(28, '2020_05_26_021817_add_content_to_option_post_option_table', 1),
(29, '2020_05_26_022248_modify_content_to_option_post_option_table', 1),
(30, '2020_05_31_080246_modify_order_id_to_order_items_table', 1),
(31, '2020_06_02_152208_add_content_to_post_option_post_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `option`
--

CREATE TABLE `option` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `option`
--

INSERT INTO `option` (`id`, `key`, `content`, `created_at`, `updated_at`) VALUES
(1, 'company', '{\"name\":\"td68\",\"tax\":null,\"logo\":null,\"stage\":\"1\",\"description\":null,\"address\":null,\"phone\":null,\"phone2\":null,\"hot_line\":null,\"email\":null,\"facebook\":null,\"google\":null,\"twitter\":null,\"flickr\":null,\"youtube\":null,\"instagram\":null,\"google_maps\":null}', '2020-06-29 08:30:40', '2020-06-29 08:48:45'),
(2, 'company', NULL, '2020-06-29 08:32:20', '2020-06-29 08:32:20'),
(3, 'banking', NULL, '2020-06-29 08:32:26', '2020-06-29 08:32:26'),
(4, 'company', NULL, '2020-06-29 08:32:28', '2020-06-29 08:32:28'),
(5, 'plugin', NULL, '2020-06-29 08:32:29', '2020-06-29 08:32:29'),
(6, 'company', NULL, '2020-06-29 08:32:30', '2020-06-29 08:32:30'),
(7, 'company', NULL, '2020-06-29 08:48:37', '2020-06-29 08:48:37');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `option_post`
--

CREATE TABLE `option_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `option_post_option`
--

CREATE TABLE `option_post_option` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option_post_from_id` bigint(20) NOT NULL,
  `option_post_to_id` bigint(20) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `status` smallint(6) NOT NULL COMMENT '1: Mới, 2: Đã thanh toán, 3: Hết hạn, 4: Đã hủy',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `message` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `discount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pages`
--

INSERT INTO `pages` (`id`, `name`, `description`, `content`, `image_id`, `slug`, `page_type`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Trang Chủ', 'Aut eum qui quisquam sapiente sit consequatur aliquid.', 'Voluptas ullam est rerum hic enim est eos ex maxime non ut repudiandae voluptates reprehenderit eum et quaerat commodi dolor et sint ut.', NULL, 'trang-chu', 'page_static', '1', '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(2, 'Sản Phẩm', 'Aspernatur voluptatum numquam possimus odit earum omnis.', 'Error est occaecati eligendi asperiores minima ut rerum est rerum tempora sed consequatur eum ad numquam.', NULL, 'san-pham', 'page_static', '1', '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(3, 'Giới Thiệu', 'Est consectetur laboriosam incidunt consequatur molestiae harum sint molestiae aperiam voluptatem inventore consequatur.', 'Fuga aut vel reprehenderit non rerum voluptas aut est nemo consequatur perferendis laborum est perferendis minus aut et qui dolores ea quae modi sunt facilis debitis est.', NULL, 'gioi-thieu', 'page_static', '1', '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(4, 'Hướng Dẫn', 'Adipisci et dignissimos iure est at rerum.', 'Sunt animi sint qui eligendi ut quibusdam sit nihil veritatis rerum asperiores ducimus aut et perspiciatis inventore sed alias cum fugit expedita et excepturi dignissimos aut.', NULL, 'huong-dan', 'page_static', '1', '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(5, 'Blog', 'Ex provident deserunt sint rerum ut dolores ut cumque.', 'Optio voluptatibus expedita unde excepturi aspernatur nemo incidunt nostrum sed porro odit et cum quia dolorem nam cupiditate sequi.', NULL, 'blog', 'page_static', '1', '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(6, 'Q&A', 'Tenetur quia saepe deserunt architecto impedit quia est sed ipsa.', 'Qui sequi qui et earum rerum sed repellat error perferendis dicta et sint perferendis numquam consequatur numquam placeat eveniet consequuntur sed aperiam libero molestias laboriosam corrupti aspernatur.', NULL, 'qa', 'page_static', '1', '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(7, 'Liên Hệ', 'Incidunt molestias dolore repellendus accusamus ut dolorem quo at.', 'Consequatur non minus harum animi sint pariatur aliquid rem voluptatum officia dolorum vitae omnis aut fugiat quibusdam nostrum odio enim reiciendis similique et et et.', NULL, 'lien-he', 'page_static', '1', '2020-06-29 07:22:59', '2020-06-29 07:22:59');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `name`, `description`, `content`, `image_id`, `slug`, `post_type`, `state`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Iphone 6 16G', '', 'Iphone 6 16G', 3, 'iphone-6-16g', 'product', '1', NULL, '2020-06-29 07:55:27', '2020-06-29 07:55:28'),
(2, 'tin tuc 1', 'tin tuc 1 mo ta', 'tin tuc 1', 4, 'tin-tuc-1', 'tutorial', '1', NULL, '2020-06-29 07:56:52', '2020-06-29 07:56:53'),
(3, 'aa', '', 'aaa', 5, '', 'slide', '1', NULL, '2020-06-29 07:59:00', '2020-06-29 07:59:00'),
(4, 'Sản Phẩm', '', 'Sản Phẩm', NULL, 'san-pham', 'page', '1', NULL, '2020-06-29 08:32:54', '2020-06-29 08:32:54'),
(5, 'Tin Tức - Thủ Thuật', '', 'Tin Tức - Thủ Thuật', NULL, 'tin-tuc', 'page', '1', NULL, '2020-06-29 08:33:17', '2020-06-29 08:33:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_option_post`
--

CREATE TABLE `post_option_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `option_post_id` bigint(20) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `seo_data`
--

CREATE TABLE `seo_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `item_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `seo_data`
--

INSERT INTO `seo_data` (`id`, `name`, `description`, `image_id`, `type`, `item_type`, `item_id`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 3, NULL, 'post', 1, '2020-06-29 07:55:28', '2020-06-29 07:55:28'),
(2, NULL, NULL, 4, NULL, 'post', 2, '2020-06-29 07:56:53', '2020-06-29 07:56:53'),
(3, NULL, NULL, 5, NULL, 'post', 3, '2020-06-29 07:59:01', '2020-06-29 07:59:01'),
(4, NULL, NULL, NULL, NULL, 'post', 4, '2020-06-29 08:32:54', '2020-06-29 08:32:54'),
(5, NULL, NULL, NULL, NULL, 'post', 5, '2020-06-29 08:33:17', '2020-06-29 08:33:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider_user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL COMMENT '1 => sadmin, 2 => admin, 3 => mod',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `role`, `phone`, `name`, `avatar`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sadmin', 'sadmin@gmail.com', '2020-06-29 07:22:57', '$2y$10$mOt5FHt1alPZpSzJibk9ROf2//i68PWoDWzRrIDI7T1/szzSkP5GC', 1, NULL, NULL, NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(2, 'admin', 'admin@gmail.com', '2020-06-29 07:22:57', '$2y$10$dopBhtJC8aXF.p1YYQpNDeH5fbVioAy20TZRxwtNbYT/vGKoDXy9K', 2, NULL, NULL, NULL, 1, 'smRI9CBKAySFCI9xwuu5in0AbidChMIf5YzVFGb8W8GGWMr7CZBW2nThPPEh', '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(3, 'mod', 'mod@gmail.com', '2020-06-29 07:22:57', '$2y$10$DgFZN/j26ybN655uHjx4XuU9tkzbJcj9gtESpz88mwCTsPHYbR7cy', 3, NULL, NULL, NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(4, 'user1', 'user1@gmail.com', '2020-06-29 07:22:57', '$2y$10$7hxFY7oaN8ADwNQ9lmS9FOe/PDorDx1pCUUDeqMh7l6yaZRqJ9R6q', 9, NULL, 'June Kovacek', NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(5, 'user2', 'user2@gmail.com', '2020-06-29 07:22:57', '$2y$10$vqN.z6nBxqjqGZne3YUTWuT75FhFVPx16pOMVJiyBL8Ux/muLIS2S', 9, NULL, 'Nicklaus Haag', NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(6, 'user3', 'user3@gmail.com', '2020-06-29 07:22:57', '$2y$10$pgvlWtwztn35CVZPHSPzfe0mbnv1TKVoQoVO8nYBiZz8vR2YIGN/q', 9, NULL, 'Miss Joannie Parker IV', NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(7, 'user4', 'user4@gmail.com', '2020-06-29 07:22:57', '$2y$10$eWUIVX4kLmHQnUc8XlZ5eeSmQrGHa30al3IelRgfVEsGIhaAbKNpC', 9, NULL, 'Rey Renner', NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(8, 'user5', 'user5@gmail.com', '2020-06-29 07:22:57', '$2y$10$c4MXYtG3EBrACf6Aj1/OfukdDcGj394ONXuY5w3WnZcm67xbsnLOW', 9, NULL, 'Blanca Zboncak', NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(9, 'user6', 'user6@gmail.com', '2020-06-29 07:22:57', '$2y$10$UAZJQJ7pLhyF4FIkFHbTdeeY3RNkT3jRVqT00c8EHoVuI6E2/RHVu', 9, NULL, 'Jazmin Corwin IV', NULL, 1, NULL, '2020-06-29 07:22:57', '2020-06-29 07:22:57'),
(10, 'user7', 'user7@gmail.com', '2020-06-29 07:22:58', '$2y$10$RNQwTyBK9/aPCD6lMTcAhOmgWudQPneHkdWa3zu9Dnor7GktNVw6O', 9, NULL, 'Antwon Lockman', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(11, 'user8', 'user8@gmail.com', '2020-06-29 07:22:58', '$2y$10$tNH64PAptwh3pozI7MmUV.cyTv91uXVfMXEnNz5R1NQPGVzQ2h0VS', 9, NULL, 'Mitchell Conroy', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(12, 'user9', 'user9@gmail.com', '2020-06-29 07:22:58', '$2y$10$RdgXWMkQcjRLSaUJh14IZunq98YJSOmOOtKujgqaC/GQa4or8uW3i', 9, NULL, 'Martine Davis', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(13, 'user10', 'user10@gmail.com', '2020-06-29 07:22:58', '$2y$10$lbko.eSgBrzPsCiPYm1CJ.Iib51zFJvRs1sUbwXk7iWepbwJ.rwSi', 9, NULL, 'Prof. Meggie Ferry Jr.', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(14, 'user11', 'user11@gmail.com', '2020-06-29 07:22:58', '$2y$10$i.1VukWUIUnTZn0xHiJYMurt7qp95aSEJJ8F5SR8q2Ntrl.sZ.VfO', 9, NULL, 'Tomasa Gulgowski', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(15, 'user12', 'user12@gmail.com', '2020-06-29 07:22:58', '$2y$10$gJuj.QGksfOfoIyKHJgvwuuZrXB1HdcvxzmXvyhi78pZDOnRSsqoy', 9, NULL, 'Angie Rau', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(16, 'user13', 'user13@gmail.com', '2020-06-29 07:22:58', '$2y$10$hsWZnhwxKWL4vWPUYn2breOfX1Wk8wXjmCEJvpMJSydlNw2eq2oLG', 9, NULL, 'Jennyfer Hammes', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(17, 'user14', 'user14@gmail.com', '2020-06-29 07:22:58', '$2y$10$CgZF8V28jkUXisivYT1/ae53.ow6ZGKWdC6EMgVeE5U2wxryHRqza', 9, NULL, 'Miss Kaelyn Beahan', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(18, 'user15', 'user15@gmail.com', '2020-06-29 07:22:58', '$2y$10$X5i3T63RETuB/2hD.wSD3u1.961cxl/XKox3syrdIgM92fOSfsOm.', 9, NULL, 'Mr. Juvenal Hammes', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(19, 'user16', 'user16@gmail.com', '2020-06-29 07:22:58', '$2y$10$9jkE.zXtLOxCQP7WwmP1Zup5JeywH/QpGqiGrG.2QMZf9UDld5nlS', 9, NULL, 'Mrs. Janelle Schiller', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(20, 'user17', 'user17@gmail.com', '2020-06-29 07:22:58', '$2y$10$s8EYtEvzNuARuXMQhHcS0u9H58t.IL36PCd3q5Cjidw.SVHnAPsNa', 9, NULL, 'Ashlynn Mertz IV', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(21, 'user18', 'user18@gmail.com', '2020-06-29 07:22:58', '$2y$10$AVg8qHzppQjyzGt1Kt6EROW4g24m2rwd.KZsRskTdhHMkCdLorP.2', 9, NULL, 'Dr. Larry Corwin', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(22, 'user19', 'user19@gmail.com', '2020-06-29 07:22:58', '$2y$10$GUvGUeC7IxeDd0mnk2xJHOjIn9GtEvb71rPZ8zgTD01MUk9d1fDLC', 9, NULL, 'Eldora Gutkowski', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(23, 'user20', 'user20@gmail.com', '2020-06-29 07:22:58', '$2y$10$POuPBtlwtAxB61HJNHns6e/kofuV6cFtKQtx.Nqw4RNfnTmPFvRLi', 9, NULL, 'Chet Terry', NULL, 1, NULL, '2020-06-29 07:22:58', '2020-06-29 07:22:58'),
(24, 'user21', 'user21@gmail.com', '2020-06-29 07:22:59', '$2y$10$LVMxXheYVxNCbF4pDK.e7O6rySGOHsLQu5YLFdkENCXsSB5ViVsmW', 9, NULL, 'Deangelo Turcotte', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(25, 'user22', 'user22@gmail.com', '2020-06-29 07:22:59', '$2y$10$gFQZzhMUHwIU/eMrdu4qhOdnXCeiM5OOG5lFyF4BEj/I3CR.mvN.W', 9, NULL, 'Dr. Ernestina Dare DVM', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(26, 'user23', 'user23@gmail.com', '2020-06-29 07:22:59', '$2y$10$1tXpkgF9eW/yTAmlFDzhLON29bBarYjG1xWE2m0tmeTo7XQXGqwc6', 9, NULL, 'Dr. Jace VonRueden DVM', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(27, 'user24', 'user24@gmail.com', '2020-06-29 07:22:59', '$2y$10$pNhj2nTA2xOo4qv912MK4OSYf.Ne0gossu5RksZ2N/TX2NDvdAzu2', 9, NULL, 'Elnora Wolff', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(28, 'user25', 'user25@gmail.com', '2020-06-29 07:22:59', '$2y$10$8EaZBidbVAokkznG3ZpmeeO0Tmiz7/lHZA90Fb7du8dc0c/HV66ri', 9, NULL, 'Rylan Wilderman', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(29, 'user26', 'user26@gmail.com', '2020-06-29 07:22:59', '$2y$10$Fh6CdKCu8PcMaHvHHzvFrubfYo.t7osykGzR1k6tGgh7KBYg5TqA.', 9, NULL, 'Lenna Kuphal', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(30, 'user27', 'user27@gmail.com', '2020-06-29 07:22:59', '$2y$10$WwqirKFIy4QZKFFAvEF5X.S6aqU6fn78HmXAoyfstGHLcNd8nPnj.', 9, NULL, 'Miss Elaina Sawayn II', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(31, 'user28', 'user28@gmail.com', '2020-06-29 07:22:59', '$2y$10$2zfxfrjbfpD3v7EwO7AOmO8dCTU/6fSQ/JQRmQFICVU7eJo5vKATC', 9, NULL, 'Prof. Leopold Bosco I', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(32, 'user29', 'user29@gmail.com', '2020-06-29 07:22:59', '$2y$10$TRAMY5gG0823iYQwvQDmvemSeXNlmkZSN7v4X6y7PULj/a3G5MA6S', 9, NULL, 'Aric Pollich IV', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59'),
(33, 'user30', 'user30@gmail.com', '2020-06-29 07:22:59', '$2y$10$2cl6UTfg6qHyDbJh7L6yWuq.uqS3oWu3UGXFOWCCRfqgpO88Nkr9u', 9, NULL, 'Deron Kilback', NULL, 1, NULL, '2020-06-29 07:22:59', '2020-06-29 07:22:59');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_plan`
--

CREATE TABLE `user_plan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `active_time` datetime NOT NULL,
  `expired` datetime NOT NULL,
  `state` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category_post`
--
ALTER TABLE `category_post`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `metadata_page`
--
ALTER TABLE `metadata_page`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `metadata_product`
--
ALTER TABLE `metadata_product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `option_post`
--
ALTER TABLE `option_post`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `option_post_option`
--
ALTER TABLE `option_post_option`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `post_option_post`
--
ALTER TABLE `post_option_post`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `seo_data`
--
ALTER TABLE `seo_data`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `user_plan`
--
ALTER TABLE `user_plan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `category_post`
--
ALTER TABLE `category_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `metadata_page`
--
ALTER TABLE `metadata_page`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `metadata_product`
--
ALTER TABLE `metadata_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT cho bảng `option`
--
ALTER TABLE `option`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `option_post`
--
ALTER TABLE `option_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `option_post_option`
--
ALTER TABLE `option_post_option`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `post_option_post`
--
ALTER TABLE `post_option_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `seo_data`
--
ALTER TABLE `seo_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT cho bảng `user_plan`
--
ALTER TABLE `user_plan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
