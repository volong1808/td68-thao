<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

        $faker = Faker\Factory::create();

        DB::table('categories')->insert([
            'name' => 'Sản Phẩm',
            'description' => $faker->sentence(10),
            'content' => $faker->sentence(20),
            'slug' => 'san-pham',
            'post_type' => config('constants.POST_TYPE.PRODUCT'),
            'state' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        $planItems = [
            'Tự động tìm kiếm và kết bạn',
            'Quản lý danh sách bạn bè',
            'Tự động đăng bài',
            'Quản lý nhóm (group)',
            'Quản lý trang'
        ];
        foreach ($planItems as $plan) {
            $data = [
                'name' => $plan,
                'description' => $faker->sentence(10),
                'content' => $faker->sentence(20),
                'slug' => str_slug($plan),
                'post_type' => config('constants.POST_TYPE.PLAN'),
                'state' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
            DB::table('categories')->insert($data);
        }
    }
}
