<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->truncate();
        DB::table('images')->truncate();
        DB::table('categories')->truncate();
        DB::table('category_post')->truncate();
        DB::table('metadata_diy')->truncate();
        DB::table('metadata_supplies')->truncate();
        DB::table('metadata_scratch')->truncate();
        DB::table('metadata_accessories')->truncate();
        DB::table('seo_data')->truncate();
        DB::table('option_post')->truncate();
        DB::table('post_option_post')->truncate();

        $diys = $this->getPostsOldByType('table_product','sanpham', 13);
        $this->insertPosts($diys, 'diy');

        $supplies = $this->getPostsOldByType('table_product','sanpham', 10);
        $this->insertPosts($supplies, 'supplies');

        $scratches = $this->getPostsOldByType('table_product','sanpham', 15);
        $this->insertPosts($scratches, 'scratch');

        $accessories = $this->getPostsOldByType('table_product','sanpham', 12);
        $this->insertPosts($accessories, 'accessories');

        $this->insertOtherPostType('table_news', 'tuvan' ,'tutorial');
        $this->insertOtherPostType('table_news', 'hotro' ,'support');
        $this->insertOtherPostType('table_news', 'dichvu' ,'policy');

    }

    public function getPostsOldByType($tableName, $type, $idDanhMuc = null)
    {
        $where = [
            ['type', '=', $type],
            ['hienthi', '=', 1]
        ];
        if (!empty($idDanhMuc)) {
            $where[] = ['id_danhmuc', '=', $idDanhMuc];
        }

        $posts =  DB::connection('mysql2')->table($tableName)
            ->where($where)
            ->orderBy('id', 'DESC')
            ->get();
        return $posts;
    }

    public function insertPosts($oldPosts, $post_type)
    {
        foreach ($oldPosts as $post) {

            $image_id = $this->insertImage($post->photo, $post->thumb, $post->ten, $post->mota);

            $data = [
                'name' => $post->ten,
                'description' => $post->mota,
                'content' => $post->noidung,
                'image_id' => $image_id,
                'slug' => $post->tenkhongdau,
                'post_type' => $post_type,
                'state' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
            $postId = DB::table('posts')->insertGetId($data);

            $cateParentId = $this->insertParentCategory($post->id_list, $postId, $post_type);

            if (!empty($post->id_cat) && $post->id_cat != 0) {
                $this->insertChildrenCategory($post->id_cat, $cateParentId, $postId, $post_type);
            }

            $this->insertMetadata($postId, $post_type, $post->gia, $post->masp);
            $this->insertSize($post->size, $post->gia_theo_size, $postId,$post_type);
            $this->insertSeoData($post->ten, $post->mota, $image_id, $postId);

        }

    }

    public function insertImage($fileName, $thumb, $name, $alt)
    {
        $data = [
            'origin' => 'upload/images/' . $fileName,
            'url' => 'upload/images/' . $fileName,
            'thumb' => 'upload/images/' . $thumb,
            'file_name' => $fileName,
            'file_path' => './upload/images/' . $fileName,
            'name' => $name,
            'alt' => $alt,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ];

        return DB::table('images')->insertGetId($data);
    }

    public function getCategoryOld()
    {
        $categories =  DB::connection('mysql2')->table('table_product_list')->where('hienthi', '=', 1)->orderBy('id', 'DESC')->get();
        return $categories;
    }

    public function getCategoryOldById($id)
    {
        $category =  DB::connection('mysql2')->table('table_product_list')->where('id', '=', $id)->first();
        return $category;
    }

    public function getChildrenCategoryOldById($id)
    {
        $category =  DB::connection('mysql2')->table('table_product_cat')->where('id', '=', $id)->first();
        return $category;
    }

    public function insertParentCategory($cateIdOld, $postId, $type)
    {
        $categoryOld =  $this->getCategoryOldById($cateIdOld);

        $slug = $categoryOld->tenkhongdau;

        $category = DB::table('categories')->where('slug', '=', $slug)->where('post_type', '=', $type)->first();

        if (!empty($category)) {
            $data = [
                'category_id' => $category->id,
                'post_id' => $postId
            ];
            DB::table('category_post')->insert($data);
            return $category->id;
        } else {
            $data = [
                'name' => $categoryOld->ten,
                'description' => $categoryOld->description,
                'content' => $categoryOld->description,
                'slug' => $slug,
                'post_type' => $type,
                'state' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
            $cateId = DB::table('categories')->insertGetId($data);

            $dataUpdate = [
                'category_id' => $cateId,
                'post_id' => $postId
            ];
            DB::table('category_post')->insert($dataUpdate);

            return $cateId;
        }
    }

    public function insertChildrenCategory($cateIdOld, $cateIdParent, $postId, $type)
    {
        $categoryOld =  $this->getChildrenCategoryOldById($cateIdOld);
        if (!empty($categoryOld)) {
            $slug = $categoryOld->tenkhongdau;
            $category = DB::table('categories')->where('slug', '=', $slug)->where('post_type', '=', $type)->first();

            if (!empty($category)) {
                $data = [
                    'category_id' => $category->id,
                    'post_id' => $postId
                ];
                return DB::table('category_post')->insertGetId($data);
            } else {
                $data = [
                    'name' => $categoryOld->ten,
                    'parent_id' => $cateIdParent,
                    'description' => $categoryOld->description,
                    'content' => $categoryOld->description,
                    'slug' => $slug,
                    'post_type' => $type,
                    'state' => 1,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ];
                $cateId = DB::table('categories')->insertGetId($data);

                $dataUpdate = [
                    'category_id' => $cateId,
                    'post_id' => $postId
                ];
                return DB::table('category_post')->insertGetId($dataUpdate);
            }
        }

    }

    public function insertMetadata($postId, $postType, $price, $code)
    {
        $data = [
            'post_id' => $postId,
            'price_default' => $price,
            'code' => $code,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ];

        DB::table('metadata_' . $postType)->insert($data);
    }

    public function insertOtherPostType($tableName, $postTypeOld, $postType)
    {

        $posts = $this->getPostsOldByType($tableName, $postTypeOld);

        foreach ($posts as $post) {

            $image_id = $this->insertImage($post->photo, $post->thumb, $post->ten, $post->mota);

            $data = [
                'name' => $post->ten,
                'description' => $post->mota,
                'content' => $post->noidung,
                'image_id' => $image_id,
                'slug' => $post->tenkhongdau,
                'post_type' => $postType,
                'state' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];

            $postId = DB::table('posts')->insertGetId($data);

            $this->insertSeoData($post->ten, $post->mota, $image_id, $postId);
        }
    }

    public function insertSeoData($name, $description, $image_id, $post_id, $item_type = 'post')
    {

        $data = [
            'name' => $name,
            'description' => $description,
            'image_id' => $image_id,
            'item_type' => $item_type,
            'item_id' => $post_id,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ];

        DB::table('seo_data')->insert($data);
    }


    public function insertSize($size, $price, $post_id, $post_type)
    {

        $arrSize = explode(',' , $size);
        $arrPrice = explode(',' , $price);
        foreach ($arrSize as $key=>$item) {

            $item = trim($item);

            if (!empty($item)) {
                $option = DB::table('option_post')->where('name', '=', $item)->where('post_type', '=', $post_type)->first();

                if (!empty($option)) {
                    $data = [
                        'post_id' => $post_id,
                        'option_post_id' => $option->id,
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                    DB::table('post_option_post')->insert($data);
                } else {
                    $dataOption = [
                        'name' => $item,
                        'content' => '{"price":"' . ($arrPrice[$key] ?? $arrPrice[($key - 1)]) . '"}',
                        'type' => 'size',
                        'post_type' => $post_type,
                        'state' => 1,
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                    $option_post_id = DB::table('option_post')->insertGetId($dataOption);

                    $data = [
                        'post_id' => $post_id,
                        'option_post_id' => $option_post_id,
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                    DB::table('post_option_post')->insert($data);
                }
            }
        }
    }

}
