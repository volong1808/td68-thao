<?php
return [
    'company' => [
        'name' => [
            'label' => 'Tên Công Ty',
            'inputType' => 'text',
            'inputName' => 'name',
            'permalink' => false,
            'valid' => 'required',
        ],
        'tax' => [
            'label' => 'Mã Số Thuế',
            'inputType' => 'text',
            'inputName' => 'tax',
            'permalink' => false,
            'valid' => 'nullable|regex:/^[0-9]{10,11}$/',
        ],
        'logo' => [
            'label' => 'Logo',
            'inputType' => 'file',
            'inputName' => 'logo',
            'permalink' => false,
            'valid' => 'nullable|file|max:4028|mimes:png,jpg,gif,jpeg'
        ],
        'stage' => [
            'label' => 'Trạng Thái',
            'inputType' => 'radio',
            'inputName' => 'stage',
            'permalink' => false,
            'valid' => 'in:1,307,404',
            'item' => [
                1 => 'Công Bố',
                307 => 'Bảo Trì',
                404 => 'Khóa'
            ]
        ],
        'description' => [
            'label' => 'Mô Tả',
            'inputType' => 'text',
            'inputName' => 'description',
            'permalink' => false,
            'valid' => 'nullable'
        ],
        'address' => [
            'label' => 'Địa Chỉ',
            'inputType' => 'text',
            'inputName' => 'address',
            'permalink' => false,
            'valid' => 'nullable|max:200',
        ],
        'phone' => [
            'label' => 'Số Điện Thoại',
            'inputType' => 'text',
            'inputName' => 'phone',
            'permalink' => false,
            'valid' => 'nullable|regex:/^[0-9]{10,11}$/',
        ],
        'phone2' => [
            'label' => 'Số Điện Thoại 2',
            'inputType' => 'text',
            'inputName' => 'phone2',
            'permalink' => false,
            'valid' => 'nullable|regex:/^[0-9]{10,11}$/',
        ],
        'hot_line' => [
            'label' => 'Hot Line',
            'inputType' => 'text',
            'inputName' => 'hot_line',
            'permalink' => false,
            'valid' => 'nullable|regex:/^[0-9]{10,11}$/',
        ],
        'email' => [
            'label' => 'Email',
            'inputType' => 'email',
            'inputName' => 'email',
            'permalink' => false,
            'valid' => 'nullable|email'
        ],
        'facebook' => [
            'label' => 'Facebook',
            'inputType' => 'text',
            'inputName' => 'facebook',
            'permalink' => false,
            'valid' => 'nullable|max:255'
        ],
        'google' => [
            'label' => 'Google+',
            'inputType' => 'text',
            'inputName' => 'google',
            'permalink' => false,
            'valid' => 'nullable|max:255'
        ],
        'twitter' => [
            'label' => 'Twitter',
            'inputType' => 'text',
            'inputName' => 'twitter',
            'permalink' => false,
            'valid' => 'nullable|max:255'
        ],
        'flickr' => [
            'label' => 'Flickr',
            'inputType' => 'text',
            'inputName' => 'flickr',
            'permalink' => false,
            'valid' => 'nullable|max:255'
        ],
        'youtube' => [
            'label' => 'Youtube',
            'inputType' => 'text',
            'inputName' => 'youtube',
            'permalink' => false,
            'valid' => 'nullable|max:255'
        ],
        'instagram' => [
            'label' => 'Instagram',
            'inputType' => 'text',
            'inputName' => 'instagram',
            'permalink' => false,
            'valid' => 'nullable|max:255'
        ],
        'google_maps' => [
            'label' => 'Maps Google',
            'inputType' => 'text',
            'inputName' => 'google_maps',
            'permalink' => false,
            'valid' => 'nullable||max:1000'
        ],
    ],
    'banking' => [
        'cash' => [
            'label' => 'Thanh toán Tiền mặt',
            'inputType' => 'textarea',
            'inputName' => 'cash',
            'permalink' => false,
            'valid' => 'nullable||max:1000'
        ],
        'transfer' => [
            'label' => 'Thanh toán Chuyển khoản',
            'inputType' => 'textarea',
            'inputName' => 'transfer',
            'permalink' => false,
            'valid' => 'nullable||max:1000'
        ],
        'cod' => [
            'label' => 'Thanh toán COD',
            'inputType' => 'textarea',
            'inputName' => 'cod',
            'permalink' => false,
            'valid' => 'nullable||max:1000'
        ],
        'online' => [
            'label' => 'Thanh toán trực tuyến',
            'inputType' => 'textarea',
            'inputName' => 'online',
            'permalink' => false,
            'valid' => 'nullable||max:1000'
        ],
    ],

    'plugin' => [
        'google_analytics' => [
            'label' => 'Google Analytics',
            'inputType' => 'textarea',
            'inputName' => 'google_analytics',
            'permalink' => false,
            'valid' => 'nullable||max:1000'
        ],
        'chat_box' => [
            'label' => 'Chat Box',
            'inputType' => 'textarea',
            'inputName' => 'chat_box',
            'permalink' => false,
            'valid' => 'nullable||max:1000'
        ],
    ],
];