<?php
return [
    'product' => [
        'icon' => 'icon-stack4',
        'title' => 'Sản Phẩm',
        'post_type' => 'product',
        'formInput' => 'content_form',
        'pageSlug' => 'san-pham',
        'category' => true,
        'image' => [
            'size' => [
                'thumb' => [
                    'width' => 300,
                    'height' => 300,
                ],
                'origin' => [
                    'width' => 600,
                    'height' => 600,
                ],
            ],
            'place_holder' => 'images/product_place_holder_image.jpg',
            'label' => 'Ảnh Đại Diện',
            'name' => 'image'
        ],

        'galleries' => [
            'thumb' => [
                'width' => 100,
                'height' => 100,
            ],
            'origin' => [
                'width' => 300,
                'height' => 300,
            ],
            'place_holder' => 'images/product_place_holder_image.jpg',
            'label' => 'Ảnh Đại Diện',
            'name' => 'galleries'
        ],

        'meta_data' => [
            'info' => [
                'label' => 'Thông Số Kỹ Thuật',
                'inputName' => 'info',
                'valueDefault' => null,
                'typeValue' => 'text',
                'option' => [
                ],
                'isNotNull' => false,
                'inputType' => 'textarea',
                'validation' => 'nullable',
            ],
            'code' => [
                'label' => 'Mã Sản Phẩm',
                'inputName' => 'code',
                'valueDefault' => null,
                'typeValue' => 'string',
                'option' => [
                    'length' => 20
                ],
                'isNotNull' => false,
                'inputType' => 'text',
                'validation' => 'nullable|integer'
            ],

            'type' => [
                'label' => 'Thể Loại Sản Phẩm',
                'inputName' => 'type',
                'valueDefault' => 'active',
                'typeValue' => 'string',
                'isNotNull' => true,
                'option' => [
                    'default' => 'static',
                    'length' => 50
                ],
                'item' => [
                    'active' => 'Đã Kích Hoạt',
                    'inactive' => 'Chưa Kích Hoạt',
                ],
                'inputType' => 'radio',
                'validation' => 'nullable|max:50'
            ],

            'storage' => [
                'label' => 'Dung Lượng',
                'inputName' => 'storage',
                'valueDefault' => null,
                'typeValue' => 'string',
                'option' => [
                    'length' => 50
                ],
                'isNotNull' => false,
                'inputType' => 'text',
                'validation' => 'nullable'
            ],
            'price_default' => [
                'label' => 'Giá Tiền Mặc Định',
                'inputName' => 'price_default',
                'valueDefault' => null,
                'typeValue' => 'integer',
                'option' => [
                ],
                'isNotNull' => false,
                'inputType' => 'text',
                'validation' => 'nullable|integer'
            ],

            'price_sale' => [
                'label' => 'Giá Khuyến Mãi',
                'inputName' => 'price_sale',
                'valueDefault' => null,
                'typeValue' => 'integer',
                'option' => [
                ],
                'isNotNull' => false,
                'inputType' => 'text',
                'validation' => 'nullable|integer'
            ],
            'status' => [
                'label' => 'Tình Trạng',
                'inputName' => 'status',
                'valueDefault' => 1,
                'typeValue' => 'integer',
                'isNotNull' => false,
                'option' => [
                    'default' => 1,
                    'length' => 50
                ],
                'item' => [
                    1 => 'Còn Hàng',
                    2 => 'Hết Hàng',
                ],
                'inputType' => 'radio',
                'validation' => 'nullable|integer'
            ],

        ],
    ],

    'slide' => [
        'title' => 'Slide Trang Chủ',
        'icon' => 'icon-versions',
        'post_type' => 'slide',
        'formInput' => 'image_form',
        'category' => false,
        'pageSlug' => '',
        'permalink' => false,
        'image' => [
            'size' => [
                'thumb' => [
                    'width' => 1141,
                    'height' => 443,
                ],
                'origin' => [
                    'width' => 1141,
                    'height' => 443,
                ],
            ],
            'place_holder' => 'images/product_place_holder_image.jpg',
            'label' => 'Ảnh Đại Diện',
            'name' => 'image'
        ],
    ],

    'tutorial' => [
        'title' => 'Tin Tức - Thủ Thuật',
        'icon' => 'icon-drawer3',
        'post_type' => 'tutorial',
        'formInput' => 'content_form',
        'category' => true,
        'pageSlug' => 'tin-tuc',
        'image' => [
            'size' => [
                'thumb' => [
                    'width' => 300,
                    'height' => 300,
                ],
                'origin' => [
                    'width' => 600,
                    'height' => 600,
                ],
            ],
            'place_holder' => 'images/product_place_holder_image.jpg',
            'label' => 'Ảnh Đại Diện',
            'name' => 'image'
        ],
    ],

    'support' => [
        'title' => 'Hỗ Trợ Khách Hàng',
        'icon' => 'icon-drawer3',
        'post_type' => 'support',
        'formInput' => 'content_form',
        'category' => false,
        'pageSlug' => 'ho-tro-khach-hang',
        'image' => [
            'size' => [
                'thumb' => [
                    'width' => 300,
                    'height' => 300,
                ],
                'origin' => [
                    'width' => 600,
                    'height' => 600,
                ],
            ],
            'place_holder' => 'images/product_place_holder_image.jpg',
            'label' => 'Ảnh Đại Diện',
            'name' => 'image'
        ],
    ],

    'policy' => [
        'title' => 'Chính Sách Mua Hàng',
        'icon' => 'icon-drawer3',
        'post_type' => 'policy',
        'formInput' => 'content_form',
        'category' => false,
        'pageSlug' => 'chinh-sach-mua-hang',
        'image' => [
            'size' => [
                'thumb' => [
                    'width' => 300,
                    'height' => 300,
                ],
                'origin' => [
                    'width' => 600,
                    'height' => 600,
                ],
            ],
            'place_holder' => 'images/product_place_holder_image.jpg',
            'label' => 'Ảnh Đại Diện',
            'name' => 'image'
        ],
    ],

    'page' => [
        'icon' => ' icon-libreoffice',
        'title' => 'Trang',
        'post_type' => 'page',
        'formInput' => 'content_form',
        'category' => false,
        'pageSlug' => '',
        'image' => [
            'size' => [
                'thumb' => [
                    'width' => 600,
                    'height' => 200,
                ],
                'origin' => [
                    'width' => 1600,
                    'height' => 500,
                ],
            ],
            'place_holder' => 'images/product_place_holder_image.jpg',
            'label' => 'Ảnh Banner Trang',
            'name' => 'image'
        ],

        'meta_data' => [
            'link_post_type' => [
                'label' => 'Liên Kết Post',
                'inputName' => 'link_post_type',
                'valueDefault' => 'static',
                'typeValue' => 'string',
                'isNotNull' => true,
                'option' => [
                    'default' => 'static',
                    'length' => 50
                ],
                'item' => [
                    'static' => 'Trang Tĩnh',
                    'product' => 'Sản Phẩm',
                    'tutorial' => 'Tin Tức - Thủ Thuật',
                    'support' => 'Hỗ Trợ Khách Hàng',
                    'policy' => 'Chính Sách Mua Hàng',
                ],
                'inputType' => 'radio',
                'validation' => 'nullable|max:50'
            ],
            'show_menu' => [
                'label' => 'Hiển Thị Menu',
                'inputName' => 'show_menu',
                'valueDefault' => '1',
                'typeValue' => 'integer',
                'isNotNull' => true,
                'option' => [
                    'default' => 1,
                    'length' => 50
                ],
                'item' => [
                    '1' => 'Có',
                    '2' => 'Không',
                ],
                'inputType' => 'radio',
                'validation' => 'nullable|max:50'
            ],
            'icon_menu' => [
                'label' => 'Icon Menu',
                'inputName' => 'icon_menu',
                'valueDefault' => 'icofont-ui-settings',
                'typeValue' => 'string',
                'isNotNull' => false,
                'option' => [
                    'default' => 'static',
                    'length' => 255
                ],
                'inputType' => 'text',
                'validation' => 'nullable|max:50'
            ]
        ],
    ],
];