$(document).ready(function () {
    /* Scroll Top */
    $(window).scroll(function() {
        if ( $(this).scrollTop() > 300 ) {
            $('#scrollTop').show();
        } else {
            $('#scrollTop').hide();
        }
    });

    $('#scrollTop').on('click', function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    /* Product Item */
    $(".size__item").on("mouseover", function () {
        var price = $(this).data("price");
        var elmPrice = $(this).parent().parent().parent().find(".price");

        elmPrice[0].innerHTML = price;
    });

    /* Slider */
    $('#slider-home').owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000,
        nav:true,
        navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        dots: false
    });
    var heightSlider = $("#slider-home").width()/1.774;
    $(".slider-home .slider-item").css('height',heightSlider);

    $(window).on('resize', function () {
        heightSlider = $("#slider-home").width()/1.774;
        $(".slider-home .slider-item").css('height',heightSlider);
    });

    /* Product Info */
    $('.info__size .size').on('click', function () {
        var infoPrice = $(this).data('price');
        $('.size.active').removeClass('active');
        $(this).addClass('active');
        $('#show-price').html(infoPrice);
    })

    /* Slider Collection */
    $('#collection-slide').owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 4000,
        nav:false,
        dots: false
    });

});
$(function () {
    $('nav#menu').mmenu({
        extensions: ['effect-slide-menu', 'pageshadow'],
        searchfield: true,
        counters: false,
        navbar: {
            title: 'Lặt Vặt Ahihi',
            position: 'top',
            content: [
                'prev',
                'close'
            ]
        },
        offCanvas: {
            position: "left"
        },
        navbars: [
            {
                position: 'top',
                content: ['searchfield']
            }
        ]
    });
});