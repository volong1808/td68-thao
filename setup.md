### Cấu hình virtual host theo sample ở phía dưới
```
<VirtualHost *:80> 
    DocumentRoot "PATH_TO\Source\public"
    ServerName fb.test
    ServerAlias www.fb.test     
    <Directory "PATH_TO\Source\public">
        AllowOverride All
        Require all granted
        Options Indexes FollowSymLinks
    </Directory>
</VirtualHost>
```
