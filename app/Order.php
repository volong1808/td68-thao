<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = true;
    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }
}
