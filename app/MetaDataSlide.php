<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaDataSlide extends Model
{
    protected $table = 'metadata_slide';

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class, 'id', 'post_id');
    }
}
