<?php

namespace App\Imports;

use App\Image;
use App\OptionPost;
use App\Post;
use App\Services\CategoryService;
use App\Services\CustomPostService;
use App\Services\OptionPostService;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Intervention\Image\ImageManagerStatic as ImageResize;

class ProductImport implements ToCollection
{
    public $customPostService;
    public $postType;
    public $pathDir;
    public $isUploadImage;


    public function __construct()
    {
        $this->isUploadImage = false;
        $this->customPostService = new CustomPostService('diy');
    }

    public function setPostType($postType)
    {
        $this->postType = $postType;
        $this->customPostService->setPostType($postType);
    }

    public function setImportImage($pathDir)
    {
        $this->pathDir = $pathDir;
        if (!empty($pathDir)) {
            $this->isUploadImage = true;
        }
    }

    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            $item = $row->all();
            $imageName = $item[7];
            $alt = $item[2];
            $imageId = null;
            if ($this->isUploadImage) {
                $imageId = $this->importImage($imageName, $this->pathDir, $alt);
            }
            $postId = $this->importPost($item, $imageId);
            $this->insertCategorySelect($item[1], $postId);
            $this->insertMetaDat($item, $postId);
            $this->insertMaterial($item[3], $postId);
            $this->insertSeoData($item[5], $item[6], $imageId, $postId);
        }
    }

    public function importImage($imageName, $pathDir, $alt = '')
    {
        $pathOrigin = "{$pathDir}/{$imageName}";
        $pathThumb = "{$pathDir}/thumb_{$imageName}";
        $filePath = "./{$pathOrigin}";
        if (!file_exists($pathOrigin)) {
            return null;
        }
        $imageConfig = $this->customPostService->getImageConfig();
        $originSize = [
            'width' => config('default_image.WIDTH'),
            'height' => config('default_image.HEIGHT'),
        ];
        $thumbSize = [
            'width' => config('default_image.THUMB_WIDTH'),
            'height' => config('default_image.THUMB_HEIGHT'),
        ];
        if (!empty($imageConfig['size']['thumb'])) {
            $thumbSize = $imageConfig['size']['thumb'];
        }
        if (!empty($imageConfig['size']['origin'])) {
            $originSize = $imageConfig['size']['origin'];
        }
        ImageResize::make(base_path($pathOrigin))->fit($thumbSize['width'], $thumbSize['height'])->save(base_path($pathThumb));
        $image = new Image();
        $image->origin = $pathOrigin;
        $image->url = $pathOrigin;
        $image->thumb = $pathThumb;
        $image->file_name = $pathOrigin;
        $image->file_path =  $filePath;
        $image->width = $originSize['width'];
        $image->height = $originSize['height'];
        $image->name = $imageName;
        $image->size = filesize(base_path() . DIRECTORY_SEPARATOR .$pathOrigin);
        $image->alt = $alt;
        $image->save();
        return $image->id;
    }

    public function importPost($item, $image)
    {
        $post = new Post();
        $post->name = $item[2];
        $post->slug = str_slug($item[2]);
        $post->description = $item[6];
        $post->content = $item[6] . !empty($item[9]) ? '<br/>' . $item[9] : '';
        $post->post_type = $this->postType;
        $post->state = config('constants.POST_STATE.PUBLISHED');
        $post->image_id = $image;
        $post->save();
        return $post->id;
    }

    public function insertMetaDat($item, $postId)
    {
        $data = [
            'post_id' => $postId,
            'price_default' => null,
            'code' => strtoupper($item[0]),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        DB::table('metadata_' . $this->postType)->insert($data);
    }

    public function insertMaterial($materials, $postId )
    {
        $optionPostService = new OptionPostService($this->postType);
        $arrMaterials = explode(',' , $materials);

        foreach ($arrMaterials as $item) {
            $item = trim($item);
            $materialAndSize = explode(':', $item);
            if (empty($materialAndSize[0])) {
                continue;
            }

            $materialModal = new OptionPost();
            $materialStr = trim($materialAndSize[0]);
            $material = $materialModal->where('content', 'like', '%"code":"' . $materialStr . '"%')->first();

            if (empty($material)) {
                continue;
            }

            $content = [];

            if (!empty($materialAndSize[1])) {
                $optionPostService->setType('size');
                $sizeStr = trim($materialAndSize[1]);
                $sizeArr = explode('-', $sizeStr);
                $sizes = [];
                if (!empty($sizeArr)) {
                    $sizes = $optionPostService->getNameIn($sizeArr);
                }
                if (!empty($sizes)) {
                    $sizes = getValuesByKey($sizes, 'id');
                    $content[$material->id] = ['size' => $sizes];
                }
            }
            $optionPostService->savePostToPostOption([$material->id], $postId, null, $content);
        }
    }

    public function insertSeoData($name, $description, $imageId, $postId, $itemType = 'post')
    {

        $data = [
            'name' => $name,
            'description' => $description,
            'image_id' => $imageId,
            'item_type' => $itemType,
            'item_id' => $postId,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        DB::table('seo_data')->insert($data);
    }

    public function insertCategorySelect($categoryName, $postId)
    {
        $categoryService = new CategoryService();
        $category = $categoryService->getByName($categoryName);
        if (!empty($category)) {
            $dataUpdate = [
                'category_id' => $category->id,
                'post_id' => $postId
            ];
            return DB::table('category_post')->insertGetId($dataUpdate);
        }
        return false;
    }
}
