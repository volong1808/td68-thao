<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Post
 * @package App
 */
class Post extends Model
{
    /**
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return HasOne
     */
    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function slide()
    {
        return $this->hasOne(MetaDataSlide::class, 'post_id', 'id');
    }

    public function product()
    {
        return $this->hasOne(MetaDataProduct::class, 'post_id', 'id');
    }

    public function option()
    {
        return $this->belongsToMany(OptionPost::class, 'post_option_post', 'post_id', 'option_post_id');
    }

    /**
     * @return HasOne
     */
    public function video()
    {
        return $this->hasOne(MetaDataVideo::class, 'post_id', 'id');
    }

    public function faq()
    {
        return $this->hasOne(MetaDataFaq::class, 'post_id', 'id');
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function postSeo()
    {
        return $this->hasOne(SeoData::class, 'item_id', 'id')
            ->where('item_type', 'post');
    }
}
