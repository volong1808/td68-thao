<?php

namespace App\Services;

use App\Image;
use App\User;
use Illuminate\Support\Facades\Session;

class UserService {
    public $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function add($data, $role = null)
    {
        if (empty($role)) {
            $role = config('constants.role.USER');
        }
        $this->user->name = $data['name'];
        $this->user->email = $data['email'];
        $this->user->username = $data['username'];
        $this->user->phone = $data['phone'];
        $this->user->password = bcrypt($data['password']);
        $this->user->role = $role;
        return $this->user->save();
    }

    public function updatePassword($username, $passwordNew)
    {
        $user = $this->getByUserName($username);
        if (empty($user)) {
            return false;
        }
        $user->password = bcrypt($passwordNew);
        return $user->save();
    }

    public function getByEmail($email)
    {
        return $this->user->where('email', $email)->first();
    }

    public function updateProfile($username, $request){
        $user = $this->getByUserName($username);
        if (empty($user)) {
            return false;
        }
        if ($request->hasFile('avatar')) {
            if ($user->avatar) {
                $image = Image::find($user->avatar);
                deleteFileDatabase($image->url);
                deleteFileDatabase($image->thumb);
                deleteFileDatabase($image->origin);
                $image->delete();
            }
            $user->avatar = $this->saveAvatar($request);
        }
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->phone = $request['phone'];

        return $user->save();
    }

    public function saveAvatar($request)
    {
        $imagedetails = getimagesize($request->avatar->getPathName());
        $imageOrigin = getUploadImageUrl(
            $request->avatar,
            $imagedetails[0],
            $imagedetails[1],
            'origin'
        );
        $imageInfo = getUploadImageUrl(
            $request->avatar,
            config("constants.avatar.WIDTH"),
            config("constants.avatar.HEIGHT")
        );
        $thumpInfo = getUploadImageUrl(
            $request->avatar,
            config("constants.avatar.THUMB_WIDTH"),
            config("constants.avatar.THUMB_HEIGHT"),
            'thumb'
        );
        $newImage = new Image();
        $newImage->origin = $imageOrigin['url'];
        $newImage->url = $imageInfo['url'];
        $newImage->thumb = $thumpInfo['url'];
        $newImage->file_name = $imageInfo['filename'];
        $newImage->file_path = $imageInfo['filePath'] . $imageInfo['filename'];
        $newImage->width = config("constants.avatar.WIDTH");
        $newImage->height = config("constants.avatar.HEIGHT");
        $newImage->name = $request->name;
        $newImage->size = filesize(base_path() . DIRECTORY_SEPARATOR . $imageInfo['url']);
        $newImage->alt = $request->name . ' avatar';

        $newImage->save();
        return $newImage->id;
    }

    public function getByUserName($username)
    {
        return $this->user->where('username', $username)->first();
    }

    public function isLogged($username)
    {
        $user = $this->getByUserName($username);
        $userLogin = $this->getUserApiLogin();
        if (!empty($userLogin['username']) && $userLogin['username'] == $user->username) {
            return false;
        }
        $loginStatus = $user->is_api_login;
        $isLoginStatus = config('constants.IS_API_LOGIN');
        $timeWait = time() - $user->time_login;
        $maxTimeWait = env('MAX_TIME_WAIT_LOGIN');
        if ($loginStatus == $isLoginStatus && $timeWait <= $maxTimeWait) {
            return true;
        }
        return false;
    }

    public function setStatusLogged($username, $status)
    {
        $user = $this->getByUserName($username);
        $user->is_api_login = $status;
        return $user->save();
    }

    public function setTimeLogin($username, $time = null)
    {
        $user = $this->getByUserName($username);
        $user->time_login = $time;
        return $user->update();
    }

    public function setUserApiLogin($username)
    {
        $user = $this->getByUserName($username);
        $dataUser = [
            'username' => $user->username,
            'is_api_login' => $user->is_api_login,
            'time_login' => $user->time_login,
        ];
        Session::put('loginApiUser', $dataUser);
        return Session::save();
    }

    public function getUserApiLogin()
    {
        return Session::get('loginApiUser');
    }

    public function verifyUserApiLogin($username, $password, $role)
    {
        $status = config('constants.USER_STATUS.ACTIVE');
        $user =  $this->user->where('username', $username)
            ->where('role', $role)->where('status', $status)
            ->first();
        if (empty($user)) {
            return false;
        }
        if (password_verify($password, $user->password)) {
            return true;
        }
        return false;
    }

    public function apiLogout()
    {
        $userLogin = $this->getUserApiLogin();
        $user = $this->getByUserName($userLogin['username']);
        if (empty($user)) {
            return false;
        }
        $user->time_login = null;
        $user->is_api_login = 0;
        Session::forget('loginApiUser');
        Session::save();
        return $user->save();
    }
}