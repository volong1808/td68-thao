<?php

namespace App\Services;


use Zipper;

class FileService extends BaseService
{
    public function uploadFile()
    {

    }

    public function unZipper($zipCurrentPath, $extractPath)
    {
        return Zipper::make($zipCurrentPath)->extractTo($extractPath);
    }
}