<?php

namespace App\Services;


class BaseService
{
    protected function resultByState($result, $state = null, $table = 'posts')
    {
        if (!empty($state)) {
            $result->where($table . '.state', $state);
        }
        return $result;
    }

    protected function resultPage($result, $page = null)
    {
        if (!empty($page)) {
            return $result->paginate($page);
        }
        return $result->get();
    }
}