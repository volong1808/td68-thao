<?php

namespace App\Services;

use App\Post;

class PostService extends BaseService
{
    public $post;
    public $serviceCategory;

    public function __construct()
    {
        $this->post = new Post();
        $this->serviceCategory = new CategoryService();
    }

    public function getByType($type, $page = 15, $state = null)
    {
        $result = $this->post->where('post_type', $type)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->orderBy('order', 'ASC')
            ->orderBy('updated_at', 'DESC');
        $result = $this->resultByState($result, $state);
        return $this->resultPage($result, $page);
    }

    public function getList($postType, $page = 15, $state = null)
    {
        $result = $this->post->where('post_type', $postType)
            ->with('image')
            ->with('categories')
            ->orderBy('updated_at', 'DESC');
        $result = $this->resultByState($result, $state);
        return $this->resultPage($result, $page);
    }

    public function getListWithMetaData($postType, $metaDataSelect = [], $state = 1, $page = null)
    {
        $metaDataSelect[] = 'posts.*';
        $metaDataTable = "metadata_{$postType}";
        $result = $this->post->where('post_type', $postType)
            ->with('image')
            ->orderBy('updated_at', 'DESC')
            ->join($metaDataTable, $metaDataTable . '.post_id', '=', 'posts.id');

        $result = $this->resultByState($result, $state);

        return $this->resultPage($result, $page);
    }

    public function getListWithConditionInMetaData($postType, $conditions, $metaDataSelect = [], $state = 1, $page = null)
    {
        $metaDataSelect[] = 'posts.*';
        $metaDataTable = "metadata_{$postType}";
        $result = $this->post->where('post_type', $postType)->select($metaDataSelect)
            ->where($conditions)
            ->with('image')
            ->orderBy('order', 'ASC')
            ->orderBy('updated_at', 'DESC')
            ->join($metaDataTable, $metaDataTable . '.post_id', '=', 'posts.id');

        $result = $this->resultByState($result, $state);
        return $this->resultPage($result, $page);
    }

    public function getDetailWithMetaDataSlug($slug, $postType, $metaDataSelect = [], $state = 1)
    {
        $metaDataSelect[] = 'posts.*';
        $metaDataTable = "metadata_{$postType}";
        $result = $this->post->where('post_type', $postType)->select($metaDataSelect)
            ->where('slug', $slug)
            ->with('image')
            ->orderBy('order', 'ASC')
            ->orderBy('updated_at', 'DESC')
            ->leftJoin($metaDataTable, $metaDataTable . '.post_id', '=', 'posts.id');
        return $this->resultByState($result, $state)->first();
    }

    public function getBySlug($posType, $slug, $state = null)
    {
        $result = $this->post->where('slug', $slug)->where('post_type', $posType)
            ->with('image')
            ->orderBy('updated_at', 'DESC');
        $result = $this->resultByState($result, $state);
        return $result->first();
    }

    public function getByCateId($cateId, $postType, $state = 1, $page = null)
    {
        $result = $this->post->select('posts.*', 'categories.slug as cate_slug')
            ->where('posts.post_type', $postType)
            ->where('category_post.category_id', $cateId)
            ->join('category_post', 'category_post.post_id', '=', 'posts.id')
            ->join('categories', 'category_post.category_id', '=', 'categories.id')
            ->with('image')
            ->with('categories');

        $result = $this->resultByState($result, $state);
        return $this->resultPage($result, $page);
    }

    public function getDetailPost($customPostService, $postType, $postSlug, $state)
    {
        if ($customPostService->hasMetaData()) {
            $metaDataColumn = $customPostService->getColumnOfMetaData($postType);
            return $this->getDetailWithMetaDataSlug($postSlug ,$postType, $metaDataColumn, $state);
        }
        return $postDetail = $this->getBySlug($postType, $postSlug, $state);
    }

    public function getById($id, $postType, $state = null)
    {
        $result = $this->post->where('id', $id)
            ->with('image')
            ->orderBy('updated_at', 'DESC');
        $result = $this->resultByState($result, $state);
        return $result->first();
    }

    public function getOtherPostByCateId($cateId, $postID, $postType, $state = 1, $page = null)
    {
        $result = $this->post->select('posts.*', 'categories.slug as cate_slug')
            ->where('posts.post_type', $postType)
            ->where('category_post.category_id', $cateId)
            ->whereNotIn('posts.id', [$postID])
            ->join('category_post', 'category_post.post_id', '=', 'posts.id')
            ->join('categories', 'category_post.category_id', '=', 'categories.id')
            ->with('image');

        $result = $this->resultByState($result, $state);
        return $this->resultPage($result, $page);
    }

    public function getListOtherCustomPost($postType, $postID, $page = 15, $state = null)
    {
        $result = $this->post->where('post_type', $postType)
            ->whereNotIn('posts.id', [$postID])
            ->orderBy('updated_at', 'DESC');
        $result = $this->resultByState($result, $state);
        return $this->resultPage($result, $page);
    }

    public function getListSearch($conditions = [], $whereIn = [], $page = 12, $state = null)
    {
        $result = $this->post->where($conditions)
            ->whereIn('post_type', $whereIn)
            ->with('image')
            ->with('categories')
            ->orderBy('updated_at', 'DESC');
        $result = $this->resultByState($result, $state);
        return $this->resultPage($result, $page);
    }

    public function getListRandom($postType, $limit, $state = 1)
    {
        return $this->post->inRandomOrder()->where('post_type', $postType)->where('state', $state)->limit($limit)->get();
    }
}