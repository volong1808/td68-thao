<?php

namespace App\Services;

use App\Order;
use App\OrderItem;
use Illuminate\Support\Facades\Session;
use PharIo\Manifest\Email;

class OrderService
{
    public $order;
    public $orderItem;
    public $cartName = 'cart';

    public function __construct()
    {
        $this->order = new Order();
        $this->orderItem = new OrderItem();
    }

    public function getLastOder()
    {
        return $this->order->orderBy('id', 'DESC')->first();
    }

    /**
     * @param $planId
     * @param $productId
     * @return string
     */
    public function getCode($planId, $productId)
    {
        $lastId = 1;
        $lastCoder = $this->getLastOder();
        if (!empty($lastCoder)) {
            $lastId = $lastCoder->id;
        }

        return date('dmyHis') . $planId . $productId . $lastId;
    }

    /**
     * @param $price
     * @param $quantity
     * @param $request
     * @param array $option
     * @param null $optionService
     * @return float|int
     */
    public function getCalcPrice($price, $quantity, $request, $option = [], $optionService = null)
    {
        $price = $price * $quantity;
        if (!empty($option)) {
            $optionSelect = $this->getValueInputOption($option, $request->all());
            $optionPrice = $this->getOptionPrice($optionSelect, $optionService);
            $priceOption = $this->getCalcPriceProduct($optionPrice, $quantity);
            return $priceOption;
        }
        return $price;
    }

    /**
     * @param $detail
     * @param $quantity
     * @param $price
     * @param string $message
     * @param array $option
     */
    public function addToCart($detail, $quantity, $price, $message = '', $option = [])
    {
        $cart = Session::get($this->cartName);
        $cart[$detail->id]['item'] = $detail;
        $cart[$detail->id]['quantity'] = $quantity;
        $cart[$detail->id]['price'] = $price;
        $cart[$detail->id]['message'] = $message;
        $cart[$detail->id]['option'] = $option;
        return Session::put($this->cartName, $cart);
    }

    /**
     * @param $id
     * @return bool|void
     */
    public function deleteItemCart($id)
    {
        if (Session::has($this->cartName . '.' . $id)) {
            return Session::forget($this->cartName . '.' . $id);
        }
        return false;
    }

    /**
     * @param $id
     * @param $amount
     * @return bool|void
     */
    public function updateCartAmount($id, $amount)
    {
        if (Session::has($this->cartName . '.' . $id)) {
            if ($amount == 0) {
                return $this->deleteItemCart($id);
            } else {
                $cart = $this->getCart();
                $cart[$id]['quantity'] = $amount;
                return Session::put($this->cartName, $cart);
            }

        }
        return false;
    }

    public function getCart()
    {
        return Session::get($this->cartName);
    }

    public function removeCart()
    {
        return Session::forget($this->cartName);
    }

    public function add($data)
    {
        $this->order->name = $data['name'];
        $this->order->phone_number = $data['phone'];
        $this->order->code = $data['code'];
        $this->order->username = $data['username'];
        $this->order->email = $data['email'];
        $this->order->address = !empty($data['address']) ? $data['address'] : '';
        $this->order->payment_type = $data['payment_type'];
        $this->order->status = $data['status'];
        $this->order->message = !empty($data['message']) ? $data['message'] : '';
        $this->order->total = $data['total'];
        $this->order->save();
        if ($this->order->save()) {
            return $this->order->id;
        }
        return false;
    }

    public function addItems($cart, $orderId)
    {
        foreach ($cart as $item) {
            if (empty($item['item']) || empty($item['item']->id)) {
                continue;
            }
            $this->addItem($item, $orderId);
        }
        $totalPriceOrderItem = $this->getTotalPriceOrderItemByOrderId($orderId);
        if (empty($totalPriceOrderItem)) {
            $this->deleteOrder($orderId);
        }
        return $this->updateTotalOrder($orderId, $totalPriceOrderItem);
    }

    public function addItem($data, $orderId)
    {
        $item = $data['item'];
        $this->orderItem = new OrderItem();
        $this->orderItem->order_id = $orderId;
        $this->orderItem->item_id = $item['id'];
        $this->orderItem->message = !empty($data['message']) ? $data['message'] : '';
        $this->orderItem->price = !empty($data['price']) ? $data['price'] : '';
        $this->orderItem->quantity = !empty($data['quantity']) ? $data['quantity'] : '';
        $this->orderItem->options = !empty($data['options']) ? json_decode($data['options']) : '';
        $this->orderItem->discount = !empty($data['discount']) ? $data['discount'] : '';
        $this->orderItem->state = config('constants.ORDER_ITEM_STATUS.ORDER');
        return $this->orderItem->save();
    }

    public function getTotalPriceOrderItemByOrderId($orderId)
    {
        return $this->orderItem->where('order_id', $orderId)->sum('price');
    }

    public function deleteOrder($id)
    {
        return $this->order->find($id)->delete();
    }

    public function updateTotalOrder($id, $total)
    {
        $order = $this->order->find($id);
        if (empty($order)) {
            return false;
        }
        $order->total = $total;
        return $order->save();
    }

    public function getByCode($code)
    {
        return $this->order->where('code', $code)->first();
    }

    public function updateStatusPaidByCode($code, $data)
    {
        $order = $this->getByCode($code);
        $order->status = $data['status'];
        if (!empty($data['transaction_no'])) {
            $order->transaction_no = $data['transaction_no'];
        }
        return $order->save();
    }

    public function getValueInputOption($options, $request)
    {
        $optionsSelect = [];
        foreach ($options as $key => $value) {
            if (!empty($request[$key])) {
                $optionsSelect[$key] = $request[$key];
            }
        }
        return $optionsSelect;
    }

    public function getOptionPrice($options, $optionService)
    {
        if (empty($options['materials']) || empty($options['size'])) {
            return null;
        }
        $priceOption = $optionService->getMappingByFromIdAndToId($options['materials'], $options['size']);
        if (empty($priceOption->content)) {
            return null;
        }
        $content = json_decode($priceOption->content, true);
        if (empty($content['price'])) {
            return null;
        }
        return $content['price'];
    }

    public function getOptionDetailInCart($cart)
    {
        $optionService = new OptionPostService();
        $customPostService = new CustomPostService();
        foreach ($cart as $key => $item) {
            if (empty($item['option'])) {
                continue;
            }
            $product = $item['item'];
            $optionService->setPostType($product->post_type);
            $customPostService->setPostType($product->post_type);
            foreach ($item['option'] as $optionName => $optionId) {
                $optionService->setType($optionName);
                $option = $optionService->getDetail($optionId);
                $cart[$key]['option'][$optionName] = $option;
                $cart[$key]['option_config'][$optionName] = $customPostService->getOption($optionName);
            }
        }
        return $cart;
    }

    public function getCalcPriceProduct($optionPrice, $quantity)
    {
        $total = ($optionPrice*$quantity);
        return $total;
    }

    /**
     * @return float|int
     */
    public function getTotalPrice()
    {
        $total = 0;
        $cart = $this->getCart();
        if (empty($cart)) {
            return $total;
        }

        foreach ($cart as $id => $item) {
            $total += $item['price'] * $item['quantity'];
        }

        return $total;
    }

    public function getRulesValid()
    {
        $paymentRules = '';
        $paymentMethod = config('option.banking');
        if (!empty($paymentMethod)) {
            $paymentMethodKey = implode(',', array_keys($paymentMethod));
            $paymentRules = 'required|in:' . $paymentMethodKey;
        }
        return [
            'payment_type' => $paymentRules,
            'name' => 'required|max:255',
            'address' => 'nullable|max:500',
            'email' => 'required|email',
            'phone' => 'regex:/^[0-9]{10,11}$/',
            'message' => 'nullable|max:500',
        ];
    }

    public function getAttributes()
    {
        return [
            'payment_type' => 'Hình Thức Thanh Toán',
            'name' => 'Họ Và Tên',
            'address' => 'Địa Chỉ',
            'email' => 'Email',
            'phone' => 'Số Điện Thoại',
            'message' => 'Ghi Chú',
        ];
    }

    public function countOrder()
    {
        return $this->order->count();
    }

    public function countOrderToday()
    {
        return $this->where('created_at', date('Y-m-d'))->count();
    }

    public function getNewCode($suffix = 'W')
    {
        $orderCountTotalToday = $this->countOrder();
        if (empty($orderCountTotalToday)) {
            $orderCountTotalToday = 1;
        }
        $prefix = str_pad($orderCountTotalToday, 3, '0', STR_PAD_LEFT);
        return $suffix . date('Ymd') . '-' . $prefix;
    }
}