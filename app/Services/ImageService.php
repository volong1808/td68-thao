<?php

namespace App\Services;

use App\Image;


class ImageService extends BaseService
{
    public $image;
    public $imageConfig;

    public function __construct()
    {
        $this->image = new Image();
    }

    public function setConfigImage($config)
    {
        $this->imageConfig = $config;
    }

    public function getById($id)
    {
        return $this->image->find($id);
    }

    public function save($request, $inputName)
    {
        $configImage = $this->imageConfig;
        $imagedetails = getimagesize($_FILES[$inputName]['tmp_name']);
        $imageOrigin = getUploadImageUrl(
            $request->image,
            $imagedetails[0],
            $imagedetails[1],
            'origin'
        );

        $imageInfo = [];
        $widthSizeDefault =   config("constants.default_image.WIDTH");
        $heightSizeDefault =   config("constants.default_image.HEIGHT");
        $originWidthSize = config("constants.default_image.WIDTH");
        $originHeightSize = config("constants.default_image.HEIGHT");
        if (!empty($configImage)) {
            foreach ($configImage['size'] as $key => $config) {
                $widthSize = array_get($config, 'width', $widthSizeDefault);
                $heightdSize = array_get($config, 'height', $heightSizeDefault);
                $imageInfo[$key] = getUploadImageUrl(
                    $request->{$inputName},
                    $widthSize,
                    $heightdSize,
                    $key
                );
                if ($key == 'origin') {
                    $originWidthSize = $widthSize;
                    $originHeightSize = $heightdSize;
                }
            }
        }

        if (empty($configImage) || empty($imageInfo['thumb']) || empty($imageInfo['origin'])) {
            $imageInfo['origin'] = getUploadImageUrl(
                $request->{$inputName},
                config("constants.default_image.WIDTH"),
                config("constants.default_image.HEIGHT"),
                'origin'
            );
            $imageInfo['thumb'] = getUploadImageUrl(
                $request->{$inputName},
                config("constants.default_image.THUMB_WIDTH"),
                config("constants.default_image.THUMB_HEIGHT"),
                'thumb'
            );
        }

        $newImage = new Image();
        $newImage->origin =$imageOrigin['url'];
        $newImage->url = $imageInfo['origin']['url'];
        $newImage->thumb = $imageInfo['thumb']['url'];
        $newImage->file_name = $imageInfo['origin']['filename'];
        $newImage->file_path =  $imageInfo['origin']['filePath'] .  $imageInfo['origin']['filename'];
        $newImage->width = $originWidthSize;
        $newImage->height = $originHeightSize;
        $newImage->name = $request->name;
        $newImage->size = filesize(base_path() . DIRECTORY_SEPARATOR . $imageInfo['origin']['url']);
        $newImage->alt = $request->name . ' image';
        $newImage->save();
        return $newImage->id;
    }
}