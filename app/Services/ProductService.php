<?php

namespace App\Services;

use App\MetaDataProduct;

class ProductService
{
    public $product;

    public function __construct()
    {
        $this->product = new MetaDataProduct();
    }

    public function getDetail($slug)
    {
        return $this->product->with(['post' => function ($query) use ($slug) {
            $query->where('state', config('constants.POST_STATE.PUBLISHED'))
                ->where('slug', $slug)
                ->with('image')
                        ->with(['galleries' => function ($query) {
                            $query->with('image');
                        }]);
        }])->first();
    }

    public function getById($id)
    {
        return $this->product->with(['post' => function ($query) {
            $query->where('state', config('constants.POST_STATE.PUBLISHED'))
                ->with('image')
                ->with(['galleries' => function ($query) {
                    $query->with('image');
                }]);
        }])->where('id', $id)->first();
    }

    public function getAll()
    {
        return $this->product->with(['post' => function ($query) {
            $query->where('state', config('constants.POST_STATE.PUBLISHED'))
                ->with('image');
        }])->orderBy('id', 'DESC')
            ->get();
    }
}