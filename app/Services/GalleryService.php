<?php

namespace App\Services;

use App\Gallery;

class GalleryService extends BaseService
{
    public $gallery;

    public function __construct()
    {
        $this->gallery = new Gallery();
    }

    public function addToPost($imageList, $postId, $postCurrent = 0)
    {
        foreach ($imageList as $item) {
            $gallery = $this->getImageOfPost($postCurrent, $item);
            if (isset($gallery)) {
                $gallery->post_id = $postId;
                $gallery->save();
            }
        }
    }

    public function getImageOfPost($postId, $imageId)
    {
        return $this->gallery->where('post_id', $postId)->where($imageId)->first();
    }
}