<?php

namespace App\Services;

class BankingService {

    public $vnp_HashSecret;
    public $vnp_Url;
    public $vnp_TmnCode;

    public function __construct()
    {
        $this->vnp_TmnCode = env('VN_PAY_CODE');
        $this->vnp_HashSecret = env('VN_PAY_CHECKSUM');
        $this->vnp_Url = env('VN_PAT_URL');
    }

    public function getUrlBanking($dataOrder, $urlReturn)
    {
        $inputData = $this->getConfig($dataOrder, $urlReturn);

        if (isset($dataOrder['bankCode']) && $dataOrder['bankCode'] != "") {
            $inputData['vnp_BankCode'] = $dataOrder['bankCode'];
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . $key . "=" . $value;
            } else {
                $hashdata .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $this->vnp_Url . "?" . $query;
        if (isset($this->vnp_HashSecret)) {
            $vnpSecureHash = hash('sha256', $this->vnp_HashSecret . $hashdata);
            $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }
        return $vnp_Url;
    }

    public function getStageReturn($order, $data)
    {
        $inputData = $this->getReturnData($data);
        $returnData = [];
        $vnp_SecureHash = $inputData['vnp_SecureHash'];
        unset($inputData['vnp_SecureHashType']);
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $hashData = $this->getHashData($inputData);
        $secureHash = hash('sha256',$this->vnp_HashSecret . $hashData);
        $status = 0;
        if ($secureHash == $vnp_SecureHash) {
            if ($order->status == config('constants.ORDER_STATUS.NEW')) {
                if ($inputData['vnp_ResponseCode'] == '00') {
                    $status = config('constants.ORDER_STATUS.PAID');
                } else {
                    $status = config('constants.ORDER_STATUS.VN_PAY');
                }
                $returnData['RspCode'] = '00';
                $returnData['status'] = $status;
                $returnData['Message'] = 'Thanh toán thành công.';
            } else {
                $returnData['RspCode'] = '02';
                $returnData['Message'] = 'Đơn hàng sẻ được xác nhận lại.';
            }
        } else {
            $returnData['RspCode'] = '97';
            $returnData['Message'] = 'Chữ ký không hợp lệ.';
        }

        return $returnData;
    }

    public function getConfig($dataOrder, $urlReturn)
    {
        $vnp_Returnurl = $urlReturn;
        $vnp_TxnRef = $dataOrder['code'];
        $vnp_OrderInfo = "Thanh toán hóa đơn số {$dataOrder['code']}";  // nội dung
        $vnp_OrderType = env('VN_PAY_ORDER_TYPE');;
        $vnp_Amount = $dataOrder['total'] * 100; // số tiền
        $vnp_Locale = 'vn';
        $vnp_IpAddr = request()->ip();

        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => $this->vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        return $inputData;
    }

    public function getReturnData($data)
    {
        $inputData = [];
        foreach ($data as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }
        return $inputData;
    }

    public function getHashData($inputData )
    {
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . $key . "=" . $value;
            } else {
                $hashData = $hashData . $key . "=" . $value;
                $i = 1;
            }
        }
        return $hashData;
    }
}