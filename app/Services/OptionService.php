<?php

namespace App\Services;

use App\Option;

class OptionService
{
    /**
     * @var Option
     */
    public $option;

    /**
     * OptionService constructor.
     */
    public function __construct()
    {
        $this->option = new Option();
    }

    /**
     * @param $key
     * @return bool
     */
    public function setNew($key)
    {
        $this->option->key = $key;
        return $this->option->save();
    }

    /**
     * @param $key
     * @param $content
     * @return mixed
     */
    public function update($key, $content)
    {
        $option =  $this->getOptionByKey($key);
        $option->content = $content;
        return $option->save();
    }

    /**
     * @param $configForm
     * @param array $data
     * @return array
     */
    public function initValue($configForm, $data = [])
    {
        $valueForm = [];
        foreach ($configForm as $key => $value)
        {
            if (!empty($data[$key])) {
                $valueForm[$key] = $data[$key];
            } else {
                $valueForm[$key] = '';
            }
        }
        return $valueForm;
    }

    /**
     * @param $key
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getConfigOptionByKey($key)
    {
        $configForm = config("option.{$key}");
        return $configForm;
    }

    /**
     * @param $configForm
     * @param $request
     * @param $oldOption
     * @return array
     */
    public function getFormData($configForm, $request, $oldOption)
    {
        $formData = [];
        foreach ($configForm as $key => $value)
        {
            $inputName = $value['inputName'];
            if (!empty($request->{$inputName})) {
                if ($value['inputType'] == 'file' && $request->hasFile($inputName)) {
                    $formData['file'][$inputName] = $request->{$inputName};
                } else {
                    $formData['text'][$inputName] = $request->{$inputName};
                }
            } else {
                $formData['text'][$key] = $oldOption[$inputName];
            }
        }
        return $formData;
    }

    /**
     * @param $image
     * @param $nameInput
     * @return array
     */
    public function uploadImage($image, $nameInput)
    {
        $imagedetails = getimagesize($_FILES[$nameInput]['tmp_name']);
        $imageOrigin = getUploadImageUrl(
            $image,
            $imagedetails[0],
            $imagedetails[1],
            'origin'
        );
        return $imageOrigin;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function getContentOptionByKey($key)
    {
        $option = $this->getOptionByKey($key);
        if (!empty($option)) {
            return json_decode($option->content, true);
        }
        return null;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getOptionByKey($key)
    {
        return $this->option->where('key', $key)->first();
    }

    /**
     * @param $key
     * @param $itemName
     * @return null
     */
    public function getItemByKey($key, $itemName)
    {
        $content = $this->getContentOptionByKey($key);
        if (isset($content[$itemName])) {
            return $content[$itemName];
        }
        return null;
    }
}