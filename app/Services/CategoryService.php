<?php

namespace App\Services;

use App\Category;

class CategoryService extends BaseService
{
    public $category;

    public function __construct()
    {
        $this->category = new Category();
    }

    public function getList($postType, $page = 15)
    {
        return $this->category->where('post_type', $postType)
            ->orderBy('updated_at', 'DESC')->paginate($page);
    }

    public function getCateByPostType($postType, $state = 1)
    {
        $categoryContent = [];
        $parentCat = $this->getParentCate($postType, $state);
        if (empty($parentCat)) {
            return $parentCat;
        }

        foreach ($parentCat as $parent) {
            $parent->subCate = $this->getSubCate($parent, $state, 'categories');
            $categoryContent[$parent->id] = $parent;
        }
        return $categoryContent;
    }

    public function getParentCate($postType, $state = null)
    {
        $result = $this->category->where('parent_id', null)->where('post_type', $postType);
        return $this->resultByState($result, $state, 'categories')->get();
    }

    public function getSubCate($parent, $state = null)
    {
        $result = $this->category->where('parent_id', $parent->id);
        $result = $this->resultByState($result, $state, 'categories')->get();
        $subCate = $result;
        if (!empty($result)) {
            foreach ($subCate as $key => $sub) {
                $subChild = $this->getSubCate($sub, $state, 'categories');
                if (!empty($subChild)) {
                    $sub->subcat = $subChild;
                }
                $subCate[$key] = $sub;
            }
        }
        return $subCate;
    }

    public function getCateDetail($id, $state = null)
    {
        $result = $this->category->where('id', $id);
        if (!empty($state)) {
            $result->where('state', $state);
        }
        return $this->resultByState($result, $state, 'categories')->first();
    }

    public function getBySlug($slug, $state = null)
    {
        $result = $this->category->where('slug', $slug);
        return $this->resultByState($result, $state, 'categories')->first();
    }

    public function getParentOfChild ($categoryCurrent, &$parentCategory, $state = null)
    {
        if (!empty($categoryCurrent->parent_id)) {
            $categoryDetail = $this->category->where('id', $categoryCurrent->parent_id);
            $categoryDetail = $this->resultByState($categoryDetail, $state, 'categories')->first();
            if (empty($categoryDetail)) {
                return $parentCategory;
            }

            if (!empty($categoryDetail->parent_id)) {
                $parentCategory[] = $categoryDetail;
                $this->getParentCategory($categoryDetail, $parentCategory);
            }

            $parentCategory[] = $categoryDetail;
        }
    }

    public function getCateByPostId($postId, $postType, $state = null)
    {
        $result = $this->category->select('categories.*')
            ->where('categories.post_type', $postType)
            ->where('category_post.post_id', $postId)
            ->join('category_post', 'category_post.category_id', '=', 'categories.id')
            ->join('posts', 'category_post.post_id', '=', 'posts.id')
            ->with('image');

        return $this->resultByState($result, $state, 'categories')->first();
    }

    public function getByName($name)
    {
        return $this->category->where('name', $name)->first();
    }
}