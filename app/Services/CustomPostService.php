<?php

namespace App\Services;

class CustomPostService {
    public $postType;

    public function __construct($postType = null)
    {
        $this->postType = $postType;
    }

    public function setPostType($postType)
    {
        $this->postType = $postType;
    }

    public function getConfigPost()
    {
        return config('custom_post.' . $this->postType);
    }

    public function getTitle()
    {
        return array_get($this->getConfigPost(), 'title');
    }

    public function getOption($type)
    {
        return array_get($this->getConfigPost(), 'option.' . $type);
    }

    public function getContentOption($type)
    {
        return array_get($this->getOption($type), 'content');
    }

    public function getImageConfig()
    {
        return array_get($this->getConfigPost(), 'image');
    }

    public function getMetaDataConfig() {
        return array_get($this->getConfigPost(), 'meta_data');
    }

    public function getOptionConfig()
    {
        return array_get($this->getConfigPost(), 'option');
    }

    public function getColumnOfMetaData()
    {
        $columns = [];
        $configMetaData = $this->getMetaDataConfig();
        if (empty($configMetaData)) {
            return $columns;
        }

        foreach ($configMetaData as $columnConfig) {
            $columns[] = $columnConfig['inputName'];
        }

        return $columns;
    }

    public function hasMetaData()
    {
        $configMetaData = $this->getMetaDataConfig();
        if (!empty($configMetaData)) {
            return true;
        }
        return false;
    }

    public function hasOption()
    {
        $optionConfig = $this->getOptionConfig();
        if (!empty($optionConfig)) {
            return true;
        }
        return false;
    }
}