<?php

namespace App\Services;

use App\OptionPost;
use App\OptionPostOption;
use App\PostOptionPost;

class OptionPostService extends BaseService
{
    public $optionPost;
    public $postType;
    public $type;
    public $config;
    public $postOptionPost;
    public $optionPostOption;

    public function __construct($postType = null, $type = null)
    {
        $this->postType = $postType;
        $this->type = $type;
        $this->optionPost = new OptionPost();
        $this->postOptionPost = new PostOptionPost();
        $this->optionPostOption = new OptionPostOption();
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setPostType($PostType)
    {
        $this->postType = $PostType;
    }

    public function loadConfig($config)
    {
        $this->config = $config;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function getInputData($optionPost = [])
    {
        $content = $this->getContent($optionPost);
        $data = $this->getContentConfig($content);
        $data['name'] = empty($optionPost) ? '' : $optionPost->name;
        $data['state'] = empty($category) ? config('constants.POST_STATE.PUBLISHED') : $category->state;
        return $data;
    }

    public function getAll()
    {
        $result = $this->optionPost->where('option_post.post_type', $this->postType)
            ->where('option_post.type', $this->type)
            ->orderBy('updated_at', 'DESC')->get();
        return $result;
    }

    public function getOfPost($postId, $state = 1)
    {
        $result = $this->optionPost->where('option_post.post_type', $this->postType)
            ->select('option_post.*', 'post_id', 'option_post_id', 'post_option_post.order as option_post_order', 'post_option_post.content as post_option_post_content')
            ->where('option_post.type', $this->type)
            ->where('post_option_post.post_id', $postId)
            ->join('post_option_post', 'post_option_post.option_post_id', '=', 'option_post.id')
            ->join('posts', 'post_option_post.post_id', '=', 'posts.id')
            ->orderBy('updated_at', 'DESC');
        return $this->resultByState($result, $state)->get();

    }

    public function getNameIn($names)
    {
        return $this->optionPost->whereIn('name', $names)->get();
    }

    public function getMappingByFromIdInToId($formId, $toIds)
    {
        return $this->optionPostOption->where('option_post_from_id', $formId)
        ->whereIn('option_post_to_id', $toIds)->get();
    }

    public function getOptionByIds($ids)
    {
        return $this->optionPost->whereIn('id', $ids)->get();
    }

    public function getMappingFromId($formId)
    {
        return $this->optionPostOption->where('option_post_from_id', $formId)->get();
    }

    public function getMappingByFromIdAndToId($formId, $toId)
    {
        return $this->optionPostOption->where('option_post_from_id', $formId)
            ->where('option_post_to_id', $toId)->first();
    }

    /**
     * @return OptionPost
     */
    public function getOptionPost()
    {
        return $this->optionPost;
    }

    public function getList($page = 15)
    {
        $result = $this->optionPost::where('post_type', $this->postType)
            ->where('type', $this->type)
            ->orderBy('updated_at', 'DESC');
        if (!empty($page)) {
            return $result->paginate($page);
        }
        return $result->get();
    }

    public function getDetail($id)
    {
        return $this->optionPost->select('option_post.*', 'post_id', 'option_post_id', 'post_option_post.order as option_post_order')
            ->where('option_post.id', $id)
            ->where('option_post.post_type', $this->postType)
            ->leftJoin('post_option_post', 'post_option_post.option_post_id', '=', 'option_post.id')
            ->leftJoin('posts', 'post_option_post.post_id', '=', 'posts.id')
            ->where('type', $this->type)->first();
    }

    public function save($request)
    {
        $contentOld = $this->getContent($this->optionPost);
        $this->optionPost->name = $request->name;
        $this->optionPost->content = $this->getContentInput($request, $contentOld);
        $this->optionPost->state = $request->state;
        $this->optionPost->post_type = $this->postType;
        $this->optionPost->type = $this->type;
        $this->optionPost->save();
        return $this->optionPost->id;
    }

    /**
     * @param $request
     * @param $optionPostFrom
     * @return array
     */
    public function saveOptionPostOption($request, $optionPostFrom)
    {
        $errors = [];
        foreach ($request->mapping as $key => $value) {
            foreach ($value as $mappingId) {
                $this->optionPostOption = new OptionPostOption();
                $this->optionPostOption->option_post_from_id = $optionPostFrom;
                $this->optionPostOption->option_post_to_id = $mappingId;
                $this->optionPostOption->type = $key;
                if (!empty($request->mapping_content[$mappingId])) {
                    $this->optionPostOption->content = json_encode($request->mapping_content[$mappingId]);
                }
                $saveResult = $this->optionPostOption->save();
                if ($saveResult !== true) {
                    $errors[$key] = $saveResult;
                }
            }
        }
        return $errors;
    }

    /**
     * @param $mapping
     * @param $optionPostFrom
     * @return array
     */
    public function updateOptionPostOption($mapping, $optionPostFrom)
    {
        $this->deleteOptionPostOption($optionPostFrom);
        return $this->saveOptionPostOption($mapping, $optionPostFrom);
    }

    public function deleteOptionPostOption($optionPostFrom)
    {
        return $this->optionPostOption->where('option_post_from_id', $optionPostFrom)->delete();
    }

    public function update($id, $request)
    {
        $this->optionPost = $this->getDetail($id);
        if (empty($this->optionPost)) {
            return false;
        }

        $this->save($request);
    }

    public function delete($id)
    {
        $this->optionPost = $this->getDetail($id);
        $this->postOptionPost->where('option_post_id', $id)->delete();
        return $this->optionPost->delete();
    }

    public function getOptionByPostType()
    {
        $data = [];
        foreach ($this->config as $key => $option) {
            $this->type = $key;
            $data[$key] = $this->getAll();
        }
        return $data;
    }

    public function getOptionOfPost($postId, $state = 1)
    {
        $data = [];
        foreach ($this->config as $key => $option) {
            $this->type = $key;
            $data[$key] = $this->getOfPost($postId, $state);
        }
        return $data;
    }

    public function savePostToPostOption($postOptionIds, $postId, $order = [], $content)
    {
        foreach ($postOptionIds as $optionPostId) {
            $contentItem = [];
            if (!empty($content[$optionPostId])) {
                $contentItem = $content[$optionPostId];
            }
            $postsOptionPost = new PostOptionPost();
            $postsOptionPost->post_id = $postId;
            $postsOptionPost->option_post_id = $optionPostId;
            $postsOptionPost->order = isset($order[$optionPostId]) ? $order[$optionPostId] : null;
            $postsOptionPost->content = !empty($contentItem) ? json_encode($contentItem) : null;
            $postsOptionPost->save();
        }
    }

    public function deleteByPostId($postId)
    {
        $postsOptionPost = new PostOptionPost();
        $postsOptionPost->where('post_id', $postId)->delete();
    }

    public function getValueContentByKey($key, $optionPost)
    {
        $content = $this->getContent($optionPost);
        if (isset($content[$key])) {
            return $content[$key];
        }
        return null;
    }

    public function getConfigContent()
    {
        $configOptionContent = [];
        if (!empty($this->config['content'])) {
            return $this->config['content'];
        }
        return $configOptionContent;
    }

    public function getGroupConfigFormInputOption($options)
    {
        $configFormGroup = [];
        foreach ($options as $index => $option) {
            if (isset($option['display']) && $option['display'] === false) {
                continue;
            }
            $inputType = $option['typeInput'];
            $configFormGroup[$inputType][$index] = $option;
        }
        return $configFormGroup;
    }

    public function getAllConfigInputMapping()
    {
        $result = [];
        $config = $this->getAllMappingConfig();
        if (empty($config)) {
            return null;
        }
        foreach ($config as $key => $mapping) {
            $result[$key] = $this->getMappingByType($mapping);
        }
        return $result;
    }

    public function getConfigInputMappingByType()
    {
        $mapping = $this->getMappingConfig();
        if (empty($mapping)) {
            return null;
        }

        return $this->getMappingByType($mapping);
    }

    public function getMappingSelect($optionFromId, $selectFirst = true)
    {
        $selected = [];
        $mapping = $this->getMappingConfig();
        if (empty($mapping)) {
            return $selected;
        }
        foreach ($mapping as $key => $item) {
            $result = $this->optionPostOption
                ->where('option_post_from_id', $optionFromId)
                ->where('type', $key);
            if (!$selectFirst) {
                $selected[$key] = $result->get();
            } else {
                $selected[$key] = $result->first();
            }
        }
        return $selected;
    }

    protected function getContentConfig($content = [])
    {
        $data = [];
        if (!empty($this->config['content'])) {
            foreach ($this->config['content'] as $option) {
                if (isset($content[$option['inputName']])) {
                    $data[$option['inputName']] = $content[$option['inputName']];
                } else {
                    $data[$option['inputName']] = $option['valueDefault'];
                }
            }
        }
        return $data;
    }

    protected function getContentInput($request, $contentOld = [], $isJson = true)
    {
        $contentConfig = $this->getConfigContent();
        $content = [];

        if (empty($this->config['content'])) {
            return json_encode($content);
        }

        foreach ($contentConfig as $option) {
            $value = null;

            if (isset($request[$option['inputName']])) {
                $value = $request[$option['inputName']];
            }
            if ($option['inputType'] == config('constants.input_type.image') && !empty($value)) {
                $imageService = new ImageService();
                $imageService->setConfigImage($option);
                $value = $imageService->save($request, $option['inputName']);
            } elseif (empty($value)) {
                $value = isset($contentOld[$option['inputName']]) ? $contentOld[$option['inputName']] : '';
            }
            $content[$option['inputName']] = $value;
        }
        if (!$isJson) {
            return $content;
        }

        return json_encode($content);
    }

    protected function getContent($optionPost)
    {
        if (empty($optionPost)) {
            return null;
        }
        return json_decode($optionPost['content'], true);
    }

    protected function getAllMappingConfig()
    {
        $mapping = [];
        $config = $this->config;
        foreach ($config as $key => $item) {
            if (!empty($item['mappingOption'])) {
                $mapping[$key] = $item['mappingOption'];
            }
        }
        return $mapping;
    }

    protected function getMappingConfig()
    {
        if (!empty($this->config['mappingOption'])) {
            return $this->config['mappingOption'];
        }
        return null;
    }

    protected function getMappingByType($mapping)
    {
        foreach ($mapping as $key => $item) {
            $this->postType = $item['postType'];
            $this->setType($item['type']);
            $mapping[$key]['items'] = $this->getList(null);
        }
        return $mapping;
    }
}