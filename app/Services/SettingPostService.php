<?php

namespace App\Services;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SettingPostService
{
    public function getConfig($postType)
    {
        if (is_null($postType)) {
            return null;
        }
        return config('custom_post.' . $postType);
    }

    public function installMetaData($postType, $metaData)
    {
        $metaDataTableName = $this->createNameTable($postType);
        Schema::dropIfExists($metaDataTableName);
        Schema::create($metaDataTableName, function (Blueprint $table) use ($metaData) {
            $table->bigIncrements('id');
            $table->integer('post_id');
            $table->timestamps();
        });
        $afterColumn = 'post_id';
        foreach ($metaData as $column) {
            $this->saveColumnToTable($metaDataTableName, $column, $afterColumn);
            $afterColumn = $column['inputName'];
        }
    }

    public function updateMetaData($postType, $metaData)
    {
        $metaDataTableName = $this->createNameTable($postType);
        $afterColumn = 'post_id';
        if (Schema::hasTable($metaDataTableName)) {
            foreach ($metaData as $column) {
                $this->saveColumnToTable($metaDataTableName, $column, $afterColumn);
                $afterColumn = $column['inputName'];
            }
        }
    }

    protected function saveColumnToTable($tableName, $column, $afterColumn)
    {
        Schema::table($tableName, function (Blueprint $table) use ($tableName, $column, $afterColumn) {
            $table = $table->addColumn( $column['typeValue'], $column['inputName'], $column['option']);
            if (empty($column['isNotNull'])) {
                $table = $table->nullable();
            }
            if (!empty($column['default'])) {
                $table = $table->default($column['default']);
            }
            if (Schema::hasColumn($tableName, $column['inputName'])) {
                $table->change();
            } else {
                $table->after($afterColumn);
            }
        });
    }

    protected function createNameTable($name, $prefix = 'metadata')
    {
        $tableName = $name;
        if (!empty($prefix)) {
            $tableName = "{$prefix}_{$name}";
        }
        return $tableName;
    }
}