<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionPostOption extends Model
{
    protected $table = 'option_post_option';
    public $timestamps = true;

    public function fromOption()
    {
        return $this->hasOne(OptionPost::class, 'id', 'option_post_to_id');
    }
}
