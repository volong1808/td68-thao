<?php

use Intervention\Image\ImageManagerStatic as Image;

/**
 * Function get upload image
 *
 * @param $image
 * @param $maxWidth
 * @param $maxHeight
 * @return string
 */
if (!function_exists('getUploadImageUrl')) {
    function getUploadImageUrl($image, $maxWidth = 900, $maxHeight = 600, $prefix = '')
    {
        $uploadDirectoryPath = getUploadDirectoryPath();
        $fileName = $prefix . '_' . getFileNameUnique($uploadDirectoryPath, $image);
        copy($image->getPathName(), base_path($uploadDirectoryPath) . $fileName);
        $imageUrl = getFileStorageUrl($uploadDirectoryPath . $fileName);
        Image::make(base_path($imageUrl))->fit($maxWidth, $maxHeight)->save(base_path($imageUrl));
        return $imageInfo = [
            'url' => $imageUrl,
            'filename' => $fileName,
            'filePath' => $uploadDirectoryPath,
        ];
    }
}

/**
 * Function help to get upload directory path follow time
 *
 * @return string
 */
if (!function_exists('getUploadDirectoryPath')) {
    function getUploadDirectoryPath()
    {
        $currentTimestamp = time();
        createDirectory(config('constants.UPLOAD_PATH'));
        $yearDirectory_path = config('constants.UPLOAD_PATH') . date('Y', $currentTimestamp) . '/';
        $monthDirectory_path = $yearDirectory_path . date('m', $currentTimestamp) . '/';
        $dayDirectory_path = $monthDirectory_path . date('d', $currentTimestamp) . '/';
        createDirectory($yearDirectory_path);
        createDirectory($monthDirectory_path);
        createDirectory($dayDirectory_path);
        return $dayDirectory_path;
    }
}

/**
 * Function help to create directory
 *
 * @param string $directoryPath
 * @return boolean
 */
if (!function_exists('createDirectory')) {
    function createDirectory($directoryPath)
    {
        if (!is_dir($directoryPath)) {
            mkdir($directoryPath, 0777);
            return true;
        }
        return false;
    }
}

/**
 * Function help to get file name unique
 *
 * @param string $uploadPath
 * @param Object $image
 * @return string
 */
if (!function_exists('getFileNameUnique')) {
    function getFileNameUnique($uploadPath, $image)
    {
        $fileName = getRandomFileName($image);
        return $fileName;
    }
}

/**
 * Function help to get random file name (if duplicate file name)
 *
 * @param  Object $image
 * @return string
 */
if (!function_exists('getRandomFileName')) {
    function getRandomFileName($image)
    {
        $extension = $image->getClientOriginalExtension();
        $fileParts = explode('.', $image->getClientOriginalName());
        $newFileName = $fileParts[0] . '-' . date('YmdHis', time()) . '.' . $extension;
        return $newFileName;
    }
}

/**
 * Function help to get relative path use store in database
 *
 * @param string $absoluteFilePath
 * @return string
 */
if (!function_exists('getFileStorageUrl')) {
    function getFileStorageUrl($absoluteFilePath)
    {
        $pattern = "/\.\/upload\//";
        $fileUrl = preg_replace($pattern, 'upload/', $absoluteFilePath);
        return $fileUrl;
    }
}

/**
 * Function help to delete file from database url
 *
 * @param string $file
 * @return boolean
 */
if (!function_exists('deleteFileDatabase')) {
    function deleteFileDatabase($file)
    {
        if (filter_var($file, FILTER_VALIDATE_URL)) {
            return false;
        }
        $absolueFile = base_path($file);
        $result = deleteFile($absolueFile);
        return $result;
    }
}

/**
 * Function help to delete file
 *
 * @param string $absolueFile
 * @return boolean
 */
if (!function_exists('deleteFile')) {
    function deleteFile($absolueFile)
    {
        if (is_file($absolueFile)) {
            unlink($absolueFile);
            return true;
        }
        return false;
    }
}

/**
 * Function help to get upload image
 *
 * @param string $editorFile
 * @return array
 */
if ( ! function_exists('uploadImage')) {
    function uploadImage($editorFile)
    {
        $success = TRUE;
        $message = 'Bạn đã upload hình ảnh thành công. ';
        $absoluteFileUrl = getUploadImageUrl($editorFile, 1200, 1200);
        $file_url = asset($absoluteFileUrl);

        return [
            'success' => $success,
            'message' => $message,
            'file_url' => $file_url
        ];
    }
}
