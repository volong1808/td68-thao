<?php

namespace App\Helpers;

use App\Category;
use App\CategoryPost;
use App\Image;


/**
 * Trait CategoryHelper
 * @package App\Helpers
 */
trait CategoryHelper
{
    use PostHelper;
    /**
     * @param string $postType
     * @param Category $category
     *
     * @return array
     */
    public function getCategoryInputData($postType, $category = [])
    {
        $data = [];
        $data['name'] = empty($category) ? '' : $category->name;
        $data['description'] = empty($category) ? '' : $category->description;
        $data['content'] = empty($category) ? '' : $category->content;
        $data['slug'] = empty($category) ? '' : $category->slug;
        $data['state'] = empty($category) ? config('constants.POST_STATE.PUBLISHED') : $category->state;
        $data['image'] = empty($category) ? '' : Image::find($category->image_id);
        $data['parent_id'] =  empty($category) ? '' : $category->parent_id;
        return $data;
    }

    /**
     * @param $postType
     * @param Category $category
     * @return array
     */
    public function getCategoryOption($postType, $category = [])
    {
        $option = [0 => 'Danh Mục Cha'];
        $parentCategories = Category::whereNull('parent_id')->where('post_type', $postType)->get();
        foreach ($parentCategories as $parentCategory) {
            if (!empty($category) AND $category->id == $parentCategory->id) {
                continue;
            }
            $option[$parentCategory->id] = $parentCategory->name;
        }
        return $option;
    }

    /**
     * Function help to save the category
     *
     * @param $postType
     * @param $request
     * @param array $category
     * @return integer
     */
    public function saveCategory($postType, $request, $category = [])
    {
        $current = date('Y-m-d H:i:s');
        if (empty($category)) {
            $category = new Category();
            $category->created_at = $current;
        }
        if ($request->hasFile('image')) {
            $imageId = $this->savePostImage($postType, $request);
            if (!empty($category->image_id)) {
                $oldImageId = $category->image_id;
                $image = Image::find($oldImageId);
                deleteFileDatabase($image->url);
                deleteFileDatabase($image->thumb);
                deleteFileDatabase($image->origin);
                $image->delete();
            }
            $category->image_id = $imageId;
        }

        $category->post_type = $postType;
        $category->name = isset($request->name) ? $request->name : '';
        $category->description = isset($request->description) ? $request->description : '';
        $category->content = isset($request->content) ? $request->content : '';
        $category->slug = isset($request->slug) ? $request->slug : '';
        $category->state = isset($request->state) ? $request->state : 0;
        $category->parent_id = !empty($request->parent_id) ? $request->parent_id : null;
        $category->save();
        return $category->id;
    }

    /**
     * @param $category
     */
    public function deleteCategory($category)
    {
        if ($category->parent_id == null) {
            $childCategories = Category::where('parent_id', $category->id)->get();
            if (!empty($childCategories)) {
                foreach ($childCategories as $childCategory) {
                    $this->deleteCategoryPost($childCategory->id);
                    $childCategory->delete();
                }
            }

        }
        $this->deleteCategoryPost($category->id);
        $category->delete();
        return;
    }

    /**
     * @param $categoryId
     */
    public function deleteCategoryPost($categoryId)
    {
        CategoryPost::where('category_id', $categoryId)->delete();
        return;
    }
}
