<?php
namespace App\Helpers;

use App\Image;
use App\Page;
use App\SeoData;
use App\Services\CustomPostService;
use App\Services\OptionPostService;
use App\Services\OrderService;
use App\Services\PostService;
use Illuminate\Http\Request;
use App\Http\Requests\HandleOrder;
use Illuminate\Support\Facades\Auth;

trait OrderHelper
{
    public function addToCart(Request $request, $id)
    {
        $options = [];
        $orderService = new OrderService();
        $postService = new PostService();
        $postDetail = $postService->getById($id, $this->statePublish);
        if (empty($postDetail)) {
            abort(404);
        }
        $postType = $postDetail->post_type;
        $optionPostService = new OptionPostService($postType);
        $customPostService = new CustomPostService($postType);
        $metaDataField = $customPostService->getColumnOfMetaData();
        $postDetail = $postService->getDetailWithMetaDataSlug($postDetail->slug, $postType, $metaDataField, $this->statePublish);
        $this->customPostService->setPostType($postType);
        if ($this->customPostService->hasOption()) {
            $optionConfig = $this->customPostService->getOptionConfig();
            $optionPostService->loadConfig($optionConfig);
            $options = $optionPostService->getOptionOfPost($postDetail->id, $this->statePublish);
        }
        $quantity = !empty($request->quantity_handle) ? $request->quantity_handle : 1;
        $selectOption = $orderService->getValueInputOption($options, $request);
        $price = $orderService->getCalcPrice($postDetail->price_default, $quantity, $request, $options, $optionPostService);
        $message = $request->message;
        $orderService->addToCart($postDetail, $quantity, $price, $message, $selectOption);
        return redirect()->route('fr_order_list');
    }

    public function listCart()
    {
        $orderService = new OrderService();
        $cart = $orderService->getCart();
        if (empty($cart)) {
            return redirect()->route('home');
        }
        $cart = $orderService->getOptionDetailInCart($cart);
        $data = [
            'cart' => $cart,
            'title' => 'Giỏ Hàng',
        ];

        $this->pushData($data);
        $viewPath = "plugin.cart.list";
        return view($viewPath, $this->data);
    }

    public function removeItemCart($id)
    {
        $orderService = new OrderService();
        $orderService->deleteItemCart($id);
        return redirect()->route('fr_order_list');
    }

    public function updateQuantity($id, Request $request)
    {
        $quantity = $request->quantity;
        $orderService = new OrderService();
        $postService = new PostService();
        $postDetail = $postService->getById($id, $this->statePublish);
        if (empty($postDetail)) {
            abort(404);
        }
        $orderService->updateCartAmount($id, $quantity);
        $cart = $orderService->getCart();
        if (empty($cart)) {
            return redirect()->route('home');
        }
        $cart = $orderService->getOptionDetailInCart($cart);
        $viewPath = "plugin.cart.list_table";
        $data = [
            'cart' => $cart
        ];
        $dataResponsive = [
            'status' => true,
            'table' => view($viewPath, $data)->render()
        ];
        return response()->json($dataResponsive);
    }

    public function calcPrice(Request $request)
    {
        $options = [];
        $postType = $this->page->link_post_type;
        $optionPostService = new OptionPostService($postType);
        $orderService = new OrderService();
        $this->customPostService->setPostType($postType);
        $postSlug = $this->getParameterFromRoute('post_slug');
        $postDetail = $this->postService->getDetailPost(
            $this->customPostService,
            $postType,
            $postSlug,
            $this->statePublish
        );

        if (empty($postDetail)) {
            abort(404);
        }

        if ($this->customPostService->hasOption()) {
            $optionConfig = $this->customPostService->getOptionConfig();
            $optionPostService->loadConfig($optionConfig);
            $options = $optionPostService->getOptionOfPost($postDetail->id, $this->statePublish);
        }
        $price = $orderService->getCalcPrice($postDetail, $request, $options, $optionPostService);
        $unitMoney = config('constants.UNIT_MONEY');
        $data = [
            'price' => $price,
            'priceMoney' => number_format($price, 0, '', '.'),
            'unitMoney' => $unitMoney
        ];
        $dataResponsive = $this->getResponsiveAjaxSuccess($data);
        return response()->json($dataResponsive);
    }

    public function checkout(HandleOrder $request)
    {
        $data = $request->all();
        $orderService = new OrderService();
        $cart = $orderService->getCart();
        if (empty($cart)) {
            return redirect()->route('home');
        }
        $data['total'] = $orderService->getTotalPrice();
        $data['code'] = $orderService->getNewCode();
        $data['username'] = Auth::user()->username;
        $data['status'] = config('constants.ORDER_STATUS.NEW');
        $idOrder = $orderService->add($data);
        $orderService->addItems($cart, $idOrder);
        echo ('order thành công');
    }
}
