<?php
namespace App\Helpers;

use App\Image;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

include 'FormFileHelper.php';

trait UserHelper
{
    public function getInitUserData($user)
    {
        $initValue = [
            'username' => $user->username,
            'name' =>  $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'avatar' => $user->avatar,
            'password' => '',
            'new_password' => '',
            'new_password_confirm' => '',
        ];

        return $initValue;
    }

    public function saveUserData($request, $user, $isCreate = false)
    {
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if ($isCreate) {
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
        }
        if (!empty($request->role)) {
            $user->role = $request->role;
        }
        if(! empty($request->new_password)) {
            $user->password = bcrypt($request->new_password);
        }

        if(! empty($request->user_password)) {
            $user->password = bcrypt($request->user_password);
        }

        if ($request->hasFile('avatar')) {
            if (!empty($user) AND $user->avatar) {
                $image = Image::find($user->avatar);
                deleteFileDatabase($image->url);
                deleteFileDatabase($image->thumb);
                deleteFileDatabase($image->origin);
                $image->delete();
            }
            $user->avatar = $this->saveAvatar($request);
        }

        $user->save();
        return $user->id;
    }
    public function saveAvatar($request)
    {
        $imagedetails = getimagesize($request->avatar->getPathName());
        $imageOrigin = getUploadImageUrl(
            $request->avatar,
            $imagedetails[0],
            $imagedetails[1],
            'origin'
        );
        $imageInfo = getUploadImageUrl(
            $request->avatar,
            config("constants.avatar.WIDTH"),
            config("constants.avatar.HEIGHT")
        );
        $thumpInfo = getUploadImageUrl(
            $request->avatar,
            config("constants.avatar.THUMB_WIDTH"),
            config("constants.avatar.THUMB_HEIGHT"),
            'thumb'
        );
        $newImage = new Image();
        $newImage->origin = $imageOrigin['url'];
        $newImage->url = $imageInfo['url'];
        $newImage->thumb = $thumpInfo['url'];
        $newImage->file_name = $imageInfo['filename'];
        $newImage->file_path = $imageInfo['filePath'] . $imageInfo['filename'];
        $newImage->width = config("constants.avatar.WIDTH");
        $newImage->height = config("constants.avatar.HEIGHT");
        $newImage->name = $request->name;
        $newImage->size = filesize(base_path() . DIRECTORY_SEPARATOR . $imageInfo['url']);
        $newImage->alt = $request->name . ' avatar';

        $newImage->save();
        return $newImage->id;
    }
}
