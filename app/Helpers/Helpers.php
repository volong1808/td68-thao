<?php

use App\Services\OptionService;
use App\Services\PostService;
use App\Services\OptionPostService;
use App\Services\ImageService;

if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('getYoutubeIdFromUrl')) {
    //Get code youtube
    function getYoutubeIdFromUrl($url)
    {
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $qs);
            if (isset($qs['v'])) {
                return $qs['v'];
            } else {
                if ($qs['vi']) {
                    return $qs['vi'];
                }
            }
        }
        if (isset($parts['path'])) {
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path) - 1];
        }
        return null;
    }
}

if (!function_exists('getYoutubeThumbFromUrl')) {
    //Get code youtube
    function getYoutubeThumbFromUrl($url, $imageName = 0)
    {
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
        $thumbUrl = null;
        if (!empty($matches[1])) {
            $thumbUrl = "http://img.youtube.com/vi/{$matches[1]}/{$imageName}.jpg";
        }
        return $thumbUrl;
    }
}

if (!function_exists('strToHex')) {
    function strToHex($string)
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0' . $hexCode, -2);
        }
        return strToUpper($hex);
    }
}
if (!function_exists('hexToStr')) {
    function hexToStr($hex)
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }
        return $string;
    }
}

if (!function_exists('loadRouteCustomPost')) {
    function loadRouteCustomPost($customPostFile)
    {
        $customPostConfig = config($customPostFile);
        if (!empty($customPostConfig)) {
            foreach ($customPostConfig as $configPog) {
                creatRouteCustomPost($configPog['post_type']);
            }
        }
    }
}

if (!function_exists('creatRouteCustomPost')) {
    function creatRouteCustomPost($postType)
    {
        $controller = 'PostController';
        Route::group(['prefix' => 'post-' . $postType . '/{postType}', 'middleware' => ['check_post_type']], function () use ($controller, $postType) {
            if (!empty(config('custom_post.' . $postType . '.category'))) {
                $controllerCategory = 'PostCategoryController';
                Route::group(['prefix' => 'category'], function () use ($controllerCategory, $postType) {
                    Route::get('list', $controllerCategory .'@index')->name($postType . '_category_list');
                    Route::post('delete', $controllerCategory .'@delete')->name($postType . '_category_delete');
                    Route::post('bulk-action', $controllerCategory .'@bulkAction')->name($postType . '_category_bulk_action');
                    Route::get('add', $controllerCategory .'@add')->name($postType . '_category_add');
                    Route::post('add', $controllerCategory .'@handleAdd')->name($postType . '_category_add_submit');
                    Route::get('edit/{id}', $controllerCategory .'@edit')->name($postType . '_category_edit');
                    Route::post('edit/{id}', $controllerCategory .'@handleEdit')->name($postType . '_category_edit_submit');
                });
            }

            Route::get('list', $controller . '@index')->name($postType . '_list');
            Route::get('add',  $controller . '@add')->name($postType . '_add');
            Route::post('add/submit',  $controller . '@doAdd')->name($postType . '_add_submit');
            Route::post('delete',  $controller . '@delete')->name($postType . '_delete');
            Route::post('bulk-action',  $controller . '@bulkAction')->name($postType . '_bulk_action');
            Route::get('edit/{id}',  $controller . '@edit')->name($postType . '_edit');
            Route::post('do_edit/{id}',  $controller . '@doEdit')->name($postType . '_do_edit');

            if (!empty(config('custom_post.' . $postType . '.option'))) {
                $controllerCategory = 'OptionPostController';
                Route::group(['prefix' => 'option/{type}'], function () use ($controllerCategory, $postType) {
                    Route::get('list', $controllerCategory .'@index')->name($postType . '_option_list');
                    Route::post('delete', $controllerCategory .'@delete')->name($postType . '_option_delete');
                    Route::post('delete-ajax', $controllerCategory .'@deleteAjax')->name($postType . '_option_delete_ajax');
                    Route::post('bulk-action', $controllerCategory .'@bulkAction')->name($postType . '_option_bulk_action');
                    Route::get('add', $controllerCategory .'@add')->name($postType . '_option_add');
                    Route::post('add', $controllerCategory .'@handleAdd')->name($postType . '_option_add_submit');
                    Route::post('add-ajax', $controllerCategory .'@handleAddAjax')->name($postType . '_option_add_ajax');
                    Route::get('edit/{id}', $controllerCategory .'@edit')->name($postType . '_option_edit');
                    Route::post('edit/{id}', $controllerCategory .'@handleEdit')->name($postType . '_option_edit_submit');
                    Route::get('edit-ajax/{id}', $controllerCategory .'@editAjax')->name($postType . '_option_edit_ajax');
                    Route::post('edit-post-ajax/{id}', $controllerCategory .'@handleEditAjax')->name($postType . '_option_edit_post_ajax');

                });
            }
        });
    }
}

if (!function_exists('getOptionSelectFromTable')) {
    function getOptionSelectFromTable($table, $valueKey, $LabelOption, $isNull = true, $labelDefault = '--')
    {
        $options = [];
        if ($isNull) {
            $options[''] = $labelDefault;
        }
        if (!empty($table)) {
            foreach ($table as $row) {
                if (!empty($row[$valueKey]) && !empty($row[$LabelOption])) {
                    $options[$row[$valueKey]] =  $row[$LabelOption];
                }
            }
        }

        return $options;
    }
}

if (!function_exists('formatMoneyVI')) {
    function formatMoneyVI($price)
    {
        return number_format($price, 0, '.', ',') . ' VNĐ';
    }
}
if (!function_exists('getCustomPost')) {
    function getCustomPost($postType, $limit = '') {
        $postService = new PostService();

        return $postService->getList($postType, $limit, config('constants.POST_STATE.PUBLISHED'));
    }
}

if (!function_exists('getOptionByKey')) {
    function getOptionByKey($key) {
        $optionService = new OptionService();

        return $optionService->getContentOptionByKey($key);
    }
}

if (!function_exists('getSelectedOption')) {
    function getSelectedOption($option, $valueKey, $default = null) {
        $selected = [];
        if (empty($option)) {
            return $default;
        }
        foreach($option as $item) {
            $default[] = $item[$valueKey];
        }
        return $selected;
    }
}

if (!function_exists('getValuesByKey')) {
    function getValuesByKey($array, $key) {
        $result = [];
        if (empty($array)) {
            return $result;
        }
        foreach($array as $item) {
            $result[] = !empty($item[$key]) ? $item[$key] : null;
        }
        return $result;
    }
}

if (!function_exists('getMappingByKeyAndToIds')) {
    function getMappingByKeyAndToIds($toIds)
    {
        $optionService = new OptionPostService();
        return $optionService->getOptionByIds($toIds);
    }
}



if (!function_exists('getMappingByKeyInContent')) {
    function getMappingByKeyInContent($formId, $toIds, $key)
    {
        $result = [];
        $optionService = new OptionPostService();
        $mapping = $optionService->getMappingByFromIdInToId($formId, $toIds);
        if (empty($mapping)) {
            return $result;
        }
        foreach ($mapping as $value) {
            $content = [];
            if (!empty($value->content)) {
                $content = json_decode($value->content, true);
            }
            if (!empty($content[$key])) {
                $result[$value->option_post_to_id] = $content[$key];
            }
        }
        return $result;
    }
}

if (!function_exists('getConfigByKey')) {
    function getConfigByKey($key)
    {
        $configService = new OptionService();
        return $configService->getContentOptionByKey($key);
    }
}

if (!function_exists('getImageById')) {
    function getImageById($id)
    {
        $imageService = new ImageService();
        return $imageService->getById($id);
    }
}

if (!function_exists('getMappingByFromId')) {
    function getMappingByFromId($formId)
    {
        $optionService = new OptionPostService();
        return $optionService->getMappingFromId($formId);
    }
}

if (!function_exists('getContentPostOptionPost')) {
    function getContentPostOptionPost($postId, $postType, $type)
    {
        $optionService = new OptionPostService($postType, $type);
        return $optionService->getOfPost($postId);
    }
}

if (!function_exists('getImageContentOptionPost')) {
    function getImageContentOptionPost($contentOptionPost)
    {
        $content = json_decode($contentOptionPost, true);
        $image_id = null;

        if (!empty($content) && !empty($content['image'])) {
            $image_id = $content['image'];
        }

        if (empty($image_id)) {
            return null;
        }

        return getImageById($image_id);
    }
}

if (!function_exists('getValueContentOptionPostByKey')) {
    function getValueContentOptionPostByKey($contentOptionPost, $key)
    {
        $contents = json_decode($contentOptionPost, true);

        if (empty($contents) && empty($contents[$key])) {
            return null;
        }

        return $contents[$key];
    }
}

if (!function_exists('getMinPriceOptionPost')) {
    function getMinPriceOptionPost($options)
    {
        $prices = [];
        $minPriceKey = null;

        foreach ($options as $key=>$option) {
            $content = json_decode($option->content, true);
            if (!empty($content['price'])) {
                $prices[$key] = $content['price'];
            }
        }

        if (!empty($prices)) {
            $minPriceKey = array_search(min($prices), $prices);
        }

        if ($minPriceKey !== null) {
            $optionPost = $options[$minPriceKey];
            $valuePrice = getValueContentOptionPostByKey($optionPost->content, 'price');
            if ( $valuePrice !== null) {
                $data[$optionPost->id] = $valuePrice;
                return $data;
            }
        }

        return $minPriceKey;
    }
}

if (!function_exists('getStatusProduct')) {
    function getStatusProduct($status)
    {
        $dataStatus = "Còn Hàng";
        if (!empty($status)) {
            $dataStatus = config('custom_post.product.meta_data.status.item')[$status];
        }
        return $dataStatus;
    }
}

