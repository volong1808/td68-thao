<?php

namespace App\Helpers;

use Intervention\Image\ImageManagerStatic as Image;


class ImageHelper
{
    /**
     * @var $imageUrl
     */
    protected $imageUrl;

    /**
     * ImageHelper constructor.
     * @param null $imageUpload
     */
    public function __construct($imageUpload = null)
    {
        if (!empty($imageUpload)) {
            $this->imageUrl = $imageUpload->getPathname();
        }
    }

    /**
     * @param string $fileNameSave The path file to close
     * @param array $width The value of with width
     * @param array $height The value of with height
     * @return string
     */
    public function resizeImage($fileNameSave, $width = 1200, $height = 900)
    {
        $image = $this->get();
        if (empty($width) && empty($height)) {
            $width = $image->getWidth();
            $height = $image->getHeight();
        }
        $image->fit($width, $height);
        return $this->saveImage($image, $fileNameSave);
    }

    /**
     * @return \Intervention\Image\Image
     */
    public function get() {
        return Image::make($this->imageUrl);
    }

    /**
     * @param $imageUrl
     */
    public function setImageByUrl($imageUrl) {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @param $image
     * @param $fileNameSave
     * @return string
     */
    public function saveImage($image, $fileNameSave)
    {
        $ImageFilePath = base_path($fileNameSave);
        $image->save($ImageFilePath);
        if (file_exists($ImageFilePath)) {
            return $fileNameSave;
        }
        return '';
    }
}
