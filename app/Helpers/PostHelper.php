<?php
namespace App\Helpers;

use App\CategoryPost;
use App\Image;
use App\Post;
use App\SeoData;
use App\Services\CustomPostService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

include 'FormFileHelper.php';

trait PostHelper
{
    /**
     * @param $postType
     * @param array $post
     * @return array
     */
    public function getPostInputData($postType, $post = []) {
        $initValue = [];
        $initValue['name'] = empty($post) ? '' : $post->name;
        $initValue['order'] = empty($post) ? '' : $post->order;
        $initValue['description'] = empty($post) ? '' : $post->description;
        $initValue['content'] = empty($post) ? '' : $post->content;
        $initValue['slug'] = empty($post) ? '' : $post->slug;
        $initValue['state'] = empty($post) ? config('constants.POST_STATE.PUBLISHED') : $post->state;
        $initValue['image'] = empty($post) ? '' : Image::find($post->image_id);
        $initValue['categories'] =  empty($post) ? [] : $post->categories->pluck('id')->toArray();
        if (!empty($this->metadataFields)) {
            if (empty($post)) {
                foreach ($this->metadataFields as $field => $defaultValue) {
                    $initValue[$field] = $defaultValue;
                }
            } else {
                $metadata = DB::table("metadata_{$postType}")->where('post_id', $post->id)
                    ->first();
                foreach ($this->metadataFields as $field => $defaultValue) {
                    $initValue[$field] = empty($metadata) ? $defaultValue : $metadata->{$field};
                }
            }
        }
        if (isset($this->seoable) && $this->seoable) {
            if (!empty($post)) {
                $seoData = DB::table('seo_data')->where('item_type', 'post')
                    ->where('item_id', $post->id)->first();
                $initValue['seo_name'] = isset($seoData) ? $seoData->name : '';
                $initValue['seo_description'] = isset($seoData) ? $seoData->description : '';
            } else {
                $initValue['seo_name'] = '';
                $initValue['seo_description'] = '';
            }
        }
        return $initValue;
    }

    /**
     * @param $postType
     * @param $request
     * @param array $post
     * @return mixed
     */
    public function savePost($postType, $request, $post = [])
    {
        $current = date('Y-m-d H:i:s');
        if (empty($post)) {
            $post = new Post();
            $post->created_at = $current;
        }
        if ($request->hasFile('image')) {
            if (!empty($post) AND $post->image_id) {
                $image = Image::find($post->image_id);
                deleteFileDatabase($image->url);
                deleteFileDatabase($image->thumb);
                deleteFileDatabase($image->origin);
                $image->delete();
            }
            $post->image_id = $this->savePostImage($postType, $request);
        }
        $post->post_type = $postType;
        $post->name = isset($request->name) ? $request->name : '';
        $post->description = isset($request->description) ? $request->description : '';
        $post->content = isset($request->content) ? $request->content : '';
        $post->slug = isset($request->slug) ? $request->slug : '';
        $post->state = isset($request->state) ? $request->state : 0;
        $post->order = isset($request->order) ? $request->order : null;

        $post->save();
        if ($post->categories->count() > 0) {
            CategoryPost::where('post_id', $post->id)->delete();
        }
        if (! empty($request->categories)) {
            foreach ($request->categories as $category) {
                $catePost = new CategoryPost();
                $catePost->post_id = $post->id;
                $catePost->category_id  = $category;
                $catePost->save();
            }
        }

        if (!empty($this->metadataFields)) {
            $this->savePostMetadata($postType, $post->id, $request);
        }

        if (isset($this->seoable) && $this->seoable) {
            $this->saveSeoData($post, $request);
        }

        return $post->id;
    }

    public function saveSeoData($post, $request)
    {
        $seoData = SeoData::where('item_type', 'post')->where('item_id', $post->id)->first();
        if (!empty($seoData)) {
            $seoData->name = $request->seo_name;
            $seoData->description = $request->seo_description;
            $seoData->image_id = $post->image_id;
            $seoData->save();
        } else {
            $seoData = new SeoData();
            $seoData->name = $request->seo_name;
            $seoData->description = $request->seo_description;
            $seoData->item_type = 'post';
            $seoData->item_id = $post->id;
            $seoData->image_id = $post->image_id;
            $seoData->save();
        }
    }

    public function savePostMetadata($postType, $postId, $request)
    {
        $metadata = DB::table("metadata_{$postType}")->where('post_id', $postId)
            ->select()->first();
        $now = Carbon::now();
        if (empty($metadata)) {
            $metadata = [];
            $metadata['post_id'] = $postId;
            $metadata['created_at'] = $now;
            $metadata['updated_at'] = $now;
            foreach ($this->metadataFields as $field => $value) {
                $metadata[$field] = isset($request->{$field}) ? $request->{$field} : $value;
            }
            DB::table("metadata_{$postType}")->insert($metadata);
        } else {
            $updateMetadata = [];
            $updateMetadata['post_id'] = $metadata->post_id;
            $updateMetadata['created_at'] = $metadata->created_at;
            $updateMetadata['updated_at'] = $now;
            foreach ($this->metadataFields as $field => $value) {
                $updateMetadata[$field] = isset($request->{$field}) ? $request->{$field} : $value;
            }
            DB::table("metadata_{$postType}")->where('id', $metadata->id)->update($updateMetadata);
        }
    }

    /**
     * @param $postType
     * @param $request
     * @return bool
     */
    public function savePostImage($postType, $request)
    {
        $configService = new CustomPostService($postType);
        $configImage = $configService->getImageConfig();
        $imagedetails = getimagesize($_FILES['image']['tmp_name']);
        $imageOrigin = getUploadImageUrl(
            $request->image,
            $imagedetails[0],
            $imagedetails[1],
            'origin'
        );

        $imageInfo = [];
        $widthSizeDefault =   config("constants.default_image.WIDTH");
        $heightSizeDefault =   config("constants.default_image.HEIGHT");
        $originWidthSize = config("constants.default_image.WIDTH");
        $originHeightSize = config("constants.default_image.HEIGHT");
        if (!empty($configImage)) {
            foreach ($configImage['size'] as $key => $config) {
                $widthSize = array_get($config, 'width', $widthSizeDefault);
                $heightdSize = array_get($config, 'height', $heightSizeDefault);
                $imageInfo[$key] = getUploadImageUrl(
                    $request->image,
                    $widthSize,
                    $heightdSize,
                    $key
                );
                if ($key == 'origin') {
                    $originWidthSize = $widthSize;
                    $originHeightSize = $heightdSize;
                }
            }
        }

        if (empty($configImage) || empty($imageInfo['thumb']) || empty($imageInfo['origin'])) {
            $imageInfo['origin'] = getUploadImageUrl(
                $request->image,
                config("constants.default_image.WIDTH"),
                config("constants.default_image.HEIGHT"),
                'origin'
            );
            $imageInfo['thumb'] = getUploadImageUrl(
                $request->image,
                config("constants.default_image.THUMB_WIDTH"),
                config("constants.default_image.THUMB_HEIGHT"),
                'thumb'
            );
        }

        $newImage = new Image();
        $newImage->origin =$imageOrigin['url'];
        $newImage->url = $imageInfo['origin']['url'];
        $newImage->thumb = $imageInfo['thumb']['url'];
        $newImage->file_name = $imageInfo['origin']['filename'];
        $newImage->file_path =  $imageInfo['origin']['filePath'] .  $imageInfo['origin']['filename'];
        $newImage->width = $originWidthSize;
        $newImage->height = $originHeightSize;
        $newImage->name = $request->name;
        $newImage->size = filesize(base_path() . DIRECTORY_SEPARATOR . $imageInfo['origin']['url']);
        $newImage->alt = $request->name . ' image';
        $newImage->save();
        return $newImage->id;
    }

    public  function getGalleryImages($postId)
    {
        return DB::table('gallery')->where('post_id', $postId)
            ->join('images', 'image_id', '=', 'images.id')
            ->select('images.*')->get();
    }

    /**
     * @param $post
     */
    public function handleDelete($post)
    {
        $imagePath = base_path() . DIRECTORY_SEPARATOR . $post->image;
        if (file_exists($imagePath) AND !is_dir($imagePath)) {
            unlink($imagePath);
        }
        $post->delete();
        return;
    }
}
