<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'address' => 'nullable|max:500',
            'email' => 'required|email',
            'phone_number' => 'regex:/^[0-9]{10,11}$/',
            'subject' => 'required|max:500',
            'message' => 'required|max:500',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Họ và Tên',
            'address' => 'Địa Chỉ',
            'email' => 'Email',
            'phone_number' => 'Điện Thoại',
            'subject' => 'Chủ Đề',
            'message' => 'Nội Dung',
        ];
    }

    /**
     * @return array
     */
    public function  messages()
    {
        return [
            'required' => ':attribute không được để trống.',
            'regex' => ':attribute phải là số và có độ dài từ 10 đến 11 ký tự số.',
        ];
    }
}
