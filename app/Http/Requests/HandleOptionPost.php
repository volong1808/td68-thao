<?php

namespace App\Http\Requests;

use App\Services\CustomPostService;
use Illuminate\Foundation\Http\FormRequest;

class HandleOptionPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = $this->route()->parameter('type');
        $postType = $this->route()->parameter('postType');
        $customPostService = new CustomPostService($postType);
        $content = $customPostService->getContentOption($type);
        $rules = [
            'name' => 'required|max:255',
        ];

        foreach ($content as $key => $inputData)
        {
            if (!empty($inputData['valid'])) {
                $rules[$inputData['inputName']] = $inputData['valid'];
            }
        }
        return $rules;
    }

    public function attributes()
    {
        $attributes = [
            'name' => 'Tên Tùy Chọn'
        ];
        $type = $this->route()->parameter('type');
        $postType = $this->route()->parameter('postType');
        $customPostService = new CustomPostService($postType);
        $content = $customPostService->getContentOption($type);
        foreach ($content as $key => $inputData)
        {
            if (!empty($inputData['valid'])) {
                $attributes[$inputData['inputName']] = $inputData['label'];
            }
        }

        return $attributes;
    }
}
