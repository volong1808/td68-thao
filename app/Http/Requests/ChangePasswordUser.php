<?php

namespace App\Http\Requests;

use Dotenv\Exception\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class ChangePasswordUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:6',
            'password_new' => 'required|min:6',
            'password_confirmation' => 'required|same:password_new|min:6',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'password' => 'Mật Khẩu',
            'password' => 'Mật Khẩu Mới',
            'password_confirmation' => 'Mật Khẩu Nhập Lại'
        ];
    }

    /**
     * @return array
     */
    public function  messages()
    {
        return [
            'regex' => ':attribute không hợp lệ',
            'same' => 'Mật Khẩu Không Khớp.'
        ];
    }
}
