<?php

namespace App\Http\Requests;

use App\Services\OrderService;
use Illuminate\Foundation\Http\FormRequest;

class HandleOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $orderService = new OrderService();
        return $orderService->getRulesValid();
    }

    public function attributes()
    {
        $orderService = new OrderService();
        return $orderService->getAttributes();
    }
}
