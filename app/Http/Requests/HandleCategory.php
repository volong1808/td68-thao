<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'max:500',
            'image' => 'image|mimes:jpeg,png,jpg|max:2000',
            'slug' => 'required|max:255|regex:/^[a-zA-Z0-9-_]+$/i',
            'url' => 'url|max:255|nullable',
        ];
    }
}
