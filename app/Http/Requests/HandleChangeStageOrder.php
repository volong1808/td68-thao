<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleChangeStageOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $stageOrder = config('constants.ORDER_STATUS_LABEL');
        $stageKey = array_keys($stageOrder);
        $stageKey = implode(',', $stageKey);
        return [
            'code' => 'required',
            'status' => 'required|in:' . $stageKey,
        ];
    }
}
