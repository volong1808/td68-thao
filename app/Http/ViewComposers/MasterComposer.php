<?php
namespace App\Http\ViewComposers;

use App\Services\CategoryService;
use App\Services\CustomPostService;
use App\Services\PostService;
use Illuminate\View\View;
use App\Services\OptionService;

class MasterComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $option = new OptionService();
        $category = new CategoryService();
        $postService = new PostService();
        $customPostSevice = new CustomPostService('page');
        $metaDataColumn = $customPostSevice->getColumnOfMetaData();
        $conditions = [
            'show_menu' => 1
        ];
        $pages = $postService->getListWithConditionInMetaData('page', $conditions, $metaDataColumn);
        $pagesMenu = [];
        foreach ($pages as $page) {
            if (!empty($page->link_post_type)) {
                $page->category = $category->getCateByPostType($page->link_post_type);
                $pagesMenu[] = $page;
            }
        }

        $companyKeyOption = config('constants.company_config.key');
        $pluginKeyOption = config('constants.plugin_config.key');
        $company = $option->getContentOptionByKey($companyKeyOption);
        $plugin = $option->getContentOptionByKey($pluginKeyOption);
        $view->with('company', $company)->with('plugin', $plugin)->with('pageMenu', $pagesMenu);
    }
}
