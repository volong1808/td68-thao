<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordUser;
use App\Http\Requests\HandleUser;
use App\Services\UserService;
use App\Http\Requests\LoginUser;
use App\Mail\ResetPassworUserMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Http\Requests\RegisterUser;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ResetPasswordUser;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function register()
    {
        $title = 'Đăng Ký - doanvanhay.com';
        $data = [
            'flags' => true,
            'pagelogin' => true,
            'title' => $title
        ];
        return view('user.register', $data);
    }

    public function doRegister(RegisterUser $request)
    {
        $dataUser = $request->all();
        $this->userService->add($dataUser);
        return redirect()->route('login_user')
            ->with('successRegister', 'Tạo tài khoản thành công vui lòng đăng nhập');
    }

    public function login()
    {
//        $previousUrl =  URL::previous();
        $title = 'Đăng Nhập - doanvanhay.com';
        $data = [
            'flags' => true,
            'pagelogin' => true,
            'title' => $title
        ];

        return view('user.index', $data);
    }

    public function doLogin(LoginUser $request)
    {
//        $roleUser = config('constants.role.USER');
        $status = config('constants.USER_STATUS.ACTIVE');
        $data = [
            'password' => $request->password,
            'status' => $status,
//            'role' => $roleUser
        ];
        if(filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            $data['email'] = $request->username;
        } else {
            $data['username'] = $request->username;
        }

        $loginStage = Auth::attempt($data, $request->remember);
        if ($loginStage) {
            return redirect()->route('home');
        }
        return redirect()->back()
            ->with('errorLogin', 'Đăng nhập không thành công vui lòng kiểm tra lại.');
    }

    public function reset()
    {
        $title = 'Lấy lại mật khẩu - doanvanhay.com';
        $data = [
            'flags' => true,
            'pagelogin' => true,
            'title' => $title
        ];
        return view('user.reset', $data);
    }

    public function doReset(ResetPasswordUser $request)
    {
        $newPassword = generateRandomString();
        $user = $this->userService->getByEmail($request->email);
        if ($user != null) {
            $updateStage = $this->userService->updatePassword($user->username, $newPassword);
            if ($updateStage == false) {
                redirect()->route('login_user')->with('errorLogin', 'Đổi mật khẩu không thành công.');
            }

            $data = [
                'email' => $user->email,
                'name' => $user->name,
                'newPassword' => $newPassword,
            ];
            $emailReset = new ResetPassworUserMail($data);
            Mail::to($request->email)->send($emailReset);
            return redirect()->route('login_user')->with('resetSuccess', 'Khôi phục mật khẩu thành công! vui lòng đăng nhập lại.');
        }
        return redirect()->back()
            ->with('errorEmail', 'Đổi mật khẩu không thành công. Vui lòng kiểm tra lại Email!');
    }

    public function profile()
    {
        $user = Auth::user();
        $title = 'Thông Tin Tài Khoản - ' . $user->name;
        $data = [
            'flags' => true,
            'pagelogin' => true,
            'user' => $user,
            'title' => $title
        ];
        return view('user.profile', $data);
    }

    public function doUpdate(HandleUser $request)
    {
        $username = Auth::user()->username;
        if ($this->userService->updateProfile($username, $request)) {
            return redirect()->back()->with('updateSuccess', 'Cập nhập thông tin thành công');
        }

        return redirect()->back()->with('updateErrors', 'Cập nhập thông tin không thành công');
    }

    public function changePassword()
    {
        $title = 'Đổi Mật Khẩu';
        $data = [
            'flags' => true,
            'pagelogin' => true,
            'title' => $title
        ];
        return view('user.change_password', $data);
    }

    public function doChangePassword(ChangePasswordUser $request)
    {
        $passwordUser = Auth::user()->getAuthPassword();
        $username = Auth::user()->username;
        $verifyStatus = password_verify($request->password, $passwordUser);
        if ($verifyStatus) {
            $updateStage = $this->userService->updatePassword($username, $request->password_new);
            if ($updateStage) {
                return redirect()->back()->with('changePasswordSuccess', 'Đổi mật khẩu thành công.');
            } else {
                return redirect()->back()->with('changePasswordErrors', 'Đổi mật khẩu không thành công.');
            }
        }
        return redirect()->back()->with('changePasswordErrors', 'Mật khẩu không đúng.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

}
