<?php

namespace App\Http\Controllers\Admin;

use App\Imports\ProductImport;
use App\Services\FileService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ImportExportController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'postType' => $request->post_type
        ];
        return view('admin.import.upload', $data);
    }

    public function import(Request $request)
    {
        $importProduct = new ProductImport();
        $importProduct->setPostType($request->post_type);

        if ($request->file('file_image')) {
            $fileService = new FileService();
            $pathDir = getUploadDirectoryPath();
            $importProduct->setImportImage($pathDir);
            $fileService->unZipper($request->file('file_image')->getPathName(), $pathDir);
        }

        Excel::import($importProduct,$request->file('file'));
        return redirect()->back()
            ->withSuccess('Bạn vừa thực hiện import thành công');
    }
}