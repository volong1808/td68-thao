<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HandleUser;
use App\SocialAccount;
use App\User;
use App\Helpers\UserHelper;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
class UserController extends AdminController
{
    use UserHelper;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::with('plan')->where('role', config('constants.role.USER'))->orderBy('updated_at', 'DESC')
            ->paginate(config('constants.PER_PAGE'));
        $data = [
            'title' => 'Trang Quản Lý Người Dùng',
            'users' => $users
        ];
        return view('admin.user.list', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myProfile()
    {
        $data['title'] = 'My Profile';
        $data['initValue'] = $this->getInitUserData(Auth::user());
        return view('admin.user.profile', $data);
    }

    public function updateProfile(HandleUser $request)
    {
        $this->saveUserData($request, Auth::user());
        return redirect(route('my_profile'))->withSuccess('Bạn vừa cập nhật thành công tài khoản');
    }

    public function edit($userId){
        $user = User::findOrFail($userId);
        $data['user'] = $user;
        $data['initValue'] = $this->getUserInputData($user);
        return view('admin.user.edit', $data);
    }

    /**
     * @param HandleUser $request
     * @param $id
     * @return mixed
     */
    public function handleEdit(HandleUser $request, $id)
    {
        $user = User::findOrFail($id);
        $userId = $this->saveUserData($request, $user);
        return redirect(route('user_edit', ['id' => $userId]))->withSuccess('Bạn vừa cập nhật thành công');
    }

    public function create()
    {
        $data['initValue'] = $this->getUserInputData();
        return view('admin.user.create', $data);
    }

    public function handleCreate(HandleUser $request)
    {
        $user = new User();
        $this->saveUserData($request, $user, true);
        return redirect(route('user_create'))->withSuccess('Bạn vừa Thêm mới thành công');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request){
        $itemId = $request->input('itemId');
        $user = User::find($itemId);
        if(!empty($user)){
            $this->deleteImage(Image::find($user->avatar));
//            $this->deleteSocialByUser($itemId);
            $user->delete();
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request){
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $user = User::find($itemId);
                    if (isset($user)) {
                        $this->deleteImage(Image::find($user->avatar));
                        $user->delete();
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    private function getUserInputData($user = []){
        $initValue = [];
        $initValue['id'] = empty($user->id) ? '' : $user->id;
        $initValue['username'] = empty($user->username) ? '' : $user->username;
        $initValue['email'] = empty($user->email) ? '' : $user->email;
        $initValue['password'] = "";
        $initValue['password_confirmation'] = "";
        $initValue['user_password'] = "";
        $initValue['user_password_confirmation'] = "";
        $initValue['role'] = empty($user->role) ? '' : $user->role;
        $initValue['phone'] = empty($user->phone) ? '' : $user->phone;
        $initValue['name'] = empty($user->name) ? '' : $user->name;
        $initValue['avatar'] = empty($user->avatar) ? '' : Image::find($user->avatar);
        $initValue['status'] = empty($user->status) ? '' : $user->status;

        return $initValue;
    }

    /*Delete Social Account by User */
    protected function deleteSocialByUser($userId)
    {
        $socials = SocialAccount::where('user_id', $userId)->get();
        if (!empty($socials)) {
            foreach ($socials as $social) {
                if (isset($social)) {
                    $social->delete();
                }
            }
        }
    }

}
