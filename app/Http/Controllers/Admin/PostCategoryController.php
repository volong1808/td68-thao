<?php

namespace App\Http\Controllers\Admin;

use App\Category as Category;
use App\CategoryPost;
use App\Helpers\CategoryHelper;
use App\Http\Requests\HandleCategory;
use App\Services\CategoryService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class PostCategoryController
 * @package App\Http\Controllers\Admin
 */
class PostCategoryController extends AdminPostController
{
    use CategoryHelper;
    public $categoryService;

    /**
     * PostCategoryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->categoryService = new CategoryService();
        $this->data['pageSlug'] = array_get($this->postConfig, 'pageSlug', '') . '/danh-muc';
    }

    /**
     * @param $postType
     * @return Renderable|Factory|View
     */
    public function index()
    {
        $this->data['categories'] = $this->categoryService->getList($this->postType, config('constants.PER_PAGE'));
        $this->data['title'] = "Trang Quản Lý Danh Mục {$this->postTitle}";
        return view('admin.post_category.list',  $this->data);
    }

    /**
     * Function help to add new category
     *
     */
    public function add()
    {
        $this->data['title'] = "Trang Thêm Mới Danh Mục {$this->postTitle}";
        $this->data['initValue'] = $this->getCategoryInputData($this->postType);
        $this->data['parentCategories'] = $this->getCategoryOption($this->postType);
        return view('admin.post_category.add', $this->data);
    }

    /**
     * Function help to handle add new post
     *
     */
    public function handleAdd(HandleCategory $request)
    {
        $postId = $this->saveCategory($this->postType, $request);
        return redirect(route($this->postType . '_category_edit', ['postType' => $this->postType ,'id' => $postId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới danh mục.');
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($type, $id)
    {
        $category = Category::findOrFail($id);
        $this->data['title'] = "Trang Cập Nhật Thông Tin Danh Mục {$this->postType}";
        $this->data['initValue'] = $this->getCategoryInputData($this->postType, $category);
        $this->data['category'] = $category;
        $this->data['parentCategories'] = $this->getCategoryOption($this->postType);
        return view('admin.post_category.edit', $this->data);
    }

    /**
     * @param HandleCategory $request
     * @param $id
     * @return mixed
     */
    public function handleEdit(HandleCategory $request, $type, $id)
    {
        $category = Category::findOrFail($id);
        $categoryId = $this->saveCategory($this->postType, $request, $category);
        return redirect(route($this->postType . '_category_edit', ['postType' => $this->postType,'id' => $categoryId]))
            ->withSuccess('Bạn vừa cập nhật thành công danh mục');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $category = Category::find($itemId);
        if (isset($category)) {
            $this->deleteCategory($category);
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công danh mục.');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $category = Category::find($itemId);
                    if (isset($category)) {
                        $this->deleteCategory($category);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công danh mục.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }
}
