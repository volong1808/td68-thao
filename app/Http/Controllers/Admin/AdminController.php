<?php

namespace App\Http\Controllers\Admin;

use App\Image as Image;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        if (!Auth::check()) {
            Redirect::route('login');
        }
        return view('admin.index');
    }

    /**
     * @param $image
     */
    public function deleteImage($image)
    {
        if (isset($image)) {
            $imagePath = base_path() . DIRECTORY_SEPARATOR . $image->url;
            $originPath = base_path() . DIRECTORY_SEPARATOR . $image->origin;
            $thumbPath = base_path() . DIRECTORY_SEPARATOR . $image->thumb;
            if (file_exists($imagePath) AND !is_dir($imagePath)) {
                unlink($imagePath);
            }
            if (file_exists($originPath) AND !is_dir($originPath)) {
                unlink($originPath);
            }
            if (file_exists($thumbPath) AND !is_dir($thumbPath)) {
                unlink($thumbPath);
            }
            Image::destroy($image->id);
        }
        return;
    }
}
