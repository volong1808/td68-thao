<?php

namespace App\Http\Controllers\Admin;

use App\OptionPost;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Services\OptionPostService;
use App\Http\Requests\HandleOptionPost;
use Illuminate\Support\Facades\Request as RouteRequest;
use Illuminate\View\View;
use Throwable;

/**
 * Class PostCategoryController
 * @package App\Http\Controllers\Admin
 */
class OptionPostController extends AdminPostController
{
    public $service;
    public $type;

    /**
     * PostCategoryController constructor.
     */
    public function __construct()
    {
        $this->loadModal(new OptionPost);
        $this->type = RouteRequest::route()->parameter('type');
        parent::__construct();
        $this->service = new OptionPostService($this->postType, $this->type);
        $config = $this->customPostService->getOption($this->type);
        $this->service->loadConfig($config);
        $this->data['type'] = $this->type;

    }

    /**
     * @param $postType
     * @return Renderable|Factory|View
     */
    public function index()
    {
        $this->data['options'] = $this->service->getList(config('constants.PER_PAGE'));
        $optionName = $this->postConfig['option'][$this->type]['name'];
        $this->data['title'] = "Trang Quản Lý Tùy Chọn {$optionName} {$this->postTitle}";
        $this->data['optionConfig'] = $this->postConfig['option'][$this->type];
        return view('admin.option_post.list',  $this->data);
    }

    /**
     * Function help to add new category
     *
     */
    public function add()
    {
        $optionName = $this->postConfig['option'][$this->type]['name'];
        $this->data['title'] = "Trang Thêm Mới Tùy Chọn {$optionName} {$this->postTitle}";
        $this->data['initValue'] = $this->service->getInputData();
        $this->data['optionConfig'] = $this->service->getConfig();
        $this->data['mappingOption'] = $this->service->getConfigInputMappingByType();
        return view('admin.option_post.add', $this->data);
    }

    /**
     * Function help to handle add new post
     *
     */
    public function handleAdd(HandleOptionPost $request)
    {
        $id = $this->service->save($request);
        if (!empty($request->mapping)) {
            $this->service->saveOptionPostOption($request, $id);
        }
        return redirect(route($this->postType . '_option_edit', ['postType' => $this->postType, 'type' => $this->type ,'id' => $id]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới tùy chọn.');
    }

    public function handleAddAjax(HandleOptionPost $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $id = $this->service->save($request);
        if (!empty($request->mapping)) {
            $this->service->saveOptionPostOption($request, $id);
        }
        $option = $this->service->getDetail($id);
        $configOptionContent = $this->service->getConfigContent();
        $dataView = [
            'option' => $option,
            'tab' => $this->type,
            'postType' => $this->postType,
            'configOptionContent' => $configOptionContent
        ];
        $row = view('admin.option_post.option_row', $dataView)->render();
        $data = [
            'row' => $row,
            'option' => $option,
        ];
        $dataResponsive = $this->getResponsiveAjaxSuccess($data);
        return response()->json($dataResponsive);
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($type, $OptionType, $id)
    {
        $option = $this->service->getDetail($id);
        $optionName = $this->postConfig['option'][$this->type]['name'];
        $this->data['id'] = $id;
        $this->data['title'] = "Trang Cập Nhật Thông Tin Tùy Chọn {$optionName} {$this->postTitle}";
        $this->data['initValue'] = $this->service->getInputData($option);
        $this->data['optionConfig'] = $this->service->getConfig();
        $this->data['mappingOption'] = $this->service->getConfigInputMappingByType();
        $this->data['mappingSelect'] = $this->service->getMappingSelect($id, false);
        return view('admin.option_post.edit', $this->data);
    }

    /**
     * @param $type
     * @param $OptionType
     * @param $id
     * @return JsonResponse
     * @throws Throwable
     */
    public function editAjax($type, $OptionType, $id)
    {
        $option = $this->service->getDetail($id);
        $this->data['id'] = $id;
        $this->data['initValue'] = $this->service->getInputData($option);
        $this->data['optionConfig'] = $this->service->getConfig();
        $this->data['tab'] = $this->type;
        $this->data['option'] = $option;
        $this->data['mappingOption'] = $this->service->getConfigInputMappingByType();
        $this->data['mappingSelect'] = $this->service->getMappingSelect($id);
        $this->data['configOptionContent'] = $this->service->getConfigContent();
        $form = view('admin.option_post.option_form_body', $this->data)->render();
        $data = [
            'form' => $form,
            'option' => $option,
        ];
        $dataResponsive = $this->getResponsiveAjaxSuccess($data);
        return response()->json($dataResponsive);
    }

    /**
     * @param HandleOptionPost $request
     * @param $id
     * @return mixed
     */
    public function handleEditAjax(HandleOptionPost $request, $type, $OptionType, $id)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $this->service->update($id, $request);
        if (!empty($request->mapping)) {
            $this->service->updateOptionPostOption($request->mapping, $id);
        }
        $option = $this->service->getDetail($id);
        $configOptionContent = $this->service->getConfigContent();
        $dataView = [
            'option' => $option,
            'tab' => $this->type,
            'postType' => $this->postType,
            'configOptionContent' => $configOptionContent
        ];
        $row = view('admin.option_post.option_collum', $dataView)->render();
        $data = [
            'row' => $row,
            'option' => $option,
            'id' => $id
        ];
        $dataResponsive = $this->getResponsiveAjaxSuccess($data);
        return response()->json($dataResponsive);
    }

    /**
     * @param HandleOptionPost $request
     * @param $id
     * @return mixed
     */
    public function handleEdit(HandleOptionPost $request, $type, $OptionType, $id)
    {
        $this->service->update($id, $request);
        if (!empty($request->mapping)) {
            $this->service->updateOptionPostOption($request, $id);
        }
        return redirect(route($this->postType . '_option_edit', ['postType' => $this->postType, 'type' => $this->type ,'id' => $id]))
            ->withSuccess('Bạn vừa cập nhật thành công tùy chọn');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $optionPost = $this->service->getDetail($itemId);
        if (isset($optionPost)) {
            $this->service->delete($itemId);
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công tùy chọn.');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    public function deleteAjax(Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $itemId = $request->input('itemId');
        $optionPost = $this->service->getDetail($itemId);
        if (isset($optionPost)) {
            $this->service->delete($itemId);
        }
        $dataResponsive = $this->getResponsiveAjaxSuccess([]);
        return response()->json($dataResponsive);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $optionPost = $this->service->getDetail($itemId);
                    if (isset($optionPost)) {
                        $this->service->delete($itemId);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công tùy chọn.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }
}
