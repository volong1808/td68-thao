<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Helpers\GalleryHelper;
use App\Helpers\PostHelper;
use App\Post as Post;
use Illuminate\Http\Request;
use App\Image;
use Illuminate\Support\Facades\DB;

class GalleryController extends AdminController
{
    use GalleryHelper;

    /**
     * GalleryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
