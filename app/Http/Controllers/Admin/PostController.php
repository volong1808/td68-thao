<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryPost;
use App\Gallery;
use App\Http\Requests\HandleProduct;
use App\Image;
use App\OptionPost;
use App\Post as Post;
use App\Services\OptionPostService;
use App\Services\PostService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Helpers\PostHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

/**
 * Class ProductController
 * @package App\Http\Controllers\Admin
 */
class PostController extends AdminPostController
{
    public $seoable = true;
    public $postService;

    use PostHelper;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postService = new PostService();
        $this->data['pageSlug'] = array_get($this->postConfig, 'pageSlug', '');
    }

    /**
     * @return Renderable|Factory|View
     */
    public function index()
    {
        $this->data['posts'] = $this->postService->getList($this->postType, config('constants.PER_PAGE'));
        $this->data['title'] = "Trang Quản Lý {$this->postTitle}";
        return view('admin.post.list', $this->data);
    }

    /**
     * @return Factory|View
     */
    public function add()
    {
        $this->data['title'] = "Trang Thêm Mới {$this->postTitle}";
        $this->data['categories'] = Category::whereNull('parent_id')
            ->where('post_type', $this->postType)->get();
        $this->data['initValue'] = $this->getPostInputData($this->postType);
        if ($this->customPostService->hasOption()) {
            $optionService = new OptionPostService($this->postType);
            $optionService->loadConfig($this->postConfig['option']);
            $this->data['options'] = $optionService->getOptionByPostType();
            $this->data['configFormOptions'] = $optionService->getGroupConfigFormInputOption($this->postConfig['option']);
        }
        return view('admin.post.add', $this->data);
    }

    /**
     * @param HandleProduct $request
     * @return mixed
     */
    public function doAdd(HandleProduct $request)
    {
        $postId = $this->savePost($this->postType, $request);
        if (isset($request->imageList)) {
            foreach ($request->imageList as $item) {
                $gallery = Gallery::where('post_id', 0)->where('image_id', $item)->first();
                if (isset($gallery)) {
                    $gallery->post_id = $postId;
                    $gallery->save();
                }
            }
        }
        if (!empty($this->postConfig['option']) && $request->has('option_post')) {
            $optionService = new OptionPostService($this->postType, null);
            $postOptionPostContent = !empty($request->post_option_post_content) ? $request->post_option_post_content : '';
            $optionService->savePostToPostOption($request->option_post, $postId, $request->option_post_order, $postOptionPostContent);
        }
        return redirect(route($this->postType . '_edit', ['postType' => $this->postType ,'id' => $postId]))
            ->withSuccess("Bạn vừa thực hiện thành công thêm mới {$this->postTitle}.");
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($type, $id)
    {
        $post = Post::findOrFail($id);
        $data['title'] = "Trang Cập Nhật {$this->postTitle}";
        $data['categories'] = Category::whereNull('parent_id')
            ->where('post_type', $this->postType)->get();
        $data['initValue'] = $this->getPostInputData($this->postType, $post);
        if (!empty($this->postConfig['option'])) {
            $optionService = new OptionPostService($this->postType);
            $optionService->loadConfig($this->postConfig['option']);
            $this->data['options'] = $optionService->getOptionByPostType();
            $this->data['configFormOptions'] = $optionService->getGroupConfigFormInputOption($this->postConfig['option']);
            $this->data['optionsSelect'] = $optionService->getOptionOfPost($id, null);
        }
        $data['gallery'] = $this->getGalleryImages($id)->toArray();
        $data['post'] = $post;
        $this->pushData($data);
        return view('admin.post.edit', $this->data);
    }

    /**
     * @param HandleProduct $request
     * @param $id
     * @return mixed
     */
    public function doEdit(HandleProduct $request, $type, $id)
    {
        $post = Post::findOrFail($id);
        $postId = $this->savePost($this->postType, $request, $post);
        if (!empty($this->postConfig['option']) && $request->has('option_post')) {
            $optionService = new OptionPostService($this->postType, null);
            $optionService->deleteByPostId($id);
            $postOptionPostContent = !empty($request->post_option_post_content) ? $request->post_option_post_content : '';
            $optionService->savePostToPostOption($request->option_post, $id, $request->option_post_order, $postOptionPostContent);
        }
        return redirect(route($this->postType . '_edit', ['postType' => $this->postType, 'id' => $postId]))
            ->withSuccess("Bạn vừa cập nhật thành công {$this->postTitle}");
    }
}
