<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Contact;
use DemeterChain\C;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    protected $deleteMessage;

    public function __construct()
    {
        $this->deleteMessage = 'Bạn vừa thực hiện xóa thành công gói sản phẩm.';
    }

    /**
     * Function index page admin/contacts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $contacts = Contact::orderBy('created_at', 'DESC')->paginate(config('constants.PER_PAGE'));
        $data['title'] = 'Trang Quản Lý Liên Hệ';
        $data['contacts'] = $contacts;
        return view('admin.contact.list', $data);
    }

    /**
     * Function delete contact $id
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        if ($this->canDeleteContact($itemId)) {
            $this->handleDelete($itemId);
            return redirect()->back()->withSuccess($this->deleteMessage);
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $contact = Contact::find($itemId);
                    if (isset($contact)) {
                        $this->handleDelete($itemId);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công sản phẩm.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        if (isset($contact)){
            return view('admin.contact.detail', compact('contact'));
        }
        abort(404);
        //return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $status = $request->input('status');
        $contact = Contact::find($id);
        if (isset($contact) && preg_match('/^[123]{1}/', $status)) {
            if ($contact->status != 1 && $status == 1) {
                return response()->json('Lỗi! Bạn không thể thay đổi trạng thái về "Mới"', 500);
            }
            $contact->status = $status;
            $contact->save();
            return response()->json('Trạng thái liên hệ đã được cập nhật', 200);
        }
        return response()->json([config('constants.message.errorMessage')], 500);
    }

    /**
     * @param $postId
     * @param $postType
     * @return bool
     */
    public function canDeleteContact($contactId)
    {
        if (is_array($contactId)) {
            $contacts = Contact::whereIn('id', $contactId)->get();
            if (count($contacts) == count($contactId)) {
                return true;
            }
            return false;
        }
        $contact = Contact::where('id', $contactId)->get();
        if (!empty($contact)) {
            return true;
        }
        return false;
    }

    public function handleDelete($contactId)
    {
        $contact = Contact::find($contactId);
        Contact::destroy($contact->id);
        return;
    }

}
