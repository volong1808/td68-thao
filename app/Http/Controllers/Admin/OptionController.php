<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\HandleOption;
use App\Services\OptionService;

class OptionController extends Controller
{
    public $option;

    public function __construct()
    {
        $this->option = new OptionService();
    }

    public function config($key)
    {
        $configForm = $this->option->getConfigOptionByKey($key);
        if (empty($configForm)) {
            abort(404);
        }
        $option = $this->option->getContentOptionByKey($key);
        if (empty($option)) {
            $this->option->setNew($key);
        }
        $initValue = $this->option->initValue($configForm, $option);
        $data = [
            'title' => config("constants.{$key}_config.title"),
            'configForm' => $configForm,
            'initValue' => $initValue,
            'optionKey' => $key
        ];

        return view('admin.option.config', $data);
    }

    public function configSave($key,HandleOption $request)
    {
        $configForm =  $this->option->getConfigOptionByKey($key);
        $oldOption =  $this->option->getContentOptionByKey($key);
        $data =  $this->option->getFormData($configForm, $request, $oldOption);
        $dataSave = $data['text'];
        if (!empty($data['file'])) {
            foreach ($data['file'] as $name => $file) {
                $fileUpload =  $this->option->uploadImage($file, $name);
                $dataSave[$name] = $fileUpload['url'];
                deleteFileDatabase($oldOption[$name]);
            }
        }

        $content = json_encode($dataSave);
        $this->option->update($key, $content);

        return redirect()->route('config_website', $key)->with('optionSaveSuccess', 'Lưu Thông Tin Thành Công.');
    }
}
