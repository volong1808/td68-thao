<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SettingPostService;

class SettingCustomPost extends Controller
{

    protected $service;

    public function __construct()
    {
        $this->service = new SettingPostService();
    }

    public function setting($posType) {
        $configPostType = $this->service->getConfig($posType);
        if (is_null($configPostType)) {
            return abort(404);
        }

        if (!empty($configPostType['meta_data'])) {
            $this->service->installMetaData($posType, $configPostType['meta_data']);
        }
    }
}
