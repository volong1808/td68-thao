<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Services\CustomPostService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as RouteRequest;
use App\Post as Post;
use App\Image as Image;

class AdminPostController extends AdminController
{
    public $metadataFields = [];
    public $postType;
    public $postConfig;
    public $postTitle;
    public $modal;
    public $customPostService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->modal = $this->loadModal(new Post);
        $this->postType = RouteRequest::route()->parameter('postType');
        $this->customPostService = new CustomPostService($this->postType);
        if (!empty($this->postType)) {
            $this->postConfig =  $this->customPostService->getConfigPost();
            $this->postTitle =  $this->customPostService->getTitle();
        }
        if (!empty($this->postConfig['meta_data'])) {
            $this->getMetaDataDefault($this->postConfig['meta_data']);
        }
        $data = [
            'postType' => $this->postType,
            'postTitle' => $this->postTitle,
            'postConfig' => $this->postConfig,
        ];
        $this->pushData($data);
    }

    public function loadModal($modal)
    {
        $this->modal = $modal;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        if (!Auth::check()) {
            Redirect::route('login');
        }
        return view('admin.index');
    }

    /**
     * @param $postId
     * @param $postType
     * @return bool
     */
    public function canDeletePost($postId, $postType = '')
    {
        if (is_array($postId)) {
            $posts = $this->modal->whereIn('id', $postId)->where('post_type', $postType)->get();
            if (count($posts) == count($postId)) {
                return true;
            }
            return false;
        }
        $post = $this->modal->where('id', $postId)->where('post_type', $postType)->get();
        if (!empty($post)) {
            return true;
        }
        return false;
    }

    /**
     * @param $image
     */
    public function deleteImage($image)
    {
        if (isset($image)) {
            $imagePath = base_path() . DIRECTORY_SEPARATOR . $image->url;
            $originPath = base_path() . DIRECTORY_SEPARATOR . $image->origin;
            $thumbPath = base_path() . DIRECTORY_SEPARATOR . $image->thumb;
            if (file_exists($imagePath) AND !is_dir($imagePath)) {
                unlink($imagePath);
            }
            if (file_exists($originPath) AND !is_dir($originPath)) {
                unlink($originPath);
            }
            if (file_exists($thumbPath) AND !is_dir($thumbPath)) {
                unlink($thumbPath);
            }
            Image::destroy($image->id);
        }
        return;
    }

    public function getMetaDataDefault($metaData)
    {
        foreach ($metaData as $data) {
            $this->metadataFields[$data['inputName']] = $data['valueDefault'];
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $post = Post::find($itemId);
        if (isset($post)) {
            $this->handleDelete($post);
            return redirect()->back()->withSuccess("Bạn vừa thực hiện xóa thành công {$this->postTitle}.");
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param $post
     */
    public function handleDelete($post)
    {
        $this->deleteImage($post->image);
        $gallery = $this->getGalleryImages($post->id);
        if (!empty($gallery)) {
            foreach ($gallery as $item) {
                $image = Image::find($item->id);
                if (isset($image)) {
                    $this->deleteImage($image);
                    Gallery::where('post_id', $post->id)->where('image_id', $item->id)->delete();
                }
            }
        }
        if (!empty($this->metadataFields)) {
            DB::table('metadata_' . $this->postType)->where('post_id', $post->id)->delete();
        }

        $post->delete();
        return;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $post = Post::find($itemId);
                    if (isset($post)) {
                        $this->handleDelete($post);
                    }
                }
                return redirect()->back()->withSuccess("Bạn vừa thực hiện xóa thành công {$this->postTitle}.");
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }
}
