<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Order;
use DemeterChain\C;
use App\Mail\InvoiceMail;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\HandleChangeStageOrder;
use Illuminate\View\View;

class OrderController extends Controller
{
    protected $deleteMessage;

    public function __construct()
    {
        $this->deleteMessage = 'Bạn vừa thực hiện xóa thành công đơn hàng!.';
    }

    /**
     * Function index page admin/contacts
     *
     * @return Factory|View
     */
    public function index()
    {
        $orders = Order::orderBy('created_at', 'DESC')
            ->paginate(config('constants.PER_PAGE'));
        $data['title'] = 'Trang Quản Lý Đơn Hàng';
        $data['orders'] = $orders;
        return view('admin.order.list', $data);
    }

    /**
     * Function delete contact $id
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        if ($this->canDeleteContact($itemId)) {
            $this->handleDelete($itemId);
            return redirect()->back()->withSuccess($this->deleteMessage);
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $contact = Order::find($itemId);
                    if (isset($contact)) {
                        $this->handleDelete($itemId);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công sản phẩm.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    public function changeStage(HandleChangeStageOrder $request)
    {
        $code = $request->code;
        $status = $request->status;
        $orderService = new App\Services\OrderService();
        $order = $orderService->getByCode($code);
        $order->status = $status;
        $order->update();
        if ($status ==  config('constants.ORDER_STATUS.PAID')) {
            $planId = $order->plan_id;
            $orderCode = $order->code;
            $username = $order->username;
            $planService = new App\Services\PlanService();
            $productService = new App\Services\ProductService();
            $fbacc = new App\Services\FbaccService();
            $optionService = new App\Services\OptionService();
            $planDetail = $planService->getPlanById($planId);
            if (empty($planDetail)) {
                return redirect()->back()
                    ->with('orderErrors', 'Gói không tồn tại vui lòng kiểm tra lại,');
            }
            $iDay = $planDetail->time * 365;
            $fbacc->createAccount($username, $iDay);
            $companyKeyOption = config('constants.company_config.key');
            $emailTo[] = $optionService->getItemByKey($companyKeyOption, 'email');
            if (!empty($orderDetail['email'])) {
                $emailTo[] = $orderDetail['email'];
            }
            $productDetail = $productService->getById($planDetail->product_id);
            $bankingOptionKey = config('constants.banking_config.key');
            $bankingConfig = $optionService->getConfigOptionByKey($bankingOptionKey);
            $data = [
                'productDetail' => $productDetail,
                'planDetail' => $planDetail,
                'bankingConfig' => $bankingConfig,
                'order' => $order,
            ];
            $mailVoid = new InvoiceMail($data);
            Mail::to($emailTo)->send($mailVoid);
        }
        return redirect()->back()
            ->with('success', 'Bạn đã đổi trạng thái đơn hàng thành công.');
    }

    /**
     * @param $postId
     * @param $postType
     * @return bool
     */
    public function canDeleteContact($contactId)
    {
        if (is_array($contactId)) {
            $contacts = Order::whereIn('id', $contactId)->get();
            if (count($contacts) == count($contactId)) {
                return true;
            }
            return false;
        }
        $contact = Order::where('id', $contactId)->get();
        if (!empty($contact)) {
            return true;
        }
        return false;
    }

    public function handleDelete($contactId)
    {
        $contact = Order::find($contactId);
        Order::destroy($contact->id);
        return;
    }

}
