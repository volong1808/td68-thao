<?php

namespace App\Http\Controllers;

use App\Helpers\OrderHelper;
use App\Helpers\PostHelper;
use App\Services\CategoryService;
use App\Services\OptionPostService;
use App\Services\OrderService;
use Illuminate\Http\Request;

class PostController extends FrontEndController
{

    use PostHelper;

    public function __construct()
    {
        parent::__construct();
        $this->data['companyInfo'] = getOptionByKey('company');
    }

    use OrderHelper;

    public function page()
    {
        if ($this->page->link_post_type == config('constants.static_page')) {
            return view('page.single_page', $this->data);
        }
        $postType = $this->page->link_post_type;
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $this->customPostService->setPostType($postType);
        $listPost = $this->postService->getList($postType, $itemInPage, $this->statePublish);

        $data = [
            'posts' => $listPost,
            'title' => $this->page->name,
            'title_head' => $this->page->name
        ];

        $this->pushData($data);
        $viewPath = "post.{$postType}.list";
        return view($viewPath, $this->data);

    }

    public function listPostByCategory()
    {
        $categoryService = new CategoryService();
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $cateSlug = $this->getParameterFromRoute('cate_slug');
        $cateDetail = $categoryService->getBySlug($cateSlug);
        if (empty($cateDetail)) {
            return abort(404);
        }

        $parentOfItem = [];
        $listPost = $this->postService->getByCateId($cateDetail->id, $cateDetail->post_type, $this->statePublish,
            $itemInPage);
        $categoryService->getParentOfChild($cateDetail, $parentOfItem, $this->statePublish);
        $breadcrumb = [
            'categoryOfPost' => $parentOfItem,
            'active' => $cateDetail->name
        ];

        $data = [
            'parentOfItem' => $parentOfItem,
            'cateDetail' => $cateDetail,
            'posts' => $listPost,
            'title' => $cateDetail->name,
            'breadcrumb' => $breadcrumb,
            'cateSlug' => $cateSlug
        ];
        $this->pushData($data);
        $viewPath = "post.{$cateDetail->post_type}.list";
        return view($viewPath, $this->data);
    }

    public function detail()
    {
        $options = [];
        $parentOfItem = [];
        $optionConfig = [];
        $postType = $this->page->link_post_type;
        $categoryService = new CategoryService();
        $optionPostService = new OptionPostService($postType);
        $postSlug = $this->getParameterFromRoute('post_slug');
        $this->customPostService->setPostType($postType);
        $postDetail = $this->postService->getDetailPost(
            $this->customPostService,
            $postType,
            $postSlug,
            $this->statePublish
        );

        if (empty($postDetail)) {
            abort(404);
        }

        if ($this->customPostService->hasOption()) {
            $optionConfig = $this->customPostService->getOptionConfig();
            $optionPostService->loadConfig($optionConfig);
            $options = $optionPostService->getOptionOfPost($postDetail->id, $this->statePublish);
        }

        $catOfPost = $categoryService->getCateByPostId($postDetail->id, $postType, $this->statePublish);

        $categoryService->getParentOfChild($catOfPost, $parentOfItem, $this->statePublish);

        $breadcrumb = [
            'categoryOfPost' => $parentOfItem,
            'active' => $postDetail->name
        ];

        $listPost = null;
        if (isset($catOfPost)) {
            $listPost = $this->postService->getOtherPostByCateId($catOfPost->id, $postDetail->id, $catOfPost->post_type,
                $this->statePublish, 8);
        }

        $otherCustomPost = $this->postService->getListOtherCustomPost($postType, $postDetail->id, 10, $this->statePublish);

        $detailProduct = getOptionByKey('detail_product');

        $gallery = $this->getGalleryImages($postDetail->id);

        $data = [
            'postDetail' => $postDetail,
            'options' => $options,
            'title' => $postDetail->name,
            'breadcrumb' => $breadcrumb,
            'optionConfig' => $optionConfig,
            'posts' => $listPost,
            'catePost' => $catOfPost,
            'otherCustomPost' => $otherCustomPost,
            'detailProduct' => $detailProduct,
            'gallery' => $gallery
        ];

        $this->pushData($data);
        $viewPath = "post.{$postType}.detail";
        return view($viewPath, $this->data);
    }

    public function searchPost(Request $request) {
        if (!empty($request->input('key'))) {
            $keyword = $request->input('key');

            $itemInPage = config('constants.NEWS_PAGE_ITEM');

            $posts = $this->postService->getListSearch(
                [['name', 'LIKE', '%' . $keyword . '%']],
                ['product'],
                $itemInPage
            );

            $data = [
                'posts' => $posts,
                'title' => 'Tìm kiếm',
                'keyword' => $keyword
            ];

            $this->pushData($data);
            $viewPath = "post.search";
            return view($viewPath, $this->data);
        }

        return redirect()->route('home');
    }

    public function ajaxPaginateDetail(Request $request)
    {
        if($request->ajax())
        {
            $categoryService = new CategoryService();
            $postId = $request->input('postId');
            $postType = $request->input('postType');
            $pageSlug = $request->input('pageSlug');
            $catOfPost = $categoryService->getCateByPostId($postId, $postType, $this->statePublish);
            $data = $this->postService->getOtherPostByCateId($catOfPost->id, $postId, $catOfPost->post_type,
                $this->statePublish, 8);
            return view('post.pagination.view_pagination_detail', ['data' => $data, 'postId' => $postId, 'postType' => $postType, 'pageSlug' => $pageSlug])->render();
        }
    }

    public function ajaxPaginateDetailCustomPost(Request $request)
    {
        if($request->ajax())
        {
            $postId = $request->input('postId');
            $postType = $request->input('postType');
            $pageSlug = $request->input('pageSlug');
            $data = $this->postService->getListOtherCustomPost($postType, $postId, 10, $this->statePublish);
            return view('post.pagination.view_pagination_detail_custom_post', ['data' => $data, 'postId' => $postId, 'postType' => $postType, 'pageSlug' => $pageSlug ])->render();
        }
    }
}
