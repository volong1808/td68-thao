<?php

namespace App\Http\Controllers;

use App\Services\PageService;
use Illuminate\Support\Facades\Request;

class AboutController extends Controller
{
    public function index()
    {
        $pageService = new PageService();
        $slug = Request::segment(1);
        $page = $pageService->getBySlug($slug);
        if (empty($page)) {
            abort(404);
        }
        $bannerImg = 'uploads/pages/faq_banner.jpg';
        if (!empty($pageService->image)) {
            $bannerImg = $pageService->image->origin;
        }
        $banner = [
            'title' => $page->name,
            'bannerImage' => $bannerImg,
        ];
        $data = [
            'banner' => $banner,
            'page' => $page,
        ];
        return view('pages.about', $data);
    }
}
