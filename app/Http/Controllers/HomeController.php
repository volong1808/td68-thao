<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\SeoHelper;
use App\Http\Controllers\FrontEndController;
use App\Post;
use App\Services\CategoryService;
use App\Services\PostService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends FrontEndController
{
    protected $service;
    protected $serviceCategory;
    public $statePublish;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->service = new PostService();
        $this->serviceCategory = new CategoryService();
        $this->statePublish = config('constants.POST_STATE.PUBLISHED');
    }

    use SeoHelper;

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {

        $slides = $this->service->getByType('slide', null, $this->statePublish);
        $data = [
            'slides' => $slides
        ];
        $products = $this->service->getList('product', 18, $this->statePublish);
        $data['products'] = $products;

        $accessories = [];
        if ($this->serviceCategory->getBySlug('phu-kien', $this->statePublish)) {
            $accessories = $this->serviceCategory->getBySlug('phu-kien', $this->statePublish)->posts()->paginate(12);
        }

        $data['accessories'] = $accessories;

        $oldProducts = [];
        if ($this->serviceCategory->getBySlug('may-cu', $this->statePublish)) {
            $oldProducts = $this->serviceCategory->getBySlug('may-cu', $this->statePublish)->posts()->paginate(12);
        }

        $data['oldProducts'] = $oldProducts;

        $tutorials = $this->service->getList('tutorial', 8, $this->statePublish);
        $data['tutorials'] = $tutorials;

        $news = $this->service->getListRandom('tutorial', 4);
        $data['news'] = $news;

        return view('home', $data);
    }

    public function ajaxPaginate(Request $request)
    {
        if($request->ajax())
        {
            $postType = $request->input('postType');
            $title = $request->input('title');
            $data = $data = $this->service->getList($postType, 8, $this->statePublish);

            if ($postType == 'tutorial') {
                return view('post.pagination.tutorial.view_pagination_home', ['data' => $data, 'postType' => $postType, 'title' => $title])->render();
            }

            return view('post.pagination.view_pagination_home', ['data' => $data, 'postType' => $postType, 'title' => $title])->render();
        }
    }
}
