<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Helpers\SeoHelper;
use App\Mail\ContactMail;
use App\Http\Requests\HandleContact;
use App\Post;
use App\Services\OptionService;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    use SeoHelper;

    public function index()
    {
        $banner = [
            'title' => 'Liên Hệ',
            'description' => 'Chúng tôi sẻ phản hồi sớm nhất cho bạn.',
            'bannerImage' => 'uploads/pages/contact_page.jpg',
        ];

        $seoData = null;

        $contact = Post::where('slug', 'lien-he')->first();
        if (!empty($contact)){
            $seoData = $this->getPostSeoData($contact->id);
        }

        $companyInfo = getOptionByKey('company');
        $data = [
            'banner' => $banner,
            'seoData' => $seoData,
            'companyInfo' => $companyInfo
        ];
        return view('contact', $data);
    }

    public function sentContact(HandleContact $request)
    {
        $data = $request->all();
        $option = new OptionService();
        $companyKeyOption = config('constants.COMPANY_CONFIG_KEY');
        $contact = new Contact();
        $contact->name = $data['name'];
        $contact->address = $data['address'];
        $contact->phone_number = $data['phone_number'];
        $contact->email = $data['email'];
        $contact->subject = $data['subject'];
        $contact->message = $data['message'];
        $contact->status = config('constants.CONTACT_STATUS.NEW');
        $contact->created_at = date('Y-m-d H:i:s', time());
        $contact->save();
        $emailContact = new ContactMail($data);
        $emailTo[] = config('constants.CONTACT_EMAIL_TO');
        $emailTo[] = $option->getItemByKey($companyKeyOption, 'email');
        Mail::to($emailTo)->send($emailContact);
        return redirect()->route('contact')->with('senContactSuccess', 'Gửi liên hệ thành công');
    }
}
