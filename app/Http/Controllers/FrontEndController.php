<?php

namespace App\Http\Controllers;

use App\Helpers\SeoHelper;
use App\Services\PostService;
use App\Services\CustomPostService;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request as RouteRequest;

class FrontEndController extends Controller
{
    use SeoHelper;

    public $page;
    public $pageSlug;
    public $postService;
    public $statePublish;
    public $routeRequest;
    public $customPostService;

    public function __construct()
    {
        $routeName = Route::currentRouteName();
        $this->postService = new PostService();
        $this->customPostService = new CustomPostService('page');
        $this->pageSlug = $this->getParameterFromRoute('page_slug');
        $this->statePublish = config('constants.POST_STATE.PUBLISHED');
        $this->page = $this->getPageCurent();
        $skipCheckPage = [
            'home',
            'fr_order',
            'fr_order_list',
            'fr_delete_order',
            'category_slug',
            'fr_post_detail',
            'fr_change_quantity_order_ajax',
            'ajax-pagination-detail',
            'ajax-pagination-detail-custom-post',
            'search_post',
            'checkout_cart'
        ];
        if (empty( $this->page) && !in_array($routeName, $skipCheckPage)) {
            return abort(404);
        }
        if (!empty($this->page)) {
            $this->data['seoData'] = $this->getPostSeoData($this->page->id);
            $this->data['page'] =  $this->page;
        }

    }

    public function getPageCurent()
    {
        $statePublish = config('constants.POST_STATE.PUBLISHED');
        $metaDataColumn = $this->customPostService->getColumnOfMetaData();
        return $this->postService->getDetailWithMetaDataSlug(
            $this->pageSlug,
            'page',
            $metaDataColumn,
            $statePublish
        );
    }

    public function getParameterFromRoute($paramName)
    {
        return RouteRequest::route()->parameter($paramName);
    }
}
