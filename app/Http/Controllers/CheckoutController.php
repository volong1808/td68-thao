<?php

namespace App\Http\Controllers;

use App\Mail\OrderMail;
use App\Mail\InvoiceMail;
use App\Services\BankingService;
use App\Services\OrderService;
use App\Services\ProductService;
use Illuminate\Support\Facades\Mail;
use App\Services\PlanService;
use App\Services\OptionService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\HandleCheckout;
use Illuminate\Http\Request;
use App\Services\FbaccService;

class CheckoutController extends Controller
{
    public function checkout($planId)
    {
        $planService = new PlanService();
        $productService = new ProductService();
        $plan = $planService->getPlanById($planId);
        if (empty($plan)) {
            abort(404);
        }

        $optionService = new OptionService();
        $productId = $plan->product_id;
        $productDetail = $productService->getById($productId);
        $planList = $planService->getPlanByProductId($productId);
        $bankingOptionKey = config('constants.banking_config.key');
        $bankingConfig = $optionService->getConfigOptionByKey($bankingOptionKey);
        $bankingOption = $optionService->getContentOptionByKey($bankingOptionKey);
        $userInfo = Auth::user();
        $banner = [
            'title' => 'Checkout',
            'bannerImage' => 'uploads/pages/faq_banner.jpg',
        ];

        $data = [
            'productDetail' => $productDetail,
            'planDetail' => $plan,
            'banner' => $banner,
            'planList' => $planList,
            'bankingConfig' => $bankingConfig,
            'bankingOption' => $bankingOption,
            'userInfo' => $userInfo,
        ];

        return view('checkout.checkout', $data);
    }

    public function doCheckout($planId, HandleCheckout $request)
    {
        $planService = new PlanService();
        $productService = new ProductService();
        $plan = $planService->getPlanById($planId);
        if (empty($plan)) {
            abort(404);
        }
        $orderService = new OrderService();
        $dataOrder = $request->except('_token');
        $dataOrder['plan_id'] = $planId;
        $productId = $plan->product_id;
        $productDetail = $productService->getById($productId);
        $dataOrder['status'] = config('constants.ORDER_STATUS_DEFAULT');
        $dataOrder['code'] = $orderService->getCode($planId, $productId);
        $dataOrder['total'] = $plan->price;
        $dataOrder['username'] = Auth::user()->username;
        $optionService = new OptionService();
        $bankingOptionKey = config('constants.banking_config.key');
        $bankingConfig = $optionService->getConfigOptionByKey($bankingOptionKey);
        $orderService->add($dataOrder);
        $data = [
            'productDetail' => $productDetail,
            'planDetail' => $plan,
            'bankingConfig' => $bankingConfig,
            'order' => $dataOrder,
        ];
        $companyKeyOption = config('constants.company_config.key');
        $emailTo[] = $optionService->getItemByKey($companyKeyOption, 'email');
        if (!empty($dataOrder['email'])) {
            $emailTo[] = $dataOrder['email'];
        }
        $mailOrder = new OrderMail($data);
        Mail::to($emailTo)->send($mailOrder);
        if ($request->banking_type == config('constants.PAYMENT_ONLINE_KEY')) {
            $bankingService  = new BankingService();
            $urlReturn = route('banking_online_return', $dataOrder['code']);
            $urlVnp = $bankingService->getUrlBanking($dataOrder, $urlReturn);
            return redirect($urlVnp);
        }
        return redirect()->route('checkout', $planId)->with('checkoutSuccess', 'Mua gói sự dụng thành công - Mã Đơn Hàng: [' . $dataOrder['code'] . ']');
    }

    public function bankingOnline($orderCode)
    {
        $orderService = new OrderService();
        $productService = new ProductService();
        $planService = new PlanService();
        $orderDetail = $orderService->getByCode($orderCode);
        if (empty($orderDetail)) {
            abort(404);
        }

        $banner = [
            'title' => "Thanh Toán Đơn Hàng",
            'bannerImage' => 'uploads/pages/faq_banner.jpg',
        ];
        $planDetail = $planService->getPlanById($orderDetail->plan_id);
        $productDetail = $productService->getById($planDetail->product_id);
        $data = [
            'productDetail' => $productDetail,
            'planDetail' => $planDetail,
            'order' => $orderDetail,
            'banner' => $banner,
        ];
        return view('checkout.payment', $data);
    }

    public function doBankingOnline($orderCode)
    {
        $orderService = new OrderService();
        $orderDetail = $orderService->getByCode($orderCode);
        if (empty($orderDetail)) {
            abort(404);
        }
        $bankingService  = new BankingService();
        $urlReturn = route('banking_online_return', $orderCode);
        $urlVnp = $bankingService->getUrlBanking($orderDetail, $urlReturn);
        return redirect($urlVnp);
    }

    public function paymentReturn($orderCode, Request $request)
    {
        $orderService = new OrderService();
        $bankingService  = new BankingService();
        $planService = new PlanService();
        $productService = new ProductService();
        $fbacc = new FbaccService();
        $optionService = new OptionService();

        $orderDetail = $orderService->getByCode($orderCode);
        if (empty($orderDetail)) {
            return redirect()->route('banking_online', $orderCode)
                ->with('paymentSuccess', 'Đơn hàng không tồn tại vui lòng kiểm tra lại.');
        }

        if ($orderDetail->transaction_no == $request->vnp_TransactionNo) {
            return redirect()->route('banking_online', $orderCode)
                ->with('paymentError', 'Đơn hàng đã thanh toán.');
        }

        $stateReturn = $bankingService->getStageReturn($orderDetail, $request->all());
        if ($stateReturn['RspCode'] == config('constants.PAYMENT_SUCCESS_CODE')) {
            $planDetail = $planService->getPlanById($orderDetail->plan_id);
            if (empty($planDetail)) {
                return redirect()->route('banking_online', $orderCode)
                    ->with('paymentError', 'Gói không tồn tại vui lòng kiểm tra lại,');
            }
            $username = Auth::user()->username;
            $iDay = $planDetail->time * 365;
            $dataUpdate = [
                'status' => $stateReturn['status'],
                'transaction_no' => $request->vnp_TransactionNo,

            ];
            $orderService->updateStatusPaidByCode($orderCode, $dataUpdate);
            $fbacc->createAccount($username, $iDay);
            $bankingOptionKey = config('constants.banking_config.key');
            $bankingConfig = $optionService->getConfigOptionByKey($bankingOptionKey);
            $productDetail = $productService->getById($planDetail->product_id);
            $data = [
                'productDetail' => $productDetail,
                'planDetail' => $planDetail,
                'bankingConfig' => $bankingConfig,
                'order' => $orderDetail,
            ];
            $companyKeyOption = config('constants.company_config.key');
            $emailTo[] = $optionService->getItemByKey($companyKeyOption, 'email');
            if (!empty($orderDetail['email'])) {
                $emailTo[] = $orderDetail['email'];
            }
            $mailVoid = new InvoiceMail($data);
            Mail::to($emailTo)->send($mailVoid);
            return redirect()->route('banking_online', $orderCode)
                ->with('paymentSuccess', 'Bạn đã thành toán thành công.');
        }
        return redirect()->route('banking_online', $orderCode)
            ->with('paymentError', 'Thanh toán không thành công! Vui lòng thử lại.');
    }
}
