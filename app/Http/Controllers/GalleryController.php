<?php

namespace App\Http\Controllers;

use App\Helpers\GalleryHelper;
use App\Helpers\PostHelper;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    use GalleryHelper;
}
