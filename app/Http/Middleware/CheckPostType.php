<?php

namespace App\Http\Middleware;

use Closure;

class CheckPostType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $postType = $request->route()->parameter('postType');
        if (is_null(config('custom_post.' . $postType))) {
            return abort(404);
        }

        return $next($request);
    }
}
