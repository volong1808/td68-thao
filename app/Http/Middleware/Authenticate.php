<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        $routeLogin = [
            'product_download',
            'checkout'
        ];
        $currentRouteName = Route::currentRouteName();
        if (! $request->expectsJson()) {
            if (in_array($currentRouteName, $routeLogin)) {
                Session::flash('errorLogin', 'Vui lòng đăng nhập để được thực thiện thao tác tiếp theo.');
                return route('front_en_login');
            }
            if ($currentRouteName == 'ping') {
                exit("0");
            }
            return route('login');
        }
    }
}
