<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\OptionService;
use Illuminate\Http\Request;

class CheckStageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $option = new OptionService();
        $companyKeyOption = config('constants.company_config.key');
        $stage = $option->getItemByKey($companyKeyOption, 'stage');
        if (empty($stage)) {
            abort(307);
        }
        if ($stage != config('constants.PUBLIC_STATE_WEBSITE')) {
            abort($stage);
        }
        return $next($request);
    }
}
