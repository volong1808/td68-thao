<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostOptionPost extends Model
{
    protected $table = 'post_option_post';
    public $timestamps = true;

    public function option()
    {
        return $this->belongsTo(OptionPost::class, 'option_post_id', 'id');
    }
}
