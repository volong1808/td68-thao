<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }
}
