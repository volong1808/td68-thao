<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserPlan
 * @package App
 */
class UserPlan extends Model
{
    protected $table = 'user_plan';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany(User::class, 'users', 'user_id');
    }
}
