<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionPost extends Model
{
    protected $table = 'option_post';
    public $timestamps = true;
}
