jssor_slider_gallery = function (containerId) {
	var _SlideshowTransitions = [
		{$Duration:1000,$Delay:200,$Opacity:2}
	];
	var options = {
		$AutoPlay: true,
		$AutoPlaySteps: 1,
		$AutoPlayInterval: 2000,
		$PauseOnHover: 0,
		$ArrowKeyNavigation: true,
		$SlideDuration: 500,
		$MinDragOffsetToSlide: 20,
		//$SlideWidth: 981,
		//$SlideHeight: 594,
		$SlideSpacing: 0,
		$DisplayPieces: 1,
		$ParkingPosition: 0,
		$UISearchMode: 1,
		$PlayOrientation: 1,
		$DragOrientation: 3,

		$SlideshowOptions: {
			$Class: $JssorSlideshowRunner$,
			$Transitions: _SlideshowTransitions,
			$TransitionsOrder: 1,
			$ShowLink: true
		},

		$ArrowNavigatorOptions: {
			$Class: $JssorArrowNavigator$,
			$ChanceToShow: 1,
			$AutoCenter: 2,
			$Steps: 1
		}
	};

	var jssor_slider = new $JssorSlider$(containerId, options);
	function ScaleSlider() {
		var parentWidth = $('#'+containerId).parent().width();
		if (parentWidth)
			jssor_slider.$SetScaleWidth(Math.max(Math.min(parentWidth, 1600), 100));
		else
			window.setTimeout(ScaleSlider, 30);
	}
	ScaleSlider();
	$(window).bind("load", ScaleSlider);
	$(window).bind("resize", ScaleSlider);
	$(window).bind("orientationchange", ScaleSlider);
};