<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => ['auth', 'admin']], function () {
            Route::get('/', 'AdminController@index')->name('admin_index');

            Route::get('/my-profile', 'UserController@myProfile')->name('my_profile');
            Route::post('/update-profile', 'UserController@updateProfile')->name('update_profile');

            Route::post('gallery/add', 'GalleryController@add')->name('add_gallery_ajax');
            Route::post('gallery/delete', 'GalleryController@delete')->name('delete_gallery_ajax');
            Route::post('gallery/clear', 'GalleryController@clear')->name('clear_gallery_ajax');

            Route::get('config/{key}', 'OptionController@config')->name('config_website');
            Route::post('config/{key}', 'OptionController@configSave')->name('config_website_post');

            Route::get('user/list', 'UserController@index')->name('user_list');
            Route::post('user/bulk-action', 'UserController@bulkAction')->name('user_bulk_action');
            Route::get('user/edit/{id}', 'UserController@edit')->name('user_edit');
            Route::post('user/edit/{id}', 'UserController@handleEdit')->name('user_edit_submit');
            Route::get('user/create', 'UserController@create')->name('user_create');
            Route::post('user/create', 'UserController@handleCreate')->name('user_create_submit');
            Route::post('user/delete', 'UserController@delete')->name('user_delete');

            Route::get('contacts', 'ContactController@index')->name('admin_contacts');
            Route::post('contact/delete', 'ContactController@delete')->name('contact_delete');
            Route::get('contact/show/{id}', 'ContactController@show')->name('contact_show');
            Route::post('contact/update', 'ContactController@update')->name('contact_update');
            Route::post('contact/bulk-action', 'ContactController@bulkAction')->name('contact_bulk_action');

            // plugins
            Route::get('order', 'OrderController@index')->name('admin_order');
            Route::post('order/delete', 'OrderController@delete')->name('order_delete');
            Route::post('order/change_stage', 'OrderController@changeStage')->name('order_chang_stage');
            Route::post('order/bulk-action', 'OrderController@bulkAction')->name('order_bulk_action');

            Route::get('import', 'ImportExportController@index')->name('admin_import');
            Route::post('import', 'ImportExportController@import')->name('admin_import_post');

            Route::post('editor/upload', 'EditorController@upload')->name('editor_upload');
            Route::get('setting-custom-post/{postType}', 'SettingCustomPost@setting')->name('admin_setting_custom_post');

            loadRouteCustomPost('custom_post');

        });
    });
});
Route::group(['middleware' => 'stage_page'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/lien-he', 'ContactController@index')->name('contact');
    Route::post('/lien-he', 'ContactController@sentContact')->name('contact_post');
    Route::get('/tim-kiem', 'PostController@searchPost')->name('search_post');
    Route::post('/paginate-home', 'HomeController@ajaxPaginate')->name('ajax-pagination-home');

    Route::get('dang-nhap', 'UserController@login')->name('login_user');
    Route::post('dang-nhap', 'UserController@doLogin')->name('login_user_post');
    Route::get('dang-ky', 'UserController@register')->name('register');
    Route::post('dang-ky', 'UserController@doRegister')->name('register_post');
    Route::get('dang-xuat', 'UserController@logout')->name('logout_user');
    Route::get('quen-mat-khau', 'UserController@reset')->name('reset_password_user');
    Route::post('quen-mat-khau', 'UserController@doReset')->name('reset_password_user_post');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('tai-khoan', 'UserController@profile')->name('profile_user');
        Route::post('tai-khoan', 'UserController@doUpdate')->name('profile_user_post');
        Route::get('doi-mat-khau', 'UserController@changePassword')->name('change_password_user');
        Route::post('doi-mat-khau', 'UserController@doChangePassword')->name('change_password_user_post');
    });

    // Socialite
    Route::get('/redirect/{social}', 'SocialAuthController@redirectToProvider');
    Route::get('/callback/{social}', 'SocialAuthController@handleProviderCallback');


    Route::post('gallery/add', 'GalleryController@add')->name('fr_add_gallery_ajax');
    Route::post('gallery/delete', 'GalleryController@delete')->name('fr_delete_gallery_ajax');
    Route::post('gallery/clear', 'GalleryController@clear')->name('fr_clear_gallery_ajax');
    Route::get('gio-hang', 'PostController@listCart')->name('fr_order_list');
    Route::get('gio-hang/{id}', 'PostController@addToCart')->name('fr_order');
    Route::get('gio-hang/xoa/{id}', 'PostController@removeItemCart')->name('fr_delete_order');
    Route::group(['middleware' => ['auth']], function () {
        Route::post('dat-hang', 'PostController@checkout')->name('checkout_cart');
    });
    Route::post('gio-hang/thay-doi-so-luong/{id}', 'PostController@updateQuantity')->name('fr_change_quantity_order_ajax');
    Route::post('calc-price/{page_slug}/{post_slug}', 'PostController@calcPrice')->name('fr_calc_price_post');
    Route::get('{page_slug}', 'PostController@page')->name('page');
    Route::get('{page_slug}/danh-muc/{cate_slug}', 'PostController@listPostByCategory')->name('category_slug');
    Route::get('{page_slug}/{post_slug}', 'PostController@detail')->name('fr_post_detail');
    Route::post('/paginate-detail', 'PostController@ajaxPaginateDetail')->name('ajax-pagination-detail');
    Route::post('/paginate-detail-custom-post', 'PostController@ajaxPaginateDetailCustomPost')->name('ajax-pagination-detail-custom-post');
});
