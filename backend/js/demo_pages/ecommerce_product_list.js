/* ------------------------------------------------------------------------------
*
*  # Ecommerce - diy list
*
*  Demo JS code for diy listing pages
*
* ---------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function() {

    // Lightbox
    $('[data-popup="lightbox"]').fancybox({
	    padding: 3
    });

    // Uniform.js - custom checkboxes
    $('.styled').uniform();

});
