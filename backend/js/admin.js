var isAddPermalink = false;
$(document).ready(function () {
    /* ===========================
      Handle logout click button
      ============================*/
    $('#logout-btn').click(function () {
        $('#logout-form').submit();
    });

    /* ===========================
     Checkbox, radio box styled
     ============================*/
    $('.styled').uniform();

    /* ===========================
     Admin List Page
     ============================*/
    $('#all-checkbox').change(function () {
        $('.table-checkbox').prop('checked', $(this).prop("checked"));
        $.uniform.update();
    });

    $('#modal-delete').on('show.bs.modal', function (e) {
        var postId = $(e.relatedTarget).attr('data-id');
        $(e.currentTarget).find('input[name="itemId"]').val(postId);
    });

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    /* ===========================
    Summernote
    ============================*/
    $('.summernote').summernote({
        height: 400,
        imageTitle: {
            specificAltField: true,
        },
        popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']],
                ['custom', ['imageTitle']]
            ],
            table: [
                ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                ['custom', ['tableStyles']]
            ]
        },
        callbacks: {
            onImageUpload: function(files) {
                var editor = $(this);
                sendFile(files[0], editor);
            }
        }
    });

    /* ===========================
    Input file
    ============================*/
    var fileInputs = $('.input-file');
    fileInputs.change(function (e) {
        var label = $(this).next();
        var labelVal = label.html();
        var fileName = '';
        if (this.files && this.files.length > 1) {
            fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        } else {
            fileName = e.target.value.split('\\').pop();
        }
        if (fileName) {
            label.text('Thay đổi hình ảnh');
        } else {
            label.text('Chọn hình ảnh');
        }
        var cover = $(this).data('cover');
        readURL(this, cover);
    });

    /* ===========================
    Permalink
    ============================*/
    var permalinkContent = $('.permalink-content');
    if (permalinkContent.length) {
        var content = permalinkContent.data('value');
        processDisplayPermalink(content);
    }

    $('#btn-edit-permalink').click(function () {
        showEditPermalink();
    });

    $('#btn-permalink-cancel').click(function () {
        hideEditPermalink();
    });

    initPermalink();
    $('#input-name').change(function () {
        var nameValue = $(this).val();
        if (isAddPermalink === true && nameValue) {
            handleChangePermalink(nameValue);
            var permalinkContainer = $('.permalink');
            permalinkContainer.show();
            isAddPermalink = false;
        }

    });

    $('#btn-permalink-ok').click(function () {
        var permalinkInput = $('#permalink');
        var permalink = permalinkInput.val();
        handleChangePermalink(permalink);
    });
});

/**
 *
 * @param input
 * @param imageSelector
 */
function readURL(input, imageSelector) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(imageSelector).attr('src', e.target.result);
            $(imageSelector).removeClass('hidden');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

/**
 * Function help to init the permalink container
 */
function initPermalink() {
    var permalinkContainer = $('.permalink');
    var permalinkContent = $('.permalink-content');
    if (permalinkContent.data('value')) {
        permalinkContainer.show();
    } else {
        permalinkContainer.hide();
        isAddPermalink = true;
    }
}

/**
 * Function help to handle change the permalink value
 *
 * @param nameValue
 */
function handleChangePermalink(nameValue) {
    var permalinkInput = $('#permalink');
    var permalinkContent = $('.permalink-content');
    var slug = slugify(nameValue);
    processDisplayPermalink(slug);
    permalinkContent.data('value', slug);
    permalinkInput.val(slug);
    hideEditPermalink();
}

/**
 * Function help to show edit permalink input
 */
function showEditPermalink() {
    var permalinkContent = $('.permalink-content');
    $('#permalink').val(permalinkContent.data('value'));
    $('.permalink-input').removeClass('hidden');
    permalinkContent.addClass('hidden');
    $('#btn-edit-permalink').addClass('hidden');
}

/**
 *
 * @param permalink
 */
function processDisplayPermalink(permalink) {
    var content = permalink;
    var permalinkContent = $('.permalink-content');
    if (permalink.length > 80) {
        content = permalink.substr(0, 20) + '......' + permalink.substr(60, 20);
    }
    permalinkContent.text(content);
}

/**
 * Function help to hide the edit permalink input
 */
function hideEditPermalink() {
    var permalinkContent = $('.permalink-content');
    $('.permalink-input').addClass('hidden');
    permalinkContent.removeClass('hidden');
    $('#btn-edit-permalink').removeClass('hidden');
}

/**
 * Function help to convert text to slug
 * @param string
 * @returns {*}
 */
function slugify(string) {
    const a = 'àáâäæãåāăąẫẵẩẫảçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõṕŕřßśšşșťțûüùúūǘůűųưứẃẍÿýžźż·/_,:;ớộửữựừợởỡợờơụểễ';
    const b = 'aaaaaaaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnooooooooprrsssssttuuuuuuuuuuuwxyyzzz------oouuuuoooooouee';
    const p = new RegExp(a.split('').join('|'), 'g');
    return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with 'and'
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
}

//Function help to show the image when choose in input
function readURL(input, imageSelector) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(imageSelector).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

//Function help to send file in editor
function sendFile(file, editor) {
    var data = new FormData();
    var _token =  $('meta[name="csrf-token"]').attr('content');
    data.append("_token", _token);
    data.append("file_name", file.name);
    data.append("file", file);
    var url = window.location.origin + '/admin/editor/upload';

    $.ajax({
        url: url,
        type: "POST",
        dataType: 'JSON',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            var csrf_token = data.csrf_token;
            $('meta[name="csrf-token"]').attr('content', csrf_token);
            if (data.success) {
                $(editor).summernote('insertImage', data.image_url);
            } else {
                alert(data.message);
            }
        }
    });
}